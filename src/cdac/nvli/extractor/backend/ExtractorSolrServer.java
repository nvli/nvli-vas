package cdac.nvli.extractor.backend;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

import cdac.nvli.extractor.extract.AwardExtractor;
import cdac.nvli.extractor.extract.ConferenceExtractor;
import cdac.nvli.extractor.extract.ActsExtractor;
import cdac.nvli.extractor.extract.ExaminationExtractor;
import cdac.nvli.extractor.extract.PolicyExtractor;
import cdac.nvli.extractor.extract.PublicationExtractor;
import cdac.nvli.extractor.extract.SchemeExtractor;
import cdac.nvli.extractor.index.VasExtractorIndexer;

/**
 * This class is used to connect solr server
 * @author pragya
 *
 */

public class ExtractorSolrServer {

	private HttpSolrServer solrServer;
	private InputStream inStream;
	Properties prop = new Properties();
	/**
	 * Logger
	 */
	private static final Log LOG = LogFactory
			.getLog(VasExtractorIndexer.class.getName());
	
	/**
	 * @return HttpSolrServer 
	 * SOLR_SERVER, SOLR_PORT and SOLR_COLLECTION params are defined in solrconfig.properties file
	 * @throws IOException
	 */
	public HttpSolrServer getSolrServer() throws IOException
	{ 
		String solrFilePath = AwardExtractor.SOLR_FILE_PATH;
		solrFilePath=ActsExtractor.SOLR_FILE_PATH;
		//solrFilePath=ConferenceExtractor.SOLR_FILE_PATH;
		//solrFilePath=ExaminationExtractor.SOLR_FILE_PATH;
		//solrFilePath=PolicyExtractor.SOLR_FILE_PATH;
		//solrFilePath=PublicationExtractor.SOLR_FILE_PATH;
		//solrFilePath=SchemeExtractor.SOLR_FILE_PATH;
		
		LOG.info("getting solr server==> "); 
		
		//inStream =getClass().getClassLoader().getResourceAsStream("resources/solrconfig.properties");	   
		
		File initialFile = new File(solrFilePath);
	    inStream = new FileInputStream(initialFile);
		
		if(inStream!=null)
		{
			prop.load(inStream);
		}
		else {
			LOG.error("property file '" + inStream + "' not found in the classpath");
			throw new FileNotFoundException("property file '" + inStream + "' not found in the classpath");
			
		}
		String solrHost=prop.getProperty("SOLR_SERVER");
		String solrPort=prop.getProperty("SOLR_PORT");
		String solrCollection=prop.getProperty("SOLR_COLLECTION");
		
		String solrUrl="http://"+solrHost+":"+solrPort+ "/solr/"+solrCollection+"/";
		
		solrServer=new HttpSolrServer(solrUrl);
		
		return solrServer;
	}
	
}
