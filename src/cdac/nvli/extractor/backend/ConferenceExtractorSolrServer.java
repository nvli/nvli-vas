package cdac.nvli.extractor.backend;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.solr.client.solrj.impl.HttpSolrServer;

public class ConferenceExtractorSolrServer {
	private HttpSolrServer solrServer;
	private InputStream inStream;
	Properties prop = new Properties();
	
	public HttpSolrServer getSolrServer() throws IOException
	{ 
		System.out.println("solrserver is"); 
		inStream =getClass().getClassLoader().getResourceAsStream("resources/solrconfig.properties");
		System.out.println("inStream---->"+inStream);
		if(inStream!=null)
		{
			prop.load(inStream);
		}
		else {
			throw new FileNotFoundException("property file '" + inStream + "' not found in the classpath");
		}
		String solrHost=prop.getProperty("SOLR_SERVER_CONFERENCE");
		String solrPort=prop.getProperty("SOLR_PORT_CONFERENCE");
		String solrCollection=prop.getProperty("SOLR_COLLECTION_CONFERENCE");
		
		String solrUrl="http://"+solrHost+":"+solrPort+ "/solr/"+solrCollection+"/";
		
		solrServer=new HttpSolrServer(solrUrl);
		
		
		return solrServer;
	}
}
