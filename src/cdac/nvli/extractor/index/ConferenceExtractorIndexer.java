package cdac.nvli.extractor.index;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import cdac.nvli.extractor.bean.ConferenceBean;

public class ConferenceExtractorIndexer {
	// The CCONFERENCE meta data attributes for sp-hadoop5:7983 solr
		/*private static final String CCONFERENCE_ID = "id";
		private static final String CCONFERENCE_DESCRIPTION = "Description";
		private static final String CCONFERENCE_TITLE = "Title";
		private static final String CCONFERENCE_DATE = "When";
		private static final String CCONFERENCE_LOCATION = "Where";
		private static final String CCONFERENCE_LINK = "Link";
		private static final String CCONFERENCE_URL = "url";*/
			
	/* CCONFERENCE meta data attributes for localhost solr*/	
		private static final String CONFERENCE_ID_LOCAL = "idVas";
		private static final String CONFERENCE_DESCRIPTION_LOCAL = "descriptionVas";
		private static final String CONFERENCE_TITLE_LOCAL = "titleVas";
		private static final String CONFERENCE_DATE_LOCAL = "dateVas";
		private static final String CONFERENCE_LOCATION_LOCAL = "locationVas";
		private static final String CONFERENCE_LINK_LOCAL = "linkVas";
		private static final String CONFERENCE_URL_LOCAL = "urlVas";
		private static final String CONFERENCE_CATEGORY_LOCAL = "categoryVas";
		private static final String RESOURCE_CODE = "resourceCode";	
		private static final String IDCODE = "idCode";
			/**
			 * Logger
			 */
			private static final Log LOG = LogFactory
					.getLog(ConferenceExtractorIndexer.class.getName());
			
			
			public void indexConferenceDocs(HttpSolrServer solrServer,ConferenceBean ConferenceBean)
			{
				System.out.println("Indexing of CCONFERENCE is successful---->");
				if(ConferenceBean!=null)
				{
					System.out.println("Indexing of CCONFERENCE is successful---->"+ConferenceBean); 
					SolrInputDocument solrDoc=new SolrInputDocument();
					/*LOG.info("Indexer started--->"+ConferenceBean.getConferenceTitle());
					
					
					solrDoc.addField(CONFERENCE_ID_LOCAL, ConferenceBean.getConferenceId());
					solrDoc.addField(CONFERENCE_TITLE_LOCAL, ConferenceBean.getConferenceTitle());
					solrDoc.addField(CONFERENCE_LINK_LOCAL, ConferenceBean.getConferenceLink());
					solrDoc.addField(CONFERENCE_URL_LOCAL, ConferenceBean.getConferenceUrl());
					solrDoc.addField(CONFERENCE_DESCRIPTION_LOCAL, ConferenceBean.getConferenceDescription());
					solrDoc.addField(CONFERENCE_DATE_LOCAL, ConferenceBean.getConferenceDate());
					solrDoc.addField(CONFERENCE_LOCATION_LOCAL, ConferenceBean.getConferenceLocation());
					solrDoc.addField(RESOURCE_CODE, ConferenceBean.getResourceCode());
					solrDoc.addField(IDCODE, ConferenceBean.getIdCode());*/
					//solrDoc.addField(CCONFERENCE_CATEGORY_LOCAL, ConferenceBean.getCategory());
					if(solrServer!=null)
					{
						try {
							solrServer.add(solrDoc);
							solrServer.commit();
							System.out.println("Indexing of CCONFERENCE is successful---->");
							LOG.info("Indexing of CCONFERENCE is successful");
						} catch (SolrServerException | IOException e) {
							LOG.info("Indexing Failed"); 
							e.printStackTrace();
						}
					}
				}
				
			}
}
