package cdac.nvli.extractor.index;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import cdac.nvli.extractor.bean.AwardBean;
import cdac.nvli.extractor.bean.VasBean;
import cdac.nvli.extractor.parse.AwardExtractorParser;

public class AwardExtractorIndexer {
	
	// The award meta data attributes
	private static final String VAS_ID = "id";
	private static final String VAS_IDENTIFIER = "identifier";
	private static final String VAS_DESCRIPTION = "content";
	private static final String VAS_TITLE = "title";
	private static final String VAS_RECIPIENT_NAME = "awardRecipientName";
	private static final String VAS_FIELD = "awardField";
	private static final String VAS_YEAR = "date";
	private static final String VAS_RECIPIENT_LOCATION = "location";
	private static final String VAS_URL = "url";
	private static final String RESOURCE_CODE = "resourceType";	
	private static final String IDCODE = "idcode";
	
		/**
		 * Logger
		 */
		private static final Log LOG = LogFactory
				.getLog(AwardExtractorIndexer.class.getName());
		
		
		public void indexVASDocs(HttpSolrServer solrServer,VasBean vasBean)
		{
			System.out.println("Indexing of award is successful---->");
			if(vasBean!=null)
			{
				System.out.println("Indexing of award is successful---->"+vasBean);
				LOG.info("Indexer started--->"+vasBean.getTitle());
				SolrInputDocument solrDoc=new SolrInputDocument();
				
				solrDoc.addField(VAS_ID, vasBean.getId());
				solrDoc.addField(VAS_IDENTIFIER, vasBean.getIdentifier());
				solrDoc.addField(VAS_TITLE, vasBean.getTitle());
				solrDoc.addField(VAS_URL, vasBean.getUrl());
				solrDoc.addField(VAS_DESCRIPTION, vasBean.getDescription());
				solrDoc.addField(VAS_RECIPIENT_NAME, vasBean.getAwardRecipientName());
				solrDoc.addField(VAS_YEAR, vasBean.getDate());
				solrDoc.addField(VAS_FIELD, vasBean.getField());
				solrDoc.addField(VAS_RECIPIENT_LOCATION, vasBean.getLocation());
				solrDoc.addField(RESOURCE_CODE, vasBean.getResourceCode());
				solrDoc.addField(IDCODE, vasBean.getIdCode());
				if(solrServer!=null)
				{
					try {
						solrServer.add(solrDoc);
						solrServer.commit();
						System.out.println("Indexing of award is successful---->");
						LOG.info("Indexing of award is successful");
					} catch (SolrServerException | IOException e) {
						LOG.info("Indexing Failed"); 
						e.printStackTrace();
					}
				}
			}
			
		}
}
