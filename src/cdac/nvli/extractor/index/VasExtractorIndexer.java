package cdac.nvli.extractor.index;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import cdac.nvli.extractor.bean.VasBean;

/**
 * This class provides common methods used for indexing the parsed data in solr
 * @author pragya
 *
 */
public class VasExtractorIndexer {
	// The meta data attributes
	private static final String VAS_ID = "id";
	private static final String VAS_IDENTIFIER = "identifier";
	private static final String VAS_DESCRIPTION = "content";
	private static final String VAS_TITLE = "title";
	private static final String VAS_RECIPIENT_NAME = "awardRecipientName";
	private static final String VAS_FIELD = "awardField";
	private static final String VAS_DATE = "date";
	private static final String VAS_END_DATE = "endDate";
	private static final String VAS_RECIPIENT_LOCATION = "location";
	private static final String VAS_URL = "url";
	private static final String RESOURCE_CODE = "resourceType";	
	private static final String IDCODE = "idcode";
	
	private static final String VAS_Editor = "editor";
	private static final String VAS_Author = "author";
	private static final String VAS_OtherInformation = "otherInformation";
	private static final String VAS_Ministry = "ministry";
	private static final String VAS_Link= "link";
	private static final String VAS_recruitment_board= "recruitment_board";
	private static final String VAS_recruitment_location= "recruitment_location";
	
		/**
		 * Logger
		 */
		private static final Log LOG = LogFactory
				.getLog(VasExtractorIndexer.class.getName());
		
		
		/**
		 * @param solrServer
		 * @param vasBean
		 */
		public void indexVASDocs(HttpSolrServer solrServer,VasBean vasBean)
		{
			
			LOG.info("Indexing of vas started---->"); 
			if(vasBean!=null)
			{
				System.out.println("Indexing of vas is started---->"+vasBean);
				LOG.info("Indexer started--->"+vasBean.getTitle());
				SolrInputDocument solrDoc=new SolrInputDocument();
				
				solrDoc.addField(VAS_ID, vasBean.getId());
				solrDoc.addField(VAS_IDENTIFIER, vasBean.getIdentifier());
				solrDoc.addField(VAS_TITLE, vasBean.getTitle());
				solrDoc.addField(VAS_URL, vasBean.getUrl());
				solrDoc.addField(VAS_DESCRIPTION, vasBean.getDescription());
				if(vasBean.getIdCode().equalsIgnoreCase("VA-AWD"))
				{
					solrDoc.addField(VAS_RECIPIENT_NAME, vasBean.getAwardRecipientName());
					solrDoc.addField(VAS_FIELD, vasBean.getField());
				}
				if(vasBean.getIdCode().equalsIgnoreCase("VA-CON"))
				{
					solrDoc.addField(VAS_END_DATE, vasBean.getEndDate());
				}
				if(vasBean.getIdCode().equalsIgnoreCase("VA-PUB"))
				{
					solrDoc.addField(VAS_Editor, vasBean.geteditor());
				}
				if(vasBean.getIdCode().equalsIgnoreCase("VA-PUB"))
				{
					solrDoc.addField(VAS_Author, vasBean.getauthor());
				}
				if(vasBean.getIdCode().equalsIgnoreCase("VA-SCH"))
				{
					solrDoc.addField(VAS_END_DATE, vasBean.getEndDate());
				}
				/*if(vasBean.getIdCode().equalsIgnoreCase("VA-EXA"))
				{
					solrDoc.addField(VAS_recruitment_board, vasBean.getrecruitment_board());
					solrDoc.addField(VAS_recruitment_location, vasBean.getrecruitment_location());
				}*/
				solrDoc.addField(VAS_recruitment_board, vasBean.getrecruitment_board());
				solrDoc.addField(VAS_recruitment_location, vasBean.getrecruitment_location());
				solrDoc.addField(VAS_Link,vasBean.getLink());
				solrDoc.addField(VAS_Ministry, vasBean.getministry());
				solrDoc.addField(VAS_OtherInformation, vasBean.getotherInformation());
				solrDoc.addField(VAS_DATE, vasBean.getDate());
				solrDoc.addField(VAS_RECIPIENT_LOCATION, vasBean.getLocation());
				solrDoc.addField(RESOURCE_CODE, vasBean.getResourceCode());
				solrDoc.addField(IDCODE, vasBean.getIdCode());
				if(solrServer!=null)
				{
					try {
						solrServer.add(solrDoc);
						solrServer.commit();
						LOG.info("Indexing of vas is successful---->"); 
						} 
					catch (SolrServerException | IOException e)
					{
						LOG.info("Indexing Failed"); 
						System.out.println(e);
						e.printStackTrace();
					}
				}
			}

		}


}
