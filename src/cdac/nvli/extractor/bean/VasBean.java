package cdac.nvli.extractor.bean;

/**
 * This class is a model for all Value added services 
 * @author pragya
 * 
 */
public abstract class VasBean {
	private String id;
	private String identifier;
	private String title;
	private String link;
	private String description;
	private String field;
	private String awardRecipientName;
	private String location;
	private String date;
	private String endDate;
	private String url;
	private String idCode;
	private String resourceCode;
	
	private String author;
	private String editor;
	private String otherInformation;
	private String ministry;
	private String recruitment_board;
	private String recruitment_location;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getAwardRecipientName() {
		return awardRecipientName;
	}
	public void setAwardRecipientName(String awardrecipientName) {
		this.awardRecipientName = awardrecipientName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIdCode() {
		return idCode;
	}
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}
	public String getResourceCode() {
		return resourceCode;
	}
	public void setResourceCode(String resourceCode) {
		this.resourceCode = resourceCode;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	//////////////////////////////////////////////
	public String geteditor() {
		return editor;
	}
	public void seteditor(String editor) {
		this.editor = editor;
	}
	public String getauthor() {
		return author;
	}
	public void setauthor(String author) {
		this.author = author;
	}
	
	public String getotherInformation() {
		return otherInformation;
	}
	public void setotherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}
	public String getministry() {
		return ministry;
	}
	public void setministry(String ministry) {
		this.ministry = ministry;
	}
	
	public String getrecruitment_board() {
		return recruitment_board;
	}
	public void setrecruitment_board(String recruitment_board) {
		this.recruitment_board = recruitment_board;
	}
	public String getrecruitment_location() {
		return recruitment_location;
	}
	public void setrecruitment_location(String recruitment_location) {
		this.recruitment_location = recruitment_location;
	}
	
	
	@Override
	public String toString() {
		return "VasBean [id=" + id + ", identifier=" + identifier + ", title=" + title + ", link=" + link
				+ ", description=" + description + ", field=" + field + ", awardRecipientName=" + awardRecipientName
				+  ",editor="+editor+",author="+author+",otherInformation="+otherInformation+",recruitment_board="+recruitment_board+",recruitment_location="+recruitment_location+",ministry="+ministry+",location=" + location + ", date=" + date + ", endDate=" + endDate + ", url=" + url + ", idCode="
				+ idCode + ", resourceCode=" + resourceCode + "]";
	}

	
	
}
