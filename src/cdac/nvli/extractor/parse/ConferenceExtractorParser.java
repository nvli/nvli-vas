package cdac.nvli.extractor.parse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;

import cdac.nvli.extractor.bean.AwardBean;
import cdac.nvli.extractor.bean.ConferenceBean;

public class ConferenceExtractorParser {
	
	private static final String XPATH_EXPRESSION_EVENT_TITLE = "//*[local-name()='head']/*[local-name()='title']";//"/html/head/title";
	private Document doc;
	private W3CDom w3cDom;
	private ArrayList<String> nodeList;
	private ArrayList<ConferenceBean> conferenceBeanList; 
	private boolean flag=false;
	private boolean flagToBreak1If= false;
	private boolean flagToBreak2For= false;
	/**
	 * Logger
	 */
	private static final Log LOG = LogFactory
			.getLog(AwardExtractorParser.class.getName());
	String conferenceUrl="";
	String category="";
	String conferenceId="";
	String conferenceIdentifier="";
	String conferenceStartDate="";
	String conferenceEndDate="";
	String conferenceTitle="";
	String conferenceDescription="";
	String conferenceLocation="";
	String conferenceLink="";
	
	public ConferenceBean createConfBean(String conferenceTitle, String conferenceDescription ,String conferenceStartDate, 
															String conferenceEndDate, String conferenceLocation, String conferenceUrl,String conferenceLink, String category )
	{
		ConferenceBean conferenceBean=new ConferenceBean();
		conferenceId=conferenceStartDate+conferenceTitle.replaceAll(" ","");
		conferenceIdentifier="VALUE_"+conferenceStartDate+"_"+conferenceTitle.replaceAll(" ","");
		
		
		conferenceBean.setId(conferenceId);
		conferenceBean.setIdentifier(conferenceIdentifier); 
		conferenceBean.setTitle(conferenceTitle);
		conferenceBean.setDescription(conferenceTitle);
		conferenceBean.setLocation(conferenceLocation);
		conferenceBean.setDate(conferenceStartDate);
		conferenceBean.setEndDate(conferenceEndDate);
		conferenceBean.setUrl(conferenceUrl);
		conferenceBean.setLink(conferenceLink); 
		conferenceBean.setIdCode(category);
		conferenceBean.setResourceCode("VALUE"); 
		
		
		return conferenceBean;
	}
	public ArrayList<ConferenceBean> parseIt(String url) 
	{
		conferenceUrl=url;
		
		try {
			doc=Jsoup.connect(url).get();
			conferenceBeanList=new ArrayList<ConferenceBean>();
		//	System.out.println(doc.title()); 
			
			
			if(doc.title().contains("Indian Institute of Advanced Study"))
			{
				Elements table=doc.select("table");
				
				for(Element e:table)
				{
					
					for(int i=1;i<e.children().size();i++)
					{
						
						Element trData=e.child(i);
						for(int j=0;j<trData.children().size();j++)
						{
							
							Element tdData=trData.child(j);
							for(int k=0;k<tdData.children().size();k+=3)
							{
								conferenceTitle=tdData.child(k).text();
								
								String date[]=tdData.child(k+2).text().split("to");
								conferenceStartDate=date[0];
								conferenceEndDate=date[1];
								category=tdData.child(k+1).text();
								
								if(category.toLowerCase().contains("conference"))
									category="VA-CON";
								else if(category.toLowerCase().contains("seminar"))
									category="VA-SEM";
								
								SimpleDateFormat oldDateParser= new SimpleDateFormat("dd-MMM-yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								
								conferenceStartDate=newDateParser.format(oldDateParser.parse(conferenceStartDate)); 
								conferenceEndDate=newDateParser.format(oldDateParser.parse(conferenceEndDate)); 
								conferenceDescription=conferenceTitle;
								
								ConferenceBean conferenceBean=createConfBean(conferenceTitle,conferenceDescription,conferenceStartDate,
																				conferenceEndDate,conferenceLocation,conferenceUrl,conferenceLink,category);
								
								conferenceBeanList.add(conferenceBean);
							}
						}
					}
				}
			}
			else if(doc.title().contains("NATIONAL CONFERENCE"))
			{
				Elements table=doc.select("div.event-wrapper");
				String yearText=doc.select("ul.nav-bar").first().getElementsByTag("a").first().text();
				String year=yearText.substring(yearText.indexOf(" "));
				
				for(Element e:table)
				{
					
					for(int i=0;i<e.children().size();i++)
					{
						Element trData=e.child(i);
						
						for(int j=0;j<trData.children().size();j++)
						{
							Element tdData=trData.child(j);
							
							for(int k=0;k<tdData.children().size();k+=2)
							{
								
								conferenceStartDate=tdData.child(k).text()+year;
								conferenceEndDate=year;
								conferenceTitle=tdData.child(k+1).text();
								conferenceDescription=conferenceTitle;
								conferenceLocation=tdData.child(k+1).text().substring(conferenceTitle.lastIndexOf(")")+1).trim();
								category="VA-CON";
								
								
								SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMM yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								conferenceStartDate=newDateParser.format(oldDateParser.parse(conferenceStartDate));  
								
								SimpleDateFormat oldDateParserForEndDate= new SimpleDateFormat("yyyy");
								//conferenceEndDate=newDateParser.format(oldDateParserForEndDate.parse(conferenceEndDate));//needs to be rechecked
								conferenceEndDate=conferenceStartDate;
								
								
								ConferenceBean conferenceBean=createConfBean(conferenceTitle,conferenceDescription,conferenceStartDate,
										conferenceEndDate,conferenceLocation,conferenceUrl,conferenceLink,category);
								
								
								conferenceBeanList.add(conferenceBean);
							}
						}
					}
				}
			}
			else if(url.contains("mzu.edu.in"))//not completely done issue with date
			{
				Elements table=doc.select("table");
				
				for(int i=3;i<table.size();i++)
				 {
					Elements rows=table.get(i).select("tr"); 
					for(int j=0;j<rows.size();j++)
					 {
						
						 Element row=rows.get(j);
						 String dateStr="";
						 conferenceTitle=row.select("td:eq(0)").text();
						 conferenceDescription=conferenceTitle;
						 if(conferenceTitle.contains("("))
						 {
							 String dateSubStr=conferenceTitle.substring(conferenceTitle.indexOf("(")); 
							 System.out.println("date is at 62"+dateSubStr);
							 if(dateSubStr.contains(":")) 
							 {
								dateStr=dateSubStr.substring(dateSubStr.indexOf(":")+1); 
								dateStr=dateStr.replace("(","").replace(")","").trim();
								
							 }
							 else
								 dateStr=dateSubStr.replace("(","").replace(")","").trim();
							 
						 }
						 else
							 dateStr=row.select("td:eq(1)").text();
						
						 if(!dateStr.matches(".*\\d+.*")) //check if the String contains any number  
						 {
							 dateStr=row.select("td:eq(1)").text(); //if not select the created date as confDate
						 } 
						 
						 conferenceStartDate=dateStr;
						 
						
						 Elements links=row.select("td:eq(4)").select("a[href]");
						 for(Element link:links)
						 {
							 conferenceLink =link.absUrl("href");
						 }
						
						  	System.out.println("conferenceTitle: "+conferenceTitle);
							System.out.println("conferenceStartDate: "+conferenceStartDate);
							System.out.println("conferenceEndDate: "+conferenceLink);
							System.out.println("conferenceLocation: "+conferenceLocation);
							
							ConferenceBean conferenceBean=createConfBean(conferenceTitle,conferenceDescription,conferenceStartDate,
									conferenceEndDate,conferenceLocation,conferenceUrl,conferenceLink,category);
							
							
							conferenceBeanList.add(conferenceBean);
					 }
				 }
			}
			else if(url.contains("internationaljournalssrg.org"))
			{
				Elements table=doc.select("table");
				//System.out.println("div data is: "+table.text());
				for(int i=0;i<table.size();i++)
				 {
					Elements rows=table.get(i).select("tr"); 
					System.out.println("row element size is: "+rows.size());
					for(int j=2;j<rows.size();j++)
					 {
						Element row=rows.get(j);
						//System.out.println("rows text is: "+rows.text());
						
						Elements link_name=row.select("a[href]");
						 String list_name=null;
						 for(Element links:link_name)
						 {
							 list_name =links.absUrl("href");
						 }
						
						System.out.println("conferenceLink is: "+list_name);
						if(row.select("th").text().contains("Conferences -"))
						{
							continue;
						}
						else
						{
							
							conferenceTitle=row.select("td").text();
						
							if(conferenceTitle.contains(")"))
							{
								conferenceLocation=conferenceTitle.substring(conferenceTitle.lastIndexOf(")")+2,conferenceTitle.length()).trim();
								conferenceTitle=conferenceTitle.substring(0,conferenceTitle.lastIndexOf(")")+1);
								if(conferenceLocation.contains("-2014"))
								{
									conferenceLocation=conferenceLocation.substring(conferenceLocation.indexOf("4")+2,conferenceLocation.length()).trim();
								}
							}
							else
							{
								conferenceLocation=conferenceTitle;
								if(conferenceLocation.contains("2014"))
								{
									conferenceTitle=conferenceTitle.substring(0,conferenceTitle.lastIndexOf("4")+1);
									conferenceLocation=conferenceLocation.substring(conferenceLocation.indexOf("4")+2,conferenceLocation.length()).trim();
									
								}
							}
								
							
							conferenceStartDate=row.select("th").text();
							String dateStr=conferenceStartDate.substring(conferenceStartDate.indexOf(" ")+1,conferenceStartDate.length());
						
							String[] dateArr={};
							String FinalDate="";
							if(dateStr.contains(","))
							{
								dateArr=dateStr.split(",");
								FinalDate=dateArr[dateArr.length-1].replaceAll(" ","");
							}
							else if(dateStr.contains("to"))
							{
								dateArr=dateStr.split("to");
								FinalDate=dateArr[dateArr.length-1].replaceAll(" ","");
							}
							else
							{
								FinalDate=dateStr.replaceAll(" ","");
							}
							
														
							SimpleDateFormat oldDateParser= new SimpleDateFormat("dd/MM/yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							
							conferenceStartDate=newDateParser.format(oldDateParser.parse(FinalDate)); 
							
							System.out.println("title is:"+conferenceTitle);
							System.out.println("conferenceLocation is:"+conferenceLocation);
							System.out.println("conferenceStartDate is:"+conferenceStartDate);
							
							conferenceId=conferenceStartDate+conferenceTitle.replaceAll(" ","");
							conferenceIdentifier="VALUE_"+conferenceStartDate+"_"+conferenceTitle.replaceAll(" ","");
							category="VA-CON";
							conferenceDescription=conferenceTitle;
							ConferenceBean conferenceBean=createConfBean(conferenceTitle,conferenceDescription,conferenceStartDate,
									conferenceEndDate,conferenceLocation,conferenceUrl,conferenceLink,category);
							
							conferenceBeanList.add(conferenceBean);
							
							}
					}
				}
			}
			else if(url.contains("www.allconferencealert.com/event_detail.php"))
			{
				Elements table=doc.select("fieldset.fld_set");
				System.out.println("tabledata is **** "+table.text()); 
				String date="";
				String title="";
				String location="";
				String link="";
				String description="";
				for(int i=0;i<table.size();i++)
				 {
					Elements rows=table.get(i).select("tr"); 
					System.out.println("row element size is: "+rows.size());
					
					for(int j=1;j<rows.size();j++)
					 {
						Element row=rows.get(j);
						//System.out.println("rows text is: "+row.text());
						Elements td=row.select("td");
						String td1=row.select("td:eq(0)").text();
						String td2=row.select("td:eq(1)").text();
					
						if(td1.contains("Venue"))
							location=td1.substring(td1.indexOf(":")+1).trim();
						
						else if(td1.contains("Website")) 
							link=td1.substring(td1.indexOf(" ")).trim();
						
						else if(td1.contains("About Event")) 
							description=td1.substring(td1.indexOf(" ")).trim();
						else
						{
							if(date.equalsIgnoreCase(""))
							{
								String dates[]=td1.split("To");
								date=dates[0].trim();
								if(date.contains("st")) 
									date=date.replace("st","");
								if(date.contains("nd"))
									date=date.replace("nd", ""); 
								if(date.contains("rd"))
									date=date.replace("rd", ""); 
								if(date.contains("th"))	
									date=date.replace("th", ""); 
							}
							if(title.equalsIgnoreCase(""))
								title=td2;
						} 
						
						
						
					 }
				 }
				
				
							
				SimpleDateFormat oldDateParser= new SimpleDateFormat("MMM dd yyyy");
				SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				
				conferenceStartDate=newDateParser.format(oldDateParser.parse(date)); 
				
				conferenceLocation=location;
				conferenceTitle=title;
				conferenceDescription=description;
				conferenceUrl=link;
				conferenceLink=link;
				category="VA-CON";
				
				
				System.out.println("conferenceTitle: "+conferenceTitle);
				System.out.println("conferenceStartDate: "+conferenceStartDate);
				System.out.println("conferenceEndDate: "+conferenceUrl);
				System.out.println("conferenceLocation: "+conferenceLocation);
				
				
				ConferenceBean conferenceBean=createConfBean(conferenceTitle,conferenceDescription,conferenceStartDate,
						conferenceEndDate,conferenceLocation,conferenceUrl,conferenceLink,category);
				
				conferenceBeanList.add(conferenceBean);
				
				
			}
			else if(url.contains("allconferencealert")) 
			{
				Elements table=doc.select("table.conf_show_table");
				HashSet<String> linkSet=new HashSet<String>();
				System.out.println("tabledata is **** "+table.text()); 
				for(int i=0;i<table.size();i++)
				 {
					Elements rows=table.get(i).select("tr"); 
					System.out.println("row element size is: "+rows.size());
					for(int j=0;j<rows.size();j++)
					 {
						Element row=rows.get(j);
						//System.out.println("rows text is: "+row.text());
						
						Elements linkList=row.select("a[href]");
						 String link=null;
						 for(Element links:linkList)
						 {
							 link =links.absUrl("href");
						 }
						 System.out.println("list_name text is: "+link);
						try {
							if(null!=link| !link.isEmpty())
							
							 linkSet.add(link);
							}
						catch(NullPointerException ne)
							{
								continue;
							}
						}
					}
				BufferedWriter writer = null;
				
				try {
					writer = new BufferedWriter(new FileWriter(
					          new File("src/resources/upcomingConfUrls"), true));
						for(String link:linkSet)
						{
							writer.write(link.toString());
						    writer.newLine();
						    writer.flush();
						 
						}
					} catch (IOException ex) {
					  // report
					} finally {
					   try {writer.close();} catch (Exception ex) {/*ignore*/}
					}
			}
			else
			{
				Element tableData=doc.select("table.w100-50b").first();
			
				for(int i=0;i<tableData.children().size();i++)
				{
				
				Element trData=tableData.child(i);
				
				for(int j=1;j<trData.children().size();j++)
				{
					
					Element tdData=trData.child(j);
					for(int k=0;k<tdData.children().size();k=+3)
					{
						
						for(Element name:tdData.child(k).getElementsByTag("a"))
						{
							
							conferenceTitle=name.text();
						}
						String date[]=tdData.child(k+1).text().split("-");
						conferenceStartDate=date[0]+" "+"2017";
						conferenceEndDate=date[1];
						
						SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMM yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						
						conferenceStartDate=newDateParser.format(oldDateParser.parse(conferenceStartDate)); 
						conferenceEndDate=newDateParser.format(oldDateParser.parse(conferenceEndDate)); 
						conferenceDescription=conferenceTitle;
						conferenceLocation=tdData.child(k+2).text();
						category="VA-CON";
						
						
						System.out.println("conferenceTitle: "+conferenceTitle);
						System.out.println("conferenceStartDate: "+conferenceStartDate);
						System.out.println("conferenceEndDate: "+conferenceEndDate);
						System.out.println("conferenceLocation: "+conferenceLocation);
						
						ConferenceBean conferenceBean=createConfBean(conferenceTitle,conferenceDescription,conferenceStartDate,
								conferenceEndDate,conferenceLocation,conferenceUrl,conferenceLink,category);
						
					
						conferenceBeanList.add(conferenceBean);
					}
				}
			}
			
		}
			
			
		/*	w3cDom=new W3CDom();
			nodeList= new ArrayList<String>();
			conferenceBeanList=new ArrayList<ConferenceBean>();
			//convert from jsoup doc to dom doc
			org.w3c.dom.Document w3cDoc= w3cDom.fromJsoup(doc);
			
		//	System.out.println("doc is "+doc+ w3cDoc.getTextContent());
			//XPath
			XPathFactory factory = XPathFactory.newInstance();
			XPath xPath = factory.newXPath();
			// XPath expression for the award title
			XPathExpression xPathExpressionEventTitle = xPath
					.compile(XPATH_EXPRESSION_EVENT_TITLE);
			// get the award title
			String conferencePageTitle = xPathExpressionEventTitle.evaluate(w3cDoc).trim();
			// award link
			String conferenceUrl=url;
			// XPath expression for the award
			NodeList nodes = (NodeList) XPathFactory
					.newInstance()
					.newXPath().
					evaluate("//div/div/span//a/text()  | //div/b//span/text() | //tr/td/span/text() | //tr/th//a/text() "
							+ "| //tr/td//a/text() | //tr/td//a/@href " //1* till here this xpath epression works for http://www.muhs.ac.in/dept_links.aspx?doctype=CS&doctypetitle=Conferences%20/%20Seminars url
							+ "| //div/div/span/text() |//tr/td/div//strong//text() | //tr/td/table/tbody/tr/td//strong//text()" //2*
							+ "| //div/h4/text() | //div/h3/text() | //div/ul/li/text() | //div/ul/li/span/text() | //div/ul/li//a/@href" //2* this xpath epression works for http://www.nipfp.org.in/events 
							+ "| /html/body/div/div/div/div/div/div/p//h3/text() | //div/div/header/div/nav/ul/li//a/text() | //tr/td/p/text() | //tr/td/p//a/text() | //tr/td/p//a/@href"
							+ "| //*[local-name()='table']/*[local-name()='tbody']/*[local-name()='tr']/*[local-name()='td']/*[local-name()='p']/*[local-name()='a']/text() "//3*
							+ "| //*[local-name()='table']/*[local-name()='tbody']/*[local-name()='tr']/*[local-name()='td'][@class='pad10']/*[local-name()='p']/*[local-name()='a']/@href "//3*
							+ "| //*[local-name()='table']/*[local-name()='tbody']/*[local-name()='tr'][@class='header']/*[local-name()='td']/*[local-name()='p']/text()"//3*  his xpath epression works for http://www.ieee.org/conferences_events/conferences/
							, w3cDoc,XPathConstants.NODESET);
					
			System.out.println("In parse it "+nodes.getLength()+ " Title is**"+conferencePageTitle);
			
			for(int curNode=0;curNode<nodes.getLength();curNode++)
			{
				String currentNodeValue=nodes.item(curNode).getNodeValue().trim();
				//System.out.println("NODE VALUES FOR AWARD ARE"+currentNodeValue);
				nodeList.add(currentNodeValue);
			}
			for(int i=0;i<nodeList.size();i++)
			{
				String nodeVal=nodeList.get(i);
				//remove #
				if(nodeVal.equalsIgnoreCase("#"))
					nodeList.remove(i);
				//remove blank
				if(nodeVal.equalsIgnoreCase(""))
					nodeList.remove(i);	
				//remove spaces
				if(nodeVal.equalsIgnoreCase(" "))
					nodeList.remove(i);
				if(nodeVal.contains("javascript:__doPostBack"))
				{
					System.out.println("IN iF AT 86"+nodeVal);
					nodeList.remove(i);
				}
				//remove null or blank elements from the list
				nodeList.removeAll(Arrays.asList(null,""));
			}
			
			for(int curNode=0;curNode<nodeList.size();curNode++)
			{
				
				System.out.println("NODE VALUES AFTER REMOVAL ARE***109 "+nodeList.get(curNode));
			}
			if(!nodeList.isEmpty())
			{
				for(int curNode=0;curNode<nodeList.size(); curNode++)
				{
					
					//System.out.println("IN FOR "+nodeList.get(curNode));
					//LOG.info("NODE VALUES FOR AWARD ARE "+nodeList.get(curNode));
					String currentNodeValue=nodeList.get(curNode);
					
					String category="";
					String conferenceId="";
					String conferenceDate="";
					String conferenceTitle="";
					String conferenceLocation="";
					String conferenceLink="";
					if(curNode<(nodeList.size()-1))
					{
						String nextNodeValue=nodeList.get(curNode+1);
							if(currentNodeValue.equalsIgnoreCase("Date")&&nextNodeValue.equalsIgnoreCase("Department"))
							{
								flagToBreak1If=true;
								
								LOG.info("IF OF Conference Date-department NODE REACHED "+currentNodeValue+nextNodeValue);
								System.out.println("NODE VALUES FOR Conference Date-department ARE"+currentNodeValue+nextNodeValue);
								for(int nextNodes=(curNode+3);nextNodes<nodeList.size();nextNodes+=4)
								{
									String nodeval=nodeList.get(nextNodes);
									if(nodeval.equalsIgnoreCase("showpdf.aspx?src1=upload/Hyper Linking Policy.pdf"))
									{
										flag=true;
										break;
									}
									else
									{
										ConferenceBean conferenceBean=new ConferenceBean();
										conferenceDate=nodeList.get(nextNodes);
										conferenceLocation=nodeList.get(nextNodes+1);
										conferenceLink=nodeList.get(nextNodes+2);
										conferenceTitle=nodeList.get(nextNodes+3);
										
										LOG.info("IN FOR OF Conference Date-department NODE=====Date "+conferenceDate);
										LOG.info("IN FOR OF Conference Date-department NODE=====location "+conferenceLocation);
										LOG.info("IN FOR OF Conference Date-department NODE=====url "+conferenceLink);
										LOG.info("IN FOR OF Conference Date-department NODE=====title "+conferenceTitle);
										
										
										SimpleDateFormat oldSdf=new SimpleDateFormat("dd/MM/yyyy");
										SimpleDateFormat newSdf=new SimpleDateFormat("MMM dd, yyyy");
										Date olddate=(Date)oldSdf.parse(conferenceDate);
										conferenceDate=newSdf.format(olddate);
										conferenceId=conferenceDate+conferenceTitle.replaceAll("\\s+","");
										
										
										
										LOG.info("IN FOR OF Conference Date-department NODE=====ID "+conferenceId);
										category="VA-CON";
										conferenceBean.setConferenceId(conferenceId);
										conferenceBean.setConferenceTitle(conferenceTitle);
										conferenceBean.setConferenceDescription("N.A.");
										conferenceBean.setConferenceLocation(conferenceLocation);
										conferenceBean.setConferenceDate(conferenceDate);
										conferenceBean.setConferenceUrl(conferenceUrl);
										conferenceBean.setConferenceLink(conferenceLink); 
										conferenceBean.setIdCode(category);
										conferenceBean.setResourceCode("VALUE"); 
										//conferenceBean.setCategory(category); 
										conferenceBeanList.add(conferenceBean);
									
									}
									LOG.info("FOR OF conferenceUrl NODE ENDED ");
									
								}
								if(flag==true)
									break;
							}
				    }
					if(currentNodeValue.equalsIgnoreCase("Seminar")||currentNodeValue.equalsIgnoreCase("Conference"))
					{
						//flagToBreak2If=true;
						category=currentNodeValue;
						if(category.equalsIgnoreCase("Conference"))
							category="VA-CON";
						if(category.equalsIgnoreCase("Seminar"))
							category="VA_SEM";
						
						boolean dFlag=false;
						if(curNode<(nodeList.size()-1))
						{
							conferenceTitle=nodeList.get(curNode+1);
							for(int nextNodes=(curNode+2);nextNodes<nodeList.size();nextNodes++)
							{
								if(nodeList.get(nextNodes).equalsIgnoreCase("Date"))
								{
									conferenceDate=nodeList.get(nextNodes+1);
									SimpleDateFormat oldSdf=new SimpleDateFormat("EEE, dd MMM, yyyy");
									SimpleDateFormat newSdf=new SimpleDateFormat("dd MMM yyyy");
									Date olddate=(Date)oldSdf.parse(conferenceDate);
									conferenceDate=newSdf.format(olddate);
								}
								
								if(nodeList.get(nextNodes).equalsIgnoreCase("Venue"))
								{
									conferenceLocation=nodeList.get(nextNodes+1);
									dFlag=true;
								}
							
								if(dFlag==true)
									break;	
							}
							ConferenceBean conferenceBean=new ConferenceBean();
							conferenceId=conferenceDate+conferenceTitle.replaceAll("\\s+","");
							conferenceBean.setConferenceId(conferenceId);
							conferenceBean.setConferenceTitle(conferenceTitle);
							conferenceBean.setConferenceDescription(conferenceTitle);
							conferenceBean.setConferenceLocation(conferenceLocation);
							conferenceBean.setConferenceDate(conferenceDate);
							conferenceBean.setConferenceUrl(conferenceUrl);
							conferenceBean.setConferenceLink("N.A."); 
							conferenceBean.setIdCode(category);
							conferenceBean.setResourceCode("VALUE"); 
							
							conferenceBeanList.add(conferenceBean);
						}
					}
					if(currentNodeValue.equalsIgnoreCase("Conference Name")||currentNodeValue.equalsIgnoreCase("Conference Date"))
					{
						
						category="VA-CON";
						
												
						if(curNode<(nodeList.size()-1))
						{
							//conferenceTitle=nodeList.get(curNode+1);
							int nextNodes=0;
							for(nextNodes=(curNode+2);nextNodes<nodeList.size();nextNodes++)
							{
								
								System.out.println("NODE VALUES AFTER REMOVAL ARE at 241*** "+nodeList.get(nextNodes));
								if(nodeList.get(nextNodes).contains("conferences"))
								{
									ConferenceBean conferenceBean=new ConferenceBean();
									conferenceLink=nodeList.get(nextNodes);
									conferenceTitle=nodeList.get(nextNodes+1);
									conferenceDate=nodeList.get(nextNodes+3);
									/*SimpleDateFormat oldSdf=new SimpleDateFormat("EEE, dd MMM, yyyy");
									SimpleDateFormat newSdf=new SimpleDateFormat("MMM dd, yyyy");
									Date olddate=(Date)oldSdf.parse(conferenceDate);
									conferenceDate=newSdf.format(olddate);//
									StringBuilder sb=new StringBuilder();
									sb.append("");
									
									for(int newNextNode=(nextNodes+5);newNextNode<nodeList.size();newNextNode++)
									{
										System.out.println("NEXT LOOP FOR LOCATION at 255*** "+nodeList.get(newNextNode));
										if(nodeList.get(newNextNode).matches(".*?\\bconferences\\b.*?")) 
										{
											System.out.println("NODE COntains conference at 258*** "+nodeList.get(newNextNode));
											nextNodes=newNextNode-1;
											break;
										}
										/*else if(nodeList.get(newNextNode).matches(".*?\\bMeeting\\b.*?"))
										{
											System.out.println("NODE COntains meeting at 264*** "+nodeList.get(newNextNode));
											break;
										}
										else if(nodeList.get(newNextNode).matches(".*?\\bIEEE\\b.*?"))
										{
											System.out.println("NODE COntains Robotics at 269*** "+nodeList.get(newNextNode));
											break;
										}//
										else
										{
											sb.append(nodeList.get(newNextNode));
											sb.append(" ");
										}
										if(newNextNode==nodeList.size()-1)
										{
											flagToBreak2For=true;
										}
										
									}
									conferenceLocation=sb.toString();
									System.out.println("conferenceTitle*** "+conferenceTitle);
									System.out.println("conferenceLocation*** "+conferenceLocation);
									System.out.println("conferenceDate*** "+conferenceDate);
									System.out.println("conferenceLink*** "+conferenceLink);
									conferenceId=conferenceDate+conferenceTitle.replaceAll("\\s+","");
									conferenceBean.setConferenceId(conferenceId);
									conferenceBean.setConferenceTitle(conferenceTitle);
									conferenceBean.setConferenceDescription(conferenceTitle);
									conferenceBean.setConferenceLocation(conferenceLocation);
									conferenceBean.setConferenceDate(conferenceDate);
									conferenceBean.setConferenceUrl(conferenceUrl);
									conferenceBean.setConferenceLink(conferenceLink); 
									conferenceBean.setIdCode(category);
									conferenceBean.setResourceCode("VALUE"); 
									conferenceBeanList.add(conferenceBean);
									
									if(flagToBreak2For==true )
										break;
								}
								
								
							}
							
						}
					}
					if(flagToBreak1If==true )
						break;
				}
			}*/
			return conferenceBeanList;	
		} catch (Exception e)/*| XPathExpressionException e) {
			e.printStackTrace();
		} catch (ParseException e)*/ {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conferenceBeanList;	
	}
	

}
