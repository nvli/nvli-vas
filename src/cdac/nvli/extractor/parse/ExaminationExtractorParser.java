package cdac.nvli.extractor.parse;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cdac.nvli.extractor.bean.ExaminationBean;
public class ExaminationExtractorParser {

	private ArrayList<ExaminationBean> examBeanList; 
	private static final Log LOG = LogFactory
			.getLog(ExaminationExtractorParser.class.getName());
	
	
		public ArrayList<ExaminationBean> urlParser(String url) throws IOException, ParseException
		{
			examBeanList=new ArrayList<ExaminationBean>();
			URL url1 = new URL(url);
			System.out.println(url1);
			Document doc = Jsoup.parse(url1, 1000);
			Elements table=doc.select("table");	
			
			if(url.equals("http://upsc.gov.in/examinations/exam-calendar"))
			{
				Elements title=doc.select("title");
				String[] splittedParts = title.text().split("(?=UPSC)");
				String category=null;
				String examdate="";
				 for(String s:splittedParts)
				 {
					 if(s.equals("UPSC"))
					 {
					 category=s+" ";
					 }
				 }
				String urlName="";
				
				Elements table1=doc.select("table");
				Element col=table1.select("td").get(0);
				Elements links=col.select("a[href]");
				
				ExaminationBean examBean=new ExaminationBean();
				
				for(Element link:links)
				{
					urlName=link.absUrl("href");
					examBean.setLink(urlName);
				}
				
				String examTitle=category+col.text();
				String examDate="";
				
				
				/*if(examDate.isEmpty())
				{
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					Date date = new Date();
					examdate=dateFormat.format(date);
				}*/
				
				examBean.setTitle(examTitle);
				examBean.setDate(examdate);
				examBean.setId(examdate+examTitle);
				examBean.setIdentifier("VALUE_"+examdate+"_"+examTitle.replaceAll(" ",""));
				examBean.setDescription(""); 
				examBean.setUrl(url); 
				examBean.setIdCode("VA-EXA");
				examBean.setResourceCode("VALUE");
				
				examBeanList.add(examBean);
				
			}
			
			else if(url.equals("http://www.neet2017nic.in/neet-2017-exam-dates.html"))
			{
				Elements table1=doc.select("table");
				Elements rows=table1.select("tr");
				String finalDate="";
				for(int i=5;i<rows.size();i++)
				{
					Element r=rows.get(i);
					if(r.equals(rows.get(5)))
							{
						    Elements col_name=r.select("td:eq(0)");
						    Elements col_date=r.select("td:eq(1)");
						    Elements s=col_date.select("sup");
						    s.remove();
						    
						    String dateString=col_date.text().replaceFirst(String.valueOf((char) 160),"").trim();
						    DateFormat oldDateParser =  new SimpleDateFormat("dd MMM yyyy"); 
						    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate= newDateParser.format(oldDateParser.parse(dateString)); 
			
							  String examDate=finalDate;
							  String examTitle=col_name.text();
	
							  ExaminationBean examBean=new ExaminationBean();
								
							 	examBean.setId(examDate+examTitle);
								examBean.setIdentifier("VALUE_"+examDate+"_"+examTitle.replaceAll(" ",""));
								examBean.setTitle(examTitle); 
								examBean.setDescription(""); 
								examBean.setDate(examDate);
								examBean.setUrl(url); 
								examBean.setLink(""); 
								examBean.setIdCode("VA-EXA");
								examBean.setResourceCode("VALUE");
								examBeanList.add(examBean);
							  
							}
					
				}
			
			}
			
			else if(url.equals("http://www.neet2017nic.in/aiims-2017-application-form-exam-date-answer-key-admit-card-result-aiimsexams-org.html"))
			{
				Element table1=doc.select("table").get(0);
				Elements rows=table1.select("tr");
				String finalDate="";
				for(int i=0;i<rows.size();i++)
				{
					Element r=rows.get(i);
					if(r.equals(rows.get(4)))
					{
					Elements col=r.select("td:eq(0)");
					Elements col_date=r.select("td:eq(1)");
					Elements s=col_date.select("sup");
				    s.remove();
				 
				    String dateString=col_date.text().replaceFirst(String.valueOf((char) 160),"").trim();
				    DateFormat oldDateParser = new SimpleDateFormat("dd MMM yyyy"); 
				    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					finalDate= newDateParser.format(oldDateParser.parse(dateString)); 
				    
					 String examDate=finalDate;
					 String examTitle=col.text();

					  ExaminationBean examBean=new ExaminationBean();
						
					 	examBean.setId(examDate+examTitle);
						examBean.setIdentifier("VALUE_"+examDate+"_"+examTitle.replaceAll(" ",""));
						examBean.setTitle(examTitle); 
						examBean.setDescription(""); 
						examBean.setDate(examDate);
						examBean.setUrl(url); 
						examBean.setLink(""); 
						examBean.setIdCode("VA-EXA");
						examBean.setResourceCode("VALUE");
						examBeanList.add(examBean);
					}
				}
			}
			
			else if(url.equals("http://xatonline.net.in/"))
			{
				Elements title=doc.select("title");
				Elements table1=doc.select("table");
				Elements rows=table1.select("tr");
				String finalDate="";
				for(int i=0;i<rows.size();i++)
				{
					
					Element r=rows.get(i);
					if(r.equals(rows.get(5)))
					{
					Elements col=r.select("td");
					Elements col_date=r.select("td:eq(1)");
					Elements col_name=r.select("td:eq(0)");
					Elements s=col_date.select("sup");
				    s.remove();
				    
				    String dateString=col_date.text().replaceFirst(String.valueOf((char) 160),"").trim();
				    DateFormat oldDateParser = new SimpleDateFormat("dd MMMM yyyy"); 
				    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					finalDate= newDateParser.format(oldDateParser.parse(dateString)); 
					
					String examDate=finalDate;
					String examTitle=title.text();
					  
					ExaminationBean examBean=new ExaminationBean();
						
					 	examBean.setId(examDate+examTitle);
						examBean.setIdentifier("VALUE_"+examDate+"_"+examTitle.replaceAll(" ",""));
						examBean.setTitle(examTitle); 
						examBean.setDescription(""); 
						examBean.setDate(examDate);
						examBean.setUrl(url); 
						examBean.setLink(""); 
						examBean.setIdCode("VA-EXA");
						examBean.setResourceCode("VALUE");
						examBeanList.add(examBean);
					}
				}
			}
			
			else if(url.equals("http://ceed.iitb.ac.in/2017/"))
			{
				Elements title=doc.select("title");
			    Elements class_date=doc.getElementsByClass("imp-date-header");
			    Elements tag_h4=class_date.select("h4");
			    Elements tag_para=class_date.select("p");
			    String finalDate="";
			    for(int j=0;j<tag_para.size();j++)
			    {
			    	Element paragraph=tag_para.get(j);
			    	if(paragraph.equals(tag_para.get(4)))
			    	{
			 
			    for(int i=0;i<tag_h4.size();i++)
			    {
			    	Element head_4=tag_h4.get(i);
			    	if(head_4.equals(tag_h4.get(3)))
			    	{
			    		 String str=null;
			    		 str=paragraph.text();
			    		 str=str.replace(", 10:00 to 13:00 hours","");
			    		
			    		  String dateString=str.replaceFirst(String.valueOf((char) 160),"").trim();
					      DateFormat oldDateParser = new SimpleDateFormat("MMMM dd, yyyy");
					      SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						  newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						  finalDate= newDateParser.format(oldDateParser.parse(dateString)); 
		
						  String examDate=finalDate;
						  String examTitle=title.text();
						  
						  ExaminationBean examBean=new ExaminationBean();
							
						 	examBean.setId(examDate+examTitle);
							examBean.setIdentifier("VALUE_"+examDate+"_"+examTitle.replaceAll(" ",""));
							examBean.setTitle(examTitle); 
							examBean.setDescription(""); 
							examBean.setDate(examDate);
							examBean.setUrl(url); 
							examBean.setLink(""); 
							examBean.setIdCode("VA-EXA");
							examBean.setResourceCode("VALUE");
							examBeanList.add(examBean);
			    		
			    	}
			    }
			    	}
			    }
			}
			
			else if(url.equals("http://www.gate.iitr.ernet.in/?page_id=550"))
			{
				Elements table1=doc.select("table");
				Elements rows=table1.select("tr");
				String str="";
				String finalDate="";
				String title="";
				String examTitle="";
				ExaminationBean examBean=new ExaminationBean();
				
				for(int i=0;i<rows.size();i++)
				{
				
					Element row=rows.get(2);
					Element row0=rows.get(0);
					Elements col1=row.select("td:eq(0)");
					
					str=col1.text();
					title=row0.text();
				}
				
				str=str.replace("th", "");
				str=str.replace("(Saturday)", "");
				
				 String[] splittedParts = title.split("(?= Examination Schedule)");
	    		 
    			 for(String s:splittedParts) {
    				 if(s.equals("GATE 2017"))
    				 {
    		           examTitle=s;
    				 }
    			 }
				
				String dateString=str.replaceFirst(String.valueOf((char) 160),"").trim();
			    DateFormat oldDateParser =new SimpleDateFormat("dd MMMM yyyy");  
			    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				finalDate= newDateParser.format(oldDateParser.parse(dateString));
				
				examBean.setTitle(examTitle);
				examBean.setDate(finalDate);
				examBean.setId(finalDate+examTitle);
				examBean.setIdentifier("VALUE_"+finalDate+"_"+examTitle);
				examBean.setDescription(""); 
				examBean.setUrl(url); 
				examBean.setLink(""); 
				examBean.setIdCode("VA-EXA");
				examBean.setResourceCode("VALUE");
			    
				examBeanList.add(examBean);
			}
			
			else if(url.equals("http://clat.ac.in/index.php/8-general/23-clat-2017-calendar"))
			{
				 Elements table_clat=doc.select("table");
				 Elements rows=table_clat.select("tr");
				 String finalDate="";
				    for(int i=0;i<rows.size();i++)
				    {
				    	Element row=rows.get(i);
				    	if(row.equals(rows.get(5)))
				    	{
				    		Elements col_name=row.select("td:eq(1)");
				    		String str2=null;
				    		str2=col_name.text();
				    		str2=str2.replaceAll("Date of", "");
				    		//System.out.println(str2.trim());
				    		Elements col=row.select("td:eq(2)");
				    		String str=null;
				    		str=col.text();
				    		str=str.replace("(Sunday) from 03:00 pm to 05:00 pm","");
				    		str=str.replace("th", "");
				    
				    		String dateString=str.replaceFirst(String.valueOf((char) 160),"").trim();
						    DateFormat oldDateParser =new SimpleDateFormat("dd MMM, yyyy"); 
						    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate= newDateParser.format(oldDateParser.parse(dateString)); 
			
							  String examDate=finalDate;
							  String examTitle=str2.trim();
							  
							  ExaminationBean examBean=new ExaminationBean();
								
							 	examBean.setId(examDate+examTitle);
								examBean.setIdentifier("VALUE_"+examDate+"_"+examTitle.replaceAll(" ",""));
								examBean.setTitle(examTitle); 
								examBean.setDescription(""); 
								examBean.setDate(examDate);
								examBean.setUrl(url); 
								examBean.setLink(""); 
								examBean.setIdCode("VA-EXA");
								examBean.setResourceCode("VALUE");
								
								examBeanList.add(examBean);
				    	}
				    }
			}
			
			else if(url.equals("http://www.jeeadv.ac.in/exam-schedule"))
			{
				  Elements name_exam=doc.select("title");
				  Elements table1=doc.select("table");
				  Elements rows=table1.select("tr");
				  String finalDate="";
				  for(int i=1;i<rows.size();i++)
				  {
					  Element r=rows.get(i);
					  if(r.equals(rows.get(1)))
					  {
					  Elements col=r.select("td:eq(2)");
					  
					  
					  String dateString=col.text().replaceFirst(String.valueOf((char) 160),"").trim();
				      DateFormat oldDateParser = new SimpleDateFormat("dd-MMM-yyyy"); 
				      SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					  newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					  finalDate= newDateParser.format(oldDateParser.parse(dateString)); 
	
					  String examDate=finalDate;
					  String examTitle=name_exam.text();
					  
					  ExaminationBean examBean=new ExaminationBean();
						
					 	examBean.setId(examDate+examTitle);
						examBean.setIdentifier("VALUE_"+examDate+"_"+examTitle.replaceAll(" ",""));
						examBean.setTitle(examTitle); 
						examBean.setDescription(""); 
						examBean.setDate(examDate);
						examBean.setUrl(url); 
						examBean.setLink(""); 
						examBean.setIdCode("VA-EXA");
						examBean.setResourceCode("VALUE");
						
						examBeanList.add(examBean);
					  }
				  }
			}
			
			
			else if(url.equals("http://www.freejobalert.com/upcoming-exam-dates-of-various-jobs/1835/"))
			{
				Elements class1=doc.getElementsByClass("PostContent");
				Elements p=class1.select("p");
				String finalDate="";
				for(int j=1;j<p.size();j++)
				{
					Element title=p.get(j);
					try{
						if(!title.text().equals("UPSC"))
						{
						Element tables=title.nextElementSibling();
						
							if(tables.tagName().equals("table"))
							{
								Elements rows=tables.select("tr");
								for(int i=1;i<rows.size();i++)
								{
								  Element row=rows.get(i);
								  
								  ExaminationBean examBean=new ExaminationBean();


									examBean.setrecruitment_location(title.text());
								    Elements date=row.select("td:eq(0)");
									Elements examName=row.select("td:eq(2)");
									String str="";
									str=date.text();

									String dateString=str.replaceFirst(String.valueOf((char) 160),"").trim();

									if(str.trim().matches("[0-9]{2}/[0-9]{2}/[0-9]{4}"))
									{
									   DateFormat oldDateParser = new SimpleDateFormat("dd/MM/yyyy"); 
									   SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									   newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
									   finalDate= newDateParser.format(oldDateParser.parse(dateString));
									   examBean.setDate(finalDate);
									   
									   if(title.text().equals("SSC"))
										{
											Elements exName=row.select("td:eq(1)");
											examBean.setTitle(title.text()+" "+exName.text());
											examBean.setId(finalDate+title.text()+" "+exName.text());
											examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+exName.text());
										}
										if(!title.text().equals("SSC"))
										{
											Elements col2=row.select("td:eq(1)");
											examBean.setTitle(title.text()+" "+examName.text());
											examBean.setrecruitment_board(col2.text());
											examBean.setId(finalDate+title.text()+" "+examName.text());
											examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+examName.text());
										}
									}
									else if(str.trim().matches("[A-Za-z]{0,12}, [0-9]{4}"))
									{
										DateFormat oldDateParser = new SimpleDateFormat("MMMM, yyyy"); 
										SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
										finalDate= newDateParser.format(oldDateParser.parse(dateString));
										examBean.setDate(finalDate);
										
										if(title.text().equals("SSC"))
										{
											Elements exName=row.select("td:eq(1)");
											examBean.setTitle(title.text()+" "+exName.text());
											examBean.setId(finalDate+title.text()+" "+exName.text());
											examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+exName.text());
										}
										if(!title.text().equals("SSC"))
										{
											Elements col2=row.select("td:eq(1)");
											examBean.setTitle(title.text()+" "+examName.text());
											examBean.setrecruitment_board(col2.text());
											examBean.setId(finalDate+title.text()+" "+examName.text());
											examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+examName.text());
										}
									}
									else if(str.contains("&"))
									{
										String[] sp=str.split("&");
										String str2="";
										for(String spi:sp)
										{
											if(!spi.equals(sp[0]))
											{
												str2=spi.trim();
											}
											String dateString1=str2.replaceFirst(String.valueOf((char) 160),"").trim();
											if(str2.trim().matches("[0-9]{2}/[0-9]{2}/[0-9]{4}"))
											{
												DateFormat oldDateParser = new SimpleDateFormat("dd/MM/yyyy"); 
												SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
												newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
												finalDate= newDateParser.format(oldDateParser.parse(dateString1));
												examBean.setDate(finalDate);
												
												if(title.text().equals("SSC"))
												{
													Elements exName=row.select("td:eq(1)");
													examBean.setTitle(title.text()+" "+exName.text());
													examBean.setId(finalDate+title.text()+" "+exName.text());
													examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+exName.text());
												}
												if(!title.text().equals("SSC"))
												{
													Elements col2=row.select("td:eq(1)");
													examBean.setTitle(title.text()+" "+examName.text());
													examBean.setrecruitment_board(col2.text());
													examBean.setId(finalDate+title.text()+" "+examName.text());
													examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+examName.text());
												}

											}
										}
									}
									else if(str.contains("to"))
									{
										String[] sp=str.split("to");
										String str2="";
										for(String spi:sp)
										{
											if(!spi.equals(sp[0]))
											{
												str2=spi.trim();
											}
											String dateString1=str2.replaceFirst(String.valueOf((char) 160),"").trim();
											if(str2.trim().matches("[0-9]{2}/[0-9]{2}/[0-9]{4}"))
											{
												DateFormat oldDateParser = new SimpleDateFormat("dd/MM/yyyy"); 
												SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
												newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
												finalDate= newDateParser.format(oldDateParser.parse(dateString1));
												examBean.setDate(finalDate);
												
												if(title.text().equals("SSC"))
												{
													Elements exName=row.select("td:eq(1)");
													examBean.setTitle(title.text()+" "+exName.text());
													examBean.setId(finalDate+title.text()+" "+exName.text());
													examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+exName.text());
												}
												if(!title.text().equals("SSC"))
												{
													Elements col2=row.select("td:eq(1)");
													examBean.setTitle(title.text()+" "+examName.text());
													examBean.setrecruitment_board(col2.text());
													examBean.setId(finalDate+title.text()+" "+examName.text());
													examBean.setIdentifier("VALUE_"+finalDate+"_"+title.text()+" "+examName.text());
												}
											}
										}
									}
									examBean.setUrl(url);  
									examBean.setIdCode("VA-EXA");
									examBean.setResourceCode("VALUE");
									
									if(!examBean.getId().equals("null"))
									examBeanList.add(examBean);
									
								}
							}
						}
						
					}
					catch(Exception ex)
					{
						continue;
					}
				}
			}
			
			else if(url.equals("http://career.webindia123.com/career/exams/examdate.asp"))
			{
				doc.getElementsByClass("hpmheadw").remove();
				doc.getElementsByClass("sptxt").remove();
				String examCategory="";
				Elements tables=doc.select("table");
				for(int i=4;i<tables.size();i=i+3)
				{
					Element table1=tables.get(i);
					try{
						Element net=table1.previousElementSibling();
						Element net2=net.previousElementSibling();
						if(net2.tagName().equals("table"))
						{
							examCategory=net2.text();
						}
					}
					catch(Exception ex)
					{
						continue;
					}
					
					Elements rows=table1.select("tr");
					for(int j=0;j<rows.size();j++)
					{
							Element row=rows.get(j);
							if(!row.text().contains("Follow @webindia123edu") && !row.text().contains("Completed Applications") && !row.text().contains(" -") )
							{
								Elements date=row.select("td:eq(0)");
								Elements name=row.select("td:eq(2)");
								Elements appDate=row.select("td:eq(1)");
								//System.out.println(appDate.text().replace("\u00a0", ""));
								
								
								String str="";
								String str1="";
								String finalDate="";
								String finalDate1="";
								
								str=date.text();
								str1=appDate.text();
								ExaminationBean examBean=new ExaminationBean();
								
								examBean.setotherInformation("Last Date of Application "+appDate.text().replace("\u00a0", ""));
								/*String dateString2=str1.replaceFirst(String.valueOf((char) 160),"").trim();
								if(str1.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}")||
								   str1.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}")	||
								   str1.trim().matches("[A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}")||
								   str1.trim().matches("[A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}"))
										{
											DateFormat oldDateParser = new SimpleDateFormat("MMM. dd, yyyy"); 
									   	    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd");
										    newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
										    finalDate1=newDateParser.format(oldDateParser.parse(dateString2));
										    //examBean.setotherInformation("Last Date of Application "+finalDate1);
										}*/

								String dateString=str.replaceFirst(String.valueOf((char) 160),"").trim();
								if(str.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}")||str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}"))
					            {
									DateFormat oldDateParser = new SimpleDateFormat("MMM.dd,yyyy"); 
							   	    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								    newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
								    finalDate= newDateParser.format(oldDateParser.parse(dateString));
								    examBean.setDate(finalDate);
								    examBean.setId(finalDate+examCategory+" "+name.text());
								    examBean.setIdentifier("VALUE_"+finalDate+"_"+examCategory+" "+name.text());
									
					            }
								/*else if(str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}"))
								{
								  DateFormat oldDateParser = new SimpleDateFormat("MMM.dd,yyyy"); 
								  SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								  newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
								  finalDate= newDateParser.format(oldDateParser.parse(dateString));
								  examBean.setDate(finalDate);
							      examBean.setId(finalDate+examCategory+" "+name.text());
								  examBean.setIdentifier("VALUE_"+finalDate+"_"+examCategory+" "+name.text());
									
								}*/
								else if(str.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}")||str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}"))
								{
									String[] sp=str.split("-");
									for(String stri:sp)
									{
										if(!stri.equals(sp[0]))
										{
											str=stri.trim();
										}
									}
									String dateString1=str.replaceFirst(String.valueOf((char) 160),"").trim();
									DateFormat oldDateParser = new SimpleDateFormat("MMM.dd,yyyy"); 
									SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
									finalDate= newDateParser.format(oldDateParser.parse(dateString1));
									examBean.setDate(finalDate);
									examBean.setId(finalDate+examCategory+" "+name.text());
								    examBean.setIdentifier("VALUE_"+finalDate+"_"+examCategory+" "+name.text());
									
								}
								/*else if(str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}"))
								{
									
									String[] sp=str.split("-");
									for(String stri:sp)
									{
										if(!stri.equals(sp[0]))
										{
											str=stri.trim();
										}
									}
									String dateString1=str.replaceFirst(String.valueOf((char) 160),"").trim();
									DateFormat oldDateParser = new SimpleDateFormat("MMM.dd,yyyy"); 
									SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
									finalDate= newDateParser.format(oldDateParser.parse(dateString1));
									examBean.setDate(finalDate);
									examBean.setId(finalDate+examCategory+" "+name.text());
								    examBean.setIdentifier("VALUE_"+finalDate+"_"+examCategory+" "+name.text());
									
								}*/
								else if(str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}")||str.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}"))
								{
									String[] sp=str.split("-");
									for(String stri:sp)
									{
										if(!stri.equals(sp[0]))
										{
											str=stri.trim();
										}
									}
									String dateString1=str.replaceFirst(String.valueOf((char) 160),"").trim();
									DateFormat oldDateParser = new SimpleDateFormat("MMM.dd,yyyy"); 
									SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
									finalDate= newDateParser.format(oldDateParser.parse(dateString1));
									examBean.setDate(finalDate);
									examBean.setId(finalDate+examCategory+" "+name.text());
								    examBean.setIdentifier("VALUE_"+finalDate+"_"+examCategory+" "+name.text());
									
								}
								/*else if(str.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}"))
								{
									String[] sp=str.split("-");
									for(String stri:sp)
									{
										if(!stri.equals(sp[0]))
										{
											str=stri.trim();
										}
									}
									String dateString1=str.replaceFirst(String.valueOf((char) 160),"").trim();
									DateFormat oldDateParser = new SimpleDateFormat("MMM.dd,yyyy"); 
									SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									newDateParser.setTimeZone(TimeZone.getTimeZone("IST")); 
									finalDate= newDateParser.format(oldDateParser.parse(dateString1));
									examBean.setDate(finalDate);
									examBean.setId(finalDate+examCategory+" "+name.text());
								    examBean.setIdentifier("VALUE_"+finalDate+"_"+examCategory+" "+name.text());
									
								}*/
								else 
								{
									if(!str.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}") && 
									   !str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}") &&
									   !str.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}")&&
									   !str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}") &&
									   !str.trim().matches(" [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4}") &&
									   !str.trim().matches(" [A-Z,a-z]{3}. [0-9]{2}, [0-9]{4} - [A-Z,a-z]{3}. [0-9]{1}, [0-9]{4}"))
									{
										DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										Date date1 = new Date();
										str=dateFormat.format(date1);
										//examBean.setDate(str);
										str="";
										examBean.setId(str+examCategory+" "+name.text());
								        examBean.setIdentifier("VALUE_"+str+"_"+examCategory+" "+name.text());
										
									}	
								}
							
								examBean.setTitle(examCategory+" "+name.text());
								examBean.setUrl(url); 
								examBean.setLink(""); 
								examBean.setIdCode("VA-EXA");
								examBean.setResourceCode("VALUE");
								
								examBeanList.add(examBean);			
							}
						
					}
			
				}
				
		    }

	LOG.info("Generated ExamBeanList is==>"+examBeanList);
	return examBeanList;
  }
}
