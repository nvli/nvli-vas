package cdac.nvli.extractor.parse;

import java.util.ArrayList;

import cdac.nvli.extractor.bean.ActsBean;
import cdac.nvli.extractor.bean.SchemeBean;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.accessibility.AccessibleExtendedTable;
import javax.naming.LinkLoopException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.util.SystemOutLogger;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;

import com.wuman.jreadability.Readability;

public class ActsExtractorParser {
	private static final String XPATH_EXPRESSION_EVENT_TITLE = "/html/head/title";
	private Document doc;
	private W3CDom w3cDom;
	private ArrayList<String> nodeList;
	private ArrayList<ActsBean> ActsBeanList; 
	private boolean flag=false;
	private String actsDocTitle;
	
	private String actsTitle;
	private String actsDescription;
	private String actsUrl;
	
	private String actsId;
	private String actsIdentifier;
	/**
	 * Logger
	 */
	private static final Log LOG = LogFactory
			.getLog(ActsExtractorParser.class.getName());
	

	public ArrayList<ActsBean> urlParser(String url) throws IOException, ParseException{
		
		ActsBeanList=new ArrayList<ActsBean>();
		URL url1 = new URL(url);
		Document doc = Jsoup.parse(url1, 1000000);
		
		
		if(url.equals("http://doj.gov.in/acts-and-rules"))
		{
			Elements class1=doc.getElementsByClass("view-content");
			Elements h3=class1.select("h3");
			String finalDate="";
			for(int i=0;i<h3.size();i++)
			{
				Element heading=h3.get(i);
				Element ne=heading.nextElementSibling();
				Elements content=ne.select("table");
				Elements tr=content.select("tr");
				for(int j=1;j<tr.size();j++)
				{
					Element rows=tr.get(j);
					ActsBean actsBean=new ActsBean();
					/*if(finalDate.isEmpty())
					{
						DateFormat dateFormat = new SimpleDateFormat("9999-00-00'T'HH:mm:ss'Z'");
					    Date date = new Date();
					   // finalDate=dateFormat.format(date);
						//finalDate="9999-00-00'T'HH:mm:ss'Z'";
					}*/
					actsBean.setDate(finalDate);
					
					Elements title=rows.select("td:eq(1)");
					String headingTrim="";
					String[] sp=heading.text().split("\\.");
					for(String str:sp)
					{
						if(!str.equals(sp[0]))
						{
							
							headingTrim=str.trim();
						}
					}
					
					actsBean.setTitle("Ministry of Law and Justice "+title.text());
					actsBean.setministry("Ministry of Law and Justice");
					actsBean.setotherInformation("Department of Justice " +headingTrim);
					
					Elements downloads=rows.select("td:eq(2)");
					Elements links=downloads.select("a[href]");
					for(Element link:links)
					{
						actsBean.setLink(link.absUrl("href"));
					}
					
                     actsBean.setId(finalDate+"Ministry of Law and Justice "+title.text());
                     actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Law and Justice "+title.text() );
					 actsBean.setIdCode("VA-ACT");
		             actsBean.setResourceCode("VALUE");
		             actsBean.setUrl(url);
		             
                    ActsBeanList.add(actsBean);					
					
				}
			}
		}
		
		else if(url.equals("http://dea.gov.in/acts-rules")||
				url.equals("http://dea.gov.in/acts-rules?page=1")||
				url.equals("http://dea.gov.in/acts-rules?page=2")||
				url.equals("http://dea.gov.in/acts-rules?page=3")||
				url.equals("http://dea.gov.in/acts-rules?page=4")||
				url.equals("http://dea.gov.in/acts-rules?page=5"))
		{
			Elements table=doc.select("table");
			Elements rows=table.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				String finalDate="";
	
				ActsBean actsBean=new ActsBean();
				
				Element row=rows.get(i);
				
				Elements title=row.select("td:eq(0)");
				Elements acts=row.select("td:eq(1)");
				Elements rules=row.select("td:eq(2)");
				
				actsBean.setTitle("Ministry of Finance "+ title.text());
				actsBean.setministry("Ministry of Finance");
				actsBean.setotherInformation("Department of Economic Affairs");
				
				Elements links=acts.select("a[href]");
				for(Element link:links)
				{
					actsBean.setLink(link.absUrl("href"));
				}
				
//				if(finalDate.isEmpty())
//				{
//					DateFormat dateFormat = new SimpleDateFormat("9999-00-00'T'HH:mm:ss'Z'");
//				    Date date = new Date();
//				   // finalDate=dateFormat.format(date);
//				    //finalDate="0000-00-00HH:mm:ss";
//
//				}
				
	            if(url.equals("http://dea.gov.in/acts-rules?page=1"))
	            {
	            	actsBean.setUrl(url);
	            }
	            else if(url.equals("http://dea.gov.in/acts-rules"))
	            {
	            	actsBean.setUrl(url);
	            }
	            else if(url.equals("http://dea.gov.in/acts-rules?page=2"))
	            {
	            	actsBean.setUrl(url);
	            }
	            else if(url.equals("http://dea.gov.in/acts-rules?page=3"))
	            {
	            	actsBean.setUrl(url);
	            }
	            else if(url.equals("http://dea.gov.in/acts-rules?page=4"))
	            {
	            	actsBean.setUrl(url);
	            }
	            else if(url.equals("http://dea.gov.in/acts-rules?page=5"))
	            {
	            	actsBean.setUrl(url);
	            }
	            
				actsBean.setDate(finalDate);
				actsBean.setId(finalDate+"Ministry of Finance "+ title.text());
                actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Finance "+ title.text());
				actsBean.setDate(finalDate);
				actsBean.setIdCode("VA-ACT");
	            actsBean.setResourceCode("VALUE");
	            
	            ActsBeanList.add(actsBean);
			}
		}
		
		else if(url.equals("http://www.mca.gov.in/MinistryV2/companiesdonationtonationalfundact1951.html")||
				url.equals("http://www.mca.gov.in/MinistryV2/llpact.html")||
				url.equals("http://www.mca.gov.in/MinistryV2/competitionact.html")||
				url.equals("http://www.mca.gov.in/MinistryV2/thecompanysecretariesact.html")||
				url.equals("http://www.mca.gov.in/MinistryV2/societiesregistrationact.html")||
				url.equals("http://www.mca.gov.in/MinistryV2/companiesdonationtonationalfundact1951.html"))
		{
			Elements title=doc.getElementsByTag("h2");
			Element id=doc.getElementById("skipMain");
			Elements class1=id.getElementsByClass("floatLeft");
			Elements class2=id.getElementsByClass("comAct");
			Elements cl2=class2.select("a[href]");
			String finalDate="";
			
			ActsBean actsBean=new ActsBean();
			
//			if(finalDate.isEmpty())
//			{
//				DateFormat dateFormat = new SimpleDateFormat("9999-00-00'T'HH:mm:ss'Z'");
//			    Date date = new Date();
//			  //  finalDate="0000-00-00'T'HH:mm:ss'Z'";
//			}
			
			for(Element li:cl2)
			{
				actsBean.setLink(li.absUrl("href"));
			}
			
			Element paragraph=id.select("p").first();
			
			actsBean.setDate(finalDate);
			actsBean.setIdCode("VA-ACT");
            actsBean.setResourceCode("VALUE");
			actsBean.setministry("Ministry of Corporate Affairs");
			actsBean.setTitle("Ministry of Corporate Affairs "+title.text());
			actsBean.setDescription(paragraph.text());
			actsBean.setId(finalDate+"Ministry of Corporate Affairs "+ title.text());
            actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Corporate Affairs "+ title.text());
            
            if(url.equals("http://www.mca.gov.in/MinistryV2/companiesdonationtonationalfundact1951.html"))
            {
            	actsBean.setUrl(url);
            }
            else if(url.equals("http://www.mca.gov.in/MinistryV2/llpact.html"))
            {
            	actsBean.setUrl(url);
            }
            else if(url.equals("http://www.mca.gov.in/MinistryV2/competitionact.html"))
            {
            	actsBean.setUrl(url);
            }
            else if(url.equals("http://www.mca.gov.in/MinistryV2/thecompanysecretariesact.html"))
            {
            	actsBean.setUrl(url);
            }
            else if(url.equals("http://www.mca.gov.in/MinistryV2/societiesregistrationact.html"))
            {
            	actsBean.setUrl(url);
            }
            else if(url.equals("http://www.mca.gov.in/MinistryV2/companiesdonationtonationalfundact1951.html"))
            {
            	actsBean.setUrl(url);
            }
            
            ActsBeanList.add(actsBean);
			
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/List_of_Acts_of_the_Parliament_of_India"))
		{
			doc.getElementsByClass("mw-editsection-bracket").remove();
			doc.getElementsByClass("reference").remove();
			String finalDate="";
			Elements h2Elements = doc.getElementsByTag("h2");
			for(Element h2Element : h2Elements)
			{
				Element nextElement = h2Element.nextElementSibling();
					if(nextElement != null) 
					{
						boolean isTableNotReached = true;
						while(isTableNotReached)
						{
							if(nextElement.nodeName().equalsIgnoreCase("table"))
							{
								String tableTitle = h2Element.text();
								Element table = nextElement;
								Elements rows = table.select("tr");
								for (int i = 1; i < rows.size(); i++)
								{
									ActsBean actsBean=new ActsBean();
									
									Element row = rows.get(i);
								    Elements title=row.select("td:eq(0)");
								  
								    actsBean.setTitle(title.text());
								    
								    Elements date = row.select("td:eq(1)");
								    Elements actNo= row.select("td:eq(2)");
								  
								    String str=date.text().replaceFirst(String.valueOf((char) 160),"").trim();
								    
								    if(str.matches("[0-9]{4}"))
								    {
									SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
									SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
									finalDate = newDateParser.format(oldDateParser.parse(str));
									actsBean.setDate(finalDate);
									actsBean.setId(finalDate+ title.text());
						            actsBean.setIdentifier("VALUE_"+finalDate+"_"+ title.text());
								    }
									
									
									actsBean.setIdCode("VA-ACT");
						            actsBean.setResourceCode("VALUE");
									
						            actsBean.setUrl(url);
						            
						            ActsBeanList.add(actsBean);

								}
								isTableNotReached = false;
							}
							else if(nextElement.nodeName().equalsIgnoreCase("h3")) 
							{
								isTableNotReached = false;
							}
							else
							{
								if(nextElement.nextElementSibling() != null)
								{
									nextElement = nextElement.nextElementSibling();
								}
								break;
								
							}
						}
					}
			}
		}
		
		else if(url.equals("http://www.dot.gov.in/act-rules-content/2419"))
		{
			Elements title=doc.getElementsByTag("h3");
			ActsBean actsBean=new ActsBean();
			Elements class1=doc.getElementsByClass("field-content");
			Elements h4=class1.select("h4");
			int count=0;
			String finalDate="";
			
			/*if(finalDate.isEmpty())
            {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			    Date date = new Date();
			    finalDate=dateFormat.format(date);
			  //  finalDate="0000-00-00'T'HH:mm:ss'Z'";

            }*/
			
			actsBean.setTitle("Ministry of Communications "+title.text());
			actsBean.setDate(finalDate);
			actsBean.setIdCode("VA-ACT");
            actsBean.setResourceCode("VALUE");
			actsBean.setId(finalDate+"Ministry of Communications " +title.text());
			actsBean.setministry("Ministry of Communications");
			actsBean.setotherInformation("Department of Telecommunications");
            actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Communications "+title.text());
            actsBean.setUrl(url);
			
			for(int i=0;i<h4.size();i++)
			{
				Element h4ne=h4.get(i);
				Element ne=h4ne.nextElementSibling();
				if(count==0)
				{
				if(ne.tagName().equals("ol"))
				{
				    Elements ol=ne.select("ol");
				    actsBean.setDescription(ol.text());
				}
				}
				count ++;   
			}
			
			ActsBeanList.add(actsBean);
		}
		
		else if(url.equals("http://www.dot.gov.in/act-rules-content/2442"))
		{
			Elements class1=doc.getElementsByClass("view-content");
			Elements title=doc.select("h1");
			ActsBean actsBean=new ActsBean();
			
			String finalDate="";
			
			/*if(finalDate.isEmpty())
            {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			    Date date = new Date();
			    finalDate=dateFormat.format(date);
			 //   finalDate="0000-00-00'T'HH:mm:ss'Z'";

            }*/
			
			actsBean.setTitle("Ministry of Communications "+title.text());
			actsBean.setDate(finalDate);
			actsBean.setIdCode("VA-ACT");
            actsBean.setResourceCode("VALUE");
			actsBean.setId(finalDate+"Ministry of Communications " +title.text());
            actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Communications "+title.text());
			
			Elements h4=doc.select("h4");
			int count=0;
			for(int i=0;i<h4.size();i++)
			{
				Element h4ne=h4.get(i);
				Element ne=h4ne.nextElementSibling();
				if(count==0)
				{
					if(ne.tagName().equals("ol"))
					{
						Elements description=ne.select("ol");
					    actsBean.setDescription(description.text());
					}
				}
				count++;
			}
			actsBean.setUrl(url);
			
			ActsBeanList.add(actsBean);
		}
		
		else if(url.equals("http://www.finmin.nic.in/acts-rules"))
		{
			Elements class1=doc.getElementsByClass("view-content");
			Elements table=class1.select("table");
			Elements rows=table.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				ActsBean actsBean=new ActsBean();
				
				String finalDate="";
				
				/*if(finalDate.isEmpty())
	            {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				    Date date = new Date();
				    finalDate=dateFormat.format(date);
				 //   finalDate="0000-00-00'T'HH:mm:ss'Z'";

	            }*/
				
				Elements title=row.select("td:eq(1)");
				Elements links=row.select("td:eq(2)");
				
				Elements ahref=links.select("a[href]");
				for(Element link:ahref)
				{
					actsBean.setLink(link.absUrl("href"));
				}
				
				actsBean.setTitle("Ministry of Finance "+ title.text());
				actsBean.setDate(finalDate);
				actsBean.setIdCode("VA-ACT");
	            actsBean.setResourceCode("VALUE");
				actsBean.setId(finalDate+"Ministry of Finance "+title.text());
	            actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Finance "+title.text());
	            actsBean.setUrl(url);
	            actsBean.setministry("Ministry of Finance");
	            
				ActsBeanList.add(actsBean);

			}
		}
		
		//http://financialservices.gov.in/act-rule/Banking/Banking-Acts
		//http://financialservices.gov.in/act-rule/Banking/Banking-Acts?page=1
		//http://financialservices.gov.in/act-rule/Banking/Banking-Acts?page=2
		//http://financialservices.gov.in/act-rule/Insurance/Insurance-Acts
		//http://financialservices.gov.in/act-rule/Insurance/Ordinances
		//http://financialservices.gov.in/banking-divisions/act-rule
		//http://financialservices.gov.in/banking-divisions/act-rule?page=1
		//http://financialservices.gov.in/act-rule/Banking/Banking-Rules?page=2
		else if(url.equals("http://financialservices.gov.in/act-rule/Pension-Reforms")||
				url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Acts")||
				url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Acts?page=1")||
				url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Acts?page=2")||
				url.equals("http://financialservices.gov.in/act-rule/Insurance/Insurance-Acts")||
				url.equals("http://financialservices.gov.in/act-rule/Insurance/Ordinances")||
				url.equals("http://financialservices.gov.in/banking-divisions/act-rule")||
				url.equals("http://financialservices.gov.in/banking-divisions/act-rule?page=1")||
				url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Rules?page=2"))
		{
			Elements class1=doc.getElementsByClass("page-content");
			Elements tables=class1.select("table");
			for(int j=0;j<tables.size();j++)
			{
				Element table=tables.get(j);
				Elements rows=table.select("tr");
			   for(int i=1;i<rows.size();i++)
			   {
				 Element row=rows.get(i);
				 System.out.println(row.text());
				 ActsBean actsBean=new ActsBean();
				
				 Elements title=row.select("td:eq(1)");
				 Elements link=row.select("td:eq(2)");
				
                 String finalDate="";
				/* if(finalDate.isEmpty())
	             {
					 if(finalDate.isEmpty())
		             {
						DateFormat dateFormat = new SimpleDateFormat("0000-00-00'T'00:00:00'Z'");
					    Date date = new Date();
					   // finalDate=dateFormat.format(date);
						//finalDate="0000-00-00'T'00:00:00'Z'";
					    
					    Instant instant = Instant.parse( "1111-11-11T00:00:00Z" );
						finalDate=instant.toString();
		             }
					 
					
	             }*/
				
				Elements links=link.select("a[href]");
				for(Element href:links)
				{
					actsBean.setLink(href.absUrl("href"));
				}
					
				    actsBean.setministry("Ministry of Finance");
				    actsBean.setTitle("Ministry of Finance "+ title.text());
				    
				    actsBean.setIdCode("VA-ACT");
	                actsBean.setResourceCode("VALUE");
				    actsBean.setId(finalDate+"Ministry of Finance "+title.text());
				    actsBean.setotherInformation("Department of Financial Services");
	                actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Finance "+title.text());
	                
	                if(url.equals("http://financialservices.gov.in/act-rule/Pension-Reforms"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Acts"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Acts?page=1"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Acts?page=2"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/act-rule/Insurance/Insurance-Acts"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/act-rule/Insurance/Ordinances"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/banking-divisions/act-rule"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/banking-divisions/act-rule?page=1"))
	                {
	                	actsBean.setUrl(url);
	                }
	                if(url.equals("http://financialservices.gov.in/act-rule/Banking/Banking-Rules?page=2"))
	                {
	                	actsBean.setUrl(url);
	                }
	                
								
				ActsBeanList.add(actsBean);

			 }
		  }
			
		}
		
		else if(url.equals("http://www.dot.gov.in/act-rules-content/2430"))
		{
			Elements h1=doc.select("h1");
			Elements class1=doc.getElementsByClass("field-content");
			for(int i=0;i<class1.size();i++)
			{
				Element classi=class1.get(i);
				String finalDate="";
				ActsBean actsBean=new ActsBean();
				
				/*if(finalDate.isEmpty())
				{
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				    Date date = new Date();
				    finalDate=dateFormat.format(date);
				  //  finalDate="0000-00-00'T'HH:mm:ss'Z'";

				}*/
				
				if(!classi.text().contains("Download"))
				{
				   actsBean.setTitle("Ministry of Communication "+ classi.text());
				   actsBean.setId(finalDate+"Ministry of Finance "+classi.text());
		           actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Finance "+classi.text());
				}
				
				actsBean.setministry("Ministry of Communication");
				actsBean.setotherInformation("Department of Telecommunications");
				
				
				actsBean.setDate(finalDate);
				
				Elements href=classi.select("a[href]");
				for(Element link:href)
				{
					actsBean.setLink(link.absUrl("href"));
				}
				
				 actsBean.setIdCode("VA-ACT");
	             actsBean.setResourceCode("VALUE");
	             actsBean.setUrl(url);
	             
	           //  ActsBeanList.add(actsBean); 
			}
			
		}
		
		else if(url.equals("http://indiaculture.gov.in/acts-rules"))
		{
			Elements class1=doc.getElementsByClass("buildingGrants");
			Elements ul=class1.select("ul");
			for(int j=0;j<ul.size();j++)
			{
				Element ulList=ul.get(j);
				Elements li=ulList.select("li");
				for(int i=0;i<li.size();i++)
				{
					String finalDate="";
					ActsBean actsBean=new ActsBean();
					
					Element title=li.get(i);
					Elements links=title.select("a[href]");
					
					
					for(Element link:links)
					{
						actsBean.setLink(link.absUrl("href"));
					}
					
				/*	if(finalDate.isEmpty())
					{
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					    Date date = new Date();
					    finalDate=dateFormat.format(date);
					 //   finalDate="0000-00-00'T'HH:mm:ss'Z'";

					}*/
					
					actsBean.setTitle("Ministry of Culture "+title.text());
					actsBean.setministry("Ministry of Culture");
					actsBean.setId(finalDate+"Ministry of Culture "+title.text());
					actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Culture "+title.text());
					actsBean.setIdCode("VA-ACT");
					actsBean.setResourceCode("VALUE");
					actsBean.setUrl(url);
					actsBean.setDate(finalDate);
					
					//ActsBeanList.add(actsBean); 
					
				}
			
			}
		}
		
		else if(url.equals("https://mod.gov.in/acts")||
				url.equals("https://mod.gov.in/acts?page=1")||
				url.equals("https://mod.gov.in/acts?page=2"))
		{
			Elements class1=doc.getElementsByClass("view-content");
			Elements table=class1.select("table");
			Elements rows=table.select("tr");
			
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				ActsBean actsBean=new ActsBean();
				
				Elements title=row.select("td:eq(1)");
				Elements date=row.select("td:eq(2)");
				Elements hreflinks=row.select("td:eq(3)");
				String finalDate="";
				
				if(date.text().matches("[0-9]{2}/[0-9]{2}/[0-9]{4}"))
				{
					String str=date.text().replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					finalDate = newDateParser.format(oldDateParser.parse(str));
					//System.out.println(finalDate);
					actsBean.setDate(finalDate);
				}
				
				Elements links=hreflinks.select("a[href]");
				for(Element link:links)
				{
					actsBean.setLink(link.absUrl("href"));
				}
				
				actsBean.setTitle(title.text());
				actsBean.setministry("Ministry of Defence");
				actsBean.setId(finalDate+"Ministry of Defence "+title.text());
				actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Defence "+title.text());
				actsBean.setIdCode("VA-ACT");
				actsBean.setResourceCode("VALUE");	
				
				if(url.equals("https://mod.gov.in/acts"))
				{
					actsBean.setUrl("http://indiaculture.gov.in/acts-rules");
				}
				if(url.equals("https://mod.gov.in/acts?page=1"))
				{
					actsBean.setUrl("https://mod.gov.in/acts?page=1");
				}
				if(url.equals("https://mod.gov.in/acts?page=2"))
				{
					actsBean.setUrl("https://mod.gov.in/acts?page=2");
				}
				
				ActsBeanList.add(actsBean);
			}	
		}
		
		else if(url.equals("http://mib.gov.in/broadcasting/broadcasting-acts-rules"))
		{
			String countUrl = doc.select("a[title=\"Go to last page\"]").attr("href");
			int pagecount = Integer.parseInt(countUrl.substring(countUrl.lastIndexOf("=")+1));
			for (int i = 0; i <= pagecount; i++) 
			{
				doc = Jsoup.connect("http://mib.gov.in/broadcasting/broadcasting-acts-rules?page="+i).timeout(300000).get();
				
			Elements class1=doc.getElementsByClass("view-content");
			Elements table=class1.select("table");
			Elements rows=table.select("tr");
			for(int j=1;j<rows.size();j++)
			{
				Element row=rows.get(j);
				String finalDate="";
				ActsBean actsBean=new ActsBean();
				
				Elements title=row.select("td:eq(1)");
				Elements href=row.select("td:eq(2)");
				Elements date=row.select("td:eq(3)");
				
				if(date.text().matches("[0-9]{2}/[0-9]{2}/[0-9]{4}"))
				{
					String str=date.text().replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					finalDate = newDateParser.format(oldDateParser.parse(str));
				}
				
				Elements links=href.select("a[href]");
				for(Element link:links)
				{
					actsBean.setLink(link.absUrl("href"));
				}
				
				actsBean.setTitle("Ministry of Information and Broadcasting "+title.text());
				actsBean.setDate(finalDate);
				actsBean.setministry("Ministry of Information and Broadcasting");
				actsBean.setId(finalDate+"Ministry of Information and Broadcasting "+title.text());
				actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Information and Broadcasting "+title.text());
				actsBean.setIdCode("VA-ACT");
				actsBean.setResourceCode("VALUE");
				actsBean.setUrl(url);
							
				ActsBeanList.add(actsBean);
			 }
			
			}
		}
		
		else if(url.equals("http://www.mea.gov.in/acts-legislations-rules.htm?64/Acts,_Legislations__amp;_Rules"))
		{
			Elements class1=doc.getElementsByClass("commonListing");
			Elements liList=class1.select("li");
			for(int i=0;i<liList.size();i++)
			{
				String finalDate="";
				Element value=liList.get(i);
				ActsBean actsBean=new ActsBean();

				Elements title=value.select("a");
				Elements date=value.select("p");
				
				if(date.text().matches("[a-z,A-Z]{0,9} [0-9]{2}, [0-9]{4}"))
				{
					String str=date.text().replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("MMMM dd, yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					finalDate = newDateParser.format(oldDateParser.parse(str));
				}
				
				Elements href=title.select("a[href]");
				for(Element link:href)
				{
					actsBean.setLink(link.absUrl("href"));
				}
				
				actsBean.setTitle("Ministry of External Affairs "+title.text());
				actsBean.setDate(finalDate);
				actsBean.setId(finalDate+"Ministry of External Affairs "+title.text());
				actsBean.setIdCode("VA-ACT");
				actsBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of External Affairs "+title.text());
				actsBean.setUrl(url);
				actsBean.setResourceCode("VALUE");
				actsBean.setministry("Ministry of External Affairs");
				
				ActsBeanList.add(actsBean);
			}
		}
		
		else if(url.equals("https://dish.gujarat.gov.in/e-citizen-acts-rules.htm"))
		{
			Elements tabledata=doc.getElementsByClass("table-data");
			Elements rows=tabledata.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				String finalDate="";
				ActsBean actsBean=new ActsBean();
				
				Elements title=row.select("td:eq(1)");
				Elements hreflinks=row.select("td:eq(2)");
				
				Elements links=hreflinks.select("a[href]");
				for(Element link:links)
				{
					actsBean.setLink(link.absUrl("href"));
				}
				
				/*if(finalDate.isEmpty())
				{
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				    Date date = new Date();
				    finalDate=dateFormat.format(date);
				  //  finalDate="0000-00-00'T'HH:mm:ss'Z'";
				}*/
				
				actsBean.setTitle(title.text());
				actsBean.setDate(finalDate);
				actsBean.setId(finalDate+title.text());
				actsBean.setIdCode("VA-ACT");
				actsBean.setIdentifier("VALUE_"+finalDate+"_"+title.text());
				actsBean.setResourceCode("VALUE");
				actsBean.setotherInformation("Director Industrial Saftey and Health Labour & Employment Department Government of Gujarat");
				actsBean.setUrl(url);
				
				//ActsBeanList.add(actsBean);
			}
		}

		
		LOG.info("Generated ActsBeanList is==>"+ActsBeanList);
		return ActsBeanList;
		
	}

}
