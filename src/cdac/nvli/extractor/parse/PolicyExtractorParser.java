package cdac.nvli.extractor.parse;

import java.util.ArrayList;

import cdac.nvli.extractor.bean.PolicyBean;
import cdac.nvli.extractor.bean.SchemeBean;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.LinkLoopException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.util.SystemOutLogger;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;

import com.wuman.jreadability.Readability;

public class PolicyExtractorParser {

	private static final String XPATH_EXPRESSION_EVENT_TITLE = "/html/head/title";
	private Document doc;
	private W3CDom w3cDom;
	private ArrayList<String> nodeList;
	private ArrayList<PolicyBean> PolicyBeanList; 
	private boolean flag=false;
	private String policyDocTitle;
	
	private String policyTitle;
	private String policyDescription;
	private String policyUrl;
	
	private String policyId;
	private String policyIdentifier;
	/**
	 * Logger
	 */
	private static final Log LOG = LogFactory
			.getLog(PolicyExtractorParser.class.getName());
	

	public ArrayList<PolicyBean> urlParser(String url) throws IOException, ParseException{
		
		PolicyBeanList=new ArrayList<PolicyBean>();
		URL url1 = new URL(url);
		Document doc = Jsoup.parse(url1, 1000000);
		
		if(url.equals("http://steel.gov.in/policies")||
		   url.equals("http://steel.gov.in/policies?page=1"))
		{
			Elements table=doc.select("table");
			Elements rows=table.select("tr");
			String finalDate="";
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				Elements title=row.select("td:eq(0)");
				PolicyBean policyBean=new PolicyBean();
				
				policyBean.setTitle("Ministry of Steel "+title.text());
				Elements hreflinks=row.select("td:eq(1)");
				Elements links=hreflinks.select("a[href]");
				for(Element link:links)
				{
					policyBean.setLink(link.absUrl("href"));
				}
				Elements date1=row.select("td:eq(2)");
				
				String str=date1.text().replaceFirst(String.valueOf((char) 160),"").trim();
				SimpleDateFormat oldDateParser= new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
			    finalDate = newDateParser.format(oldDateParser.parse(str));
			   
			    policyBean.setDate(finalDate);
			    policyBean.setId(finalDate+"Ministry of Steel "+title.text());
			    policyBean.setResourceCode("VALUE");;
			    policyBean.setIdCode("VA-POL");
			    policyBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Steel "+title.text());
			    policyBean.setministry("Ministry of Steel");
			    
			    if(url.equals("http://steel.gov.in/policies"))
			    {
				    policyBean.setUrl(url);
			    }
			    else if(url.equals("http://steel.gov.in/policies?page=1"))
			    {
				    policyBean.setUrl(url);
			    }
			    
			    PolicyBeanList.add(policyBean);
			}
		}
		
		else if(url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/industrial-policy")||
				url.equals("http://dipp.nic.in/policies/national-design-policy")||
				url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/foreign-direct-investment-policy")||
				url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/national-manufacturing-policy"))
		{
			String finalDate="";
			Elements description=doc.getElementsByClass("teaser");
			Elements title=doc.select("title");
			PolicyBean policyBean=new PolicyBean();
			
			policyBean.setTitle("Ministry of Commerce and Industry "+title.text());
			Elements linkclass=doc.getElementsByClass("arrows");
			
			policyBean.setDescription(description.text());
			
			Elements links=linkclass.select("a[href]");
			for(Element link:links)
			{
				policyBean.setLink(link.absUrl("href"));
			}
			
//			if(finalDate.isEmpty())
//			{
//				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//			    Date date = new Date();
//			    finalDate=dateFormat.format(date);
//			}
			if(url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/industrial-policy"))
			{
			    policyBean.setUrl(url);
			}
			else if(url.equals("http://dipp.nic.in/policies/national-design-policy"))
			{
			    policyBean.setUrl(url);
			}
			else if(url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/foreign-direct-investment-policy"))
			{
			    policyBean.setUrl(url);
			}
			else if(url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/national-manufacturing-policy"))
			{
			    policyBean.setUrl(url);
			}
			
			policyBean.setDate(finalDate);
			policyBean.setId(finalDate+"Ministry of Commerce and Industry "+title.text());
			policyBean.setResourceCode("VALUE");;
			policyBean.setIdCode("VA-POL");
			policyBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Commerce and Industry "+title.text());
			policyBean.setministry("Ministry of Commerce and Industry");
			
		    PolicyBeanList.add(policyBean);
		}
		
		else if(url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/north-east-industrial-investment-promotion-policy-neiipp-2007")||
				url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/national-ipr-policy"))
		{
			String finalDate="";
			Elements description=doc.getElementsByClass("teaser");
			Elements title=doc.select("title");
			PolicyBean policyBean=new PolicyBean();
			
			policyBean.setTitle("Ministry of Commerce and Industry "+title.text());
			Elements linkclass=doc.getElementsByClass("arrows");
			
			policyBean.setDescription(description.text());
			
			/*if(finalDate.isEmpty())
			{
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			    Date date = new Date();
			    finalDate=dateFormat.format(date);
			}*/
			
			if(url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/north-east-industrial-investment-promotion-policy-neiipp-2007"))
			{
				policyBean.setUrl(url);
			}
			else if(url.equals("http://dipp.nic.in/policies-rules-and-acts/policies/national-ipr-policy"))
			{
				policyBean.setUrl(url);
			}
			
			policyBean.setDate(finalDate);
			policyBean.setId(finalDate+"Ministry of Commerce and Industry "+title.text());
			policyBean.setResourceCode("VALUE");;
			policyBean.setIdCode("VA-POL");
			policyBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Commerce and Industry "+title.text());
			policyBean.setministry("Ministry of Commerce and Industry");
			
			PolicyBeanList.add(policyBean);
		}
		
		else if(url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=0&linkid=46&lid=121&ltypeid=2&domid=6"))
		{
			Elements ul=doc.select("ul");
			Elements li=ul.select("li");
			String finalDate="";
			for(int i=0;i<li.size();i++)
			{
				Element description=li.get(i);
				String[] str=description.text().split("-",2);
				PolicyBean policyBean=new PolicyBean();
				
                policyBean.setTitle("Ministry of women child development "+str[0]);
				for(String stri:str)
				{
					if(!stri.equals(str[0]))
					{
					     policyBean.setDescription(stri);
					}
				}
				/*if(finalDate.isEmpty())
				{
					 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
				}*/
			    
				policyBean.setDate(finalDate);
				policyBean.setId(finalDate+"Ministry of women child development "+str[0]);
			    policyBean.setResourceCode("VALUE");;
			    policyBean.setIdCode("VA-POL");
			    policyBean.setministry("Ministry of women child development");
			    policyBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of women child development "+str[0]);
			    policyBean.setUrl(url);
			    
			    PolicyBeanList.add(policyBean);
			}
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/Look_East_policy_(India)")||
				url.equals("https://en.wikipedia.org/wiki/National_Policy_on_Education")||
				url.equals("https://en.wikipedia.org/wiki/Department_of_Industrial_Policy_and_Promotion")||
				url.equals("https://en.wikipedia.org/wiki/Environmental_policy_of_India")||
				url.equals("https://en.wikipedia.org/wiki/Energy_policy_of_India")||
				url.equals("https://en.wikipedia.org/wiki/Freight_equalisation_policy")||
				url.equals("https://en.wikipedia.org/wiki/National_Policy_on_Electronics_(India)")||
				url.equals("https://en.wikipedia.org/wiki/New_Exploration_Licensing_Policy")||
				url.equals("https://en.wikipedia.org/wiki/National_Civil_Aviation_Policy")||
				url.equals("https://en.wikipedia.org/wiki/Monetary_policy_of_India"))
		{
			doc.getElementsByClass("reference").remove();
			String finalDate="";

			Element title=doc.getElementById("firstHeading");
			PolicyBean policyBean=new PolicyBean();
			
			policyBean.setTitle(title.text());

		    Elements class1=doc.getElementsByClass("mw-content-ltr");
		    Element paragraph=class1.select("p").first();
		    
		    policyBean.setDescription(paragraph.text());
		   /* if(finalDate.isEmpty())
		    {
		    	 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			     Date date = new Date();
			     finalDate=dateFormat.format(date);
		    }*/
		   
		    if(url.equals("https://en.wikipedia.org/wiki/Look_East_policy_(India)"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/National_Policy_on_Education"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/Department_of_Industrial_Policy_and_Promotion"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/Environmental_policy_of_India"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/Energy_policy_of_India"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/Freight_equalisation_policy"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/National_Policy_on_Electronics_(India)"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/New_Exploration_Licensing_Policy"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/National_Civil_Aviation_Policy"))
		    {
			    policyBean.setUrl(url);
		    }
		    else if(url.equals("https://en.wikipedia.org/wiki/Monetary_policy_of_India"))
		    {
			    policyBean.setUrl(url);
		    }
		    
		    policyBean.setDate(finalDate);
		    policyBean.setId(finalDate+title.text());
		    policyBean.setResourceCode("VALUE");;
		    policyBean.setIdCode("VA-POL");
		    policyBean.setIdentifier("VALUE_"+finalDate+"_"+title.text());
		    
		    PolicyBeanList.add(policyBean);
		    
		}
		
		else if(url.equals("http://texmin.nic.in/policies"))
		{
			doc.getElementsByClass("reference").remove(); 
			String finalDate="";
			Elements class1=doc.getElementsByClass("view-content");

			Elements table=class1.select("table");
			Elements rows=table.select("tr");
			Elements title=doc.select("title");
			
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				Elements name=row.select("td:eq(0)");
				Elements links=row.select("td:eq(1)");	
				Elements hreflink=links.select("a[href]");
						
				PolicyBean policyBean=new PolicyBean();
				
				/*if(finalDate.isEmpty())
				{
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				    Date date = new Date();
				    finalDate=dateFormat.format(date);
				}*/
				
				policyBean.setId(finalDate+"Ministry of Textiles "+name.text());
				policyBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Textiles "+name.text());
				policyBean.setTitle("Ministry of Textiles "+name.text());
				
				for(Element link:hreflink)
				{
					policyBean.setLink(link.absUrl("href"));
				}
				
			     policyBean.setDate(finalDate);
				 policyBean.setResourceCode("VALUE");;
				 policyBean.setIdCode("VA-POL");
				 policyBean.setministry("Ministry of Textiles");
				 policyBean.setUrl(url);
					 
				PolicyBeanList.add(policyBean);
			 }
			
			
		}
		
		else if(url.equals("http://pharmaceuticals.gov.in/policy"))
		{
			Elements table=doc.select("table");
			Elements rows=table.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				String finalDate="";
				Element row=rows.get(i);
				Elements title=row.select("td:eq(0)");
				Elements details=row.select("td:eq(1)");
				Elements links=details.select("a[href]");
				
				
				PolicyBean policyBean=new PolicyBean();
				
				for(Element link:links)
				{
					policyBean.setLink(link.absUrl("href"));
				}
				/*if(finalDate.isEmpty())
				{
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				    Date date = new Date();
				    finalDate=dateFormat.format(date);
				}*/
				
				policyBean.setTitle("Department of Pharmaceutical "+title.text());
				policyBean.setDate(finalDate);
				policyBean.setId(finalDate+"Department of Pharmaceutical "+title.text());
				policyBean.setIdentifier("VALUE_"+finalDate+"_"+"Department of Pharmaceutical "+title.text());
				policyBean.setResourceCode("VALUE");;
				policyBean.setIdCode("VA-POL");
				policyBean.setUrl(url);
				policyBean.setministry("Ministry of Chemicals and Fertilizers");
				
				PolicyBeanList.add(policyBean);
				
			}
		}
		
		
		LOG.info("Generated PolicyBeanList is==>"+PolicyBeanList);
		return PolicyBeanList;
	}
}
