package cdac.nvli.extractor.parse;

import java.util.ArrayList;


import cdac.nvli.extractor.bean.SchemeBean;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.LinkLoopException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;



import com.wuman.jreadability.Readability;






public class SchemeExtractorParser {
	private static final String XPATH_EXPRESSION_EVENT_TITLE = "/html/head/title";
	private Document doc;
	private W3CDom w3cDom;
	private ArrayList<String> nodeList;
	private ArrayList<SchemeBean> SchemeBeanList; 
	private boolean flag=false;
	private String schemeDocTitle;
	
	private String schemeTitle;
	private String schemeDescription;
	private String schemeUrl;
	
	private String schemeId;
	private String schemeIdentifier;
	/**
	 * Logger
	 */
	private static final Log LOG = LogFactory
			.getLog(SchemeExtractorParser.class.getName());
	

	public ArrayList<SchemeBean> urlParser(String url) throws IOException, ParseException{
		
		SchemeBeanList=new ArrayList<SchemeBean>();
		URL url1 = new URL(url);
		//System.out.println(url1);
		Document doc = Jsoup.parse(url1, 1000000);
		
		if(url.equals("http://www.indiaculture.nic.in/schemes"))
		{
			Elements tables=doc.select("table");
			Elements rows=tables.select("tr");
			String finalDate="";
			
			for(int i=0;i<rows.size();i++)
			{
			     Element nameScheme=rows.get(i);
			     SchemeBean schemeBean=new SchemeBean();
			     
			     //System.out.println("Name of schemes-: "+nameScheme.text());
                 schemeBean.setTitle("Ministry of Culture "+nameScheme.text());			     
			     Elements links=nameScheme.select("a[href]");
			     for(Element link:links)
			     {
			        //System.out.println("links-: "+link.absUrl("href"));
			    	 schemeBean.setLink(link.absUrl("href"));
			     }

			     /*if(finalDate.isEmpty())
			     {
			    	 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
			     }*/
			     
	                schemeBean.setId(finalDate+"Ministry of Culture "+nameScheme.text());
	                schemeBean.setministry("Ministry of Culture");
	                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Culture "+nameScheme.text());
	                schemeBean.setIdCode("VA-SCH");
	                schemeBean.setResourceCode("VALUE");
	                schemeBean.setUrl(url);
				    schemeBean.setDate(finalDate);
			     
			    SchemeBeanList.add(schemeBean);
			}	
		}
		
		else if(url.equals("http://csms.nic.in/scheme_status.php"))
		{
			 Elements tables=doc.select("table");
			 Elements rows=tables.select("tr");
			 String finalDate1="";
			 String finalDate="";
			    
			 for(int i=1;i<rows.size();i++)
			 {
			    	Element row=rows.get(i);
			    	Elements schemeName=row.select("td:eq(1)");
			    	Elements startDate=row.select("td:eq(2)");
			    	Elements endDate=row.select("td:eq(3)");
			    	Elements userType=row.select("td:eq(5)");
			    	
				    SchemeBean schemeBean=new SchemeBean();
					
			    	String str=startDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("dd-MM-yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					finalDate = newDateParser.format(oldDateParser.parse(str));
					
					schemeBean.setDate(finalDate);
					//System.out.println("start date-: "+finalDate);
			    	schemeBean.setTitle(schemeName.text());
					//schemeBean.setField("User type "+userType.text());
					schemeBean.setotherInformation("User type "+userType.text());
			    	//System.out.println(userType.text());
			    	
					if(endDate.text().matches("[0-9]{0,10}-[0-9]{0,10}-[0-9]{4}"))
					{
						String str1=endDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser1= new SimpleDateFormat("dd-MM-yyyy");
						SimpleDateFormat newDateParser1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser1.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate1 = newDateParser1.format(oldDateParser1.parse(str1));
						schemeBean.setEndDate(finalDate1);
						//System.out.println("end date-: "+finalDate1);
					}
					else
						schemeBean.setEndDate(endDate.text());
				   
					 schemeBean.setId(finalDate+schemeName.text());
		             schemeBean.setIdentifier("VALUE_"+finalDate+"_"+schemeName.text());
					 schemeBean.setIdCode("VA-SCH");
		             schemeBean.setResourceCode("VALUE");
		             schemeBean.setUrl(url);
					
		             SchemeBeanList.add(schemeBean);
			  }
			
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/List_of_government_schemes_in_India"))
		{
			Elements table=doc.select("table[class= wikitable sortable]");
			doc.getElementsByClass("sortkey").remove();
			doc.getElementsByClass("reference").remove();
			Elements rows=table.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				String date="";
				String finalDate="";
				String schemeLinks="";
		
				SchemeBean schemeBean=new SchemeBean();
		
				Element row=rows.get(i);
				Elements schemeName=row.select("td:eq(0)");
				Elements ministry=row.select("td:eq(1)");
				Elements dateOfLaunch=row.select("td:eq(2)");
				Elements sector=row.select("td:eq(4)");
				Elements Provisions=row.select("td:eq(5)");
				
				Elements links=schemeName.select("a[href]");
				for(Element link:links)
				{
					schemeLinks=link.absUrl("href");
					schemeBean.setLink(schemeLinks);
				}
				
			    date=dateOfLaunch.text().replaceAll(",", "").toString();
			    date= date.replace(",", "");
			    if(date.matches("^\\d{4}"))
				{
					String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				    finalDate = newDateParser.format(oldDateParser.parse(str));
				    
				    schemeBean.setDate(finalDate);
					schemeBean.setId(finalDate+ministry.text()+schemeName.text());
		            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+ministry.text()+schemeName.text());
				}
			    else if(date.matches("[a-zA-Z]{0,10} [0-9]{4}"))
			    {
			    	String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("MMMM yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				    finalDate = newDateParser.format(oldDateParser.parse(str));
				    
				    schemeBean.setDate(finalDate);
					schemeBean.setId(finalDate+ministry.text()+schemeName.text());
		            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+ministry.text()+schemeName.text());
			    }
			    else if(date.matches("[0-9]{0,10} [a-zA-Z]{0,10} [0-9]{4}"))
			    {
			    	String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMMM yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				    finalDate = newDateParser.format(oldDateParser.parse(str));
				    
				    schemeBean.setDate(finalDate);
					schemeBean.setId(finalDate+ministry.text()+schemeName.text());
		            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+ministry.text()+schemeName.text());
			    }
			    else if(date.matches("[A-Za-z]{0,10} [0-9]{0,2} [0-9]{4}"))
			    {
			    	String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("MMMM dd yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				    finalDate = newDateParser.format(oldDateParser.parse(str));
				    
				    schemeBean.setDate(finalDate);
					schemeBean.setId(finalDate+ministry.text()+schemeName.text());
		            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+ministry.text()+schemeName.text());
			    }
			    /*else if(finalDate.isEmpty())
			    {
			    	 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date1 = new Date();
				     finalDate=dateFormat.format(date1);
			    
			    }*/
				schemeBean.setDate(finalDate);
				schemeBean.setId(finalDate+schemeName.text());
	            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+ministry.text()+schemeName.text());
				schemeBean.setTitle(ministry.text()+" "+schemeName.text());
				///****
				//schemeBean.setField("Ministry "+ministry.text()+"Sector "+sector.text());
				schemeBean.setotherInformation("Sector "+sector.text());
				schemeBean.setministry(ministry.text());
				schemeBean.setDescription(Provisions.text());
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
				
	            SchemeBeanList.add(schemeBean);
			}
		}
		
		else if(url.equals("http://www.wcd.nic.in/schemes-listing/2405")||
				url.equals("http://www.wcd.nic.in/schemes-listing/2406")||
				url.equals("http://www.wcd.nic.in/schemes-listing/2404")||
				url.equals("http://www.wcd.nic.in/schemes-listing/2419"))
		{
			Elements title=doc.select("title");
			String heading="";
			String finalDate="";
			//heading=title.text().replace("|  Ministry Of Women & Child Development | GoI","");
			Elements class1=doc.getElementsByClass("view-content");
			Elements h1=doc.select("h1");
			for(int i=0;i<class1.size();i++)
			{
				//System.out.println(class1.get(i).text());
				Element classi=class1.get(i);
				Elements ol=classi.select("li");
				Elements li=ol.select("li");
				
				
				String schemeName="";
				String schemeLinks="";
				for(int j=0;j<li.size();j++)
				{
					Element liValue=li.get(j);
					SchemeBean schemeBean=new SchemeBean();

					schemeName=liValue.text();
					Elements links=liValue.select("a[href]");
					for(Element link:links)
					{
						schemeLinks=link.absUrl("href");
					}
					
//					if(finalDate.isEmpty())
//					{
//						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//					    Date date = new Date();
//					    finalDate=dateFormat.format(date);
//					}
				    schemeBean.setDate(finalDate);
					schemeBean.setTitle("Ministry of Women & Child Development "+h1.text()+" "+liValue.text());
					schemeBean.setId(finalDate+"Ministry of Women & Child Development "+h1.text()+" "+liValue.text());
					schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Women & Child Development "+h1.text()+" "+liValue.text());
					schemeBean.setIdCode("VA-SCH");
		            schemeBean.setResourceCode("VALUE");
		            
		            if(url.equals("http://www.wcd.nic.in/schemes-listing/2405"))
		            {
			            schemeBean.setUrl(url);
		            }
		            else if(url.equals("http://www.wcd.nic.in/schemes-listing/2406"))
		            {
			            schemeBean.setUrl(url);
		            }
		            else if(url.equals("http://www.wcd.nic.in/schemes-listing/2404"))
		            {
			            schemeBean.setUrl(url);
		            }
		            else if(url.equals("http://www.wcd.nic.in/schemes-listing/2419"))
		            {
			            schemeBean.setUrl(url);
		            }
		            schemeBean.setministry("Ministry of Women & Child Development");
					schemeBean.setLink(schemeLinks);
		            SchemeBeanList.add(schemeBean);	
				}
				
			}
		}
		
		else if(url.equals("http://mhrd.gov.in/scheme"))
		{
			Elements class1=doc.getElementsByClass("field-items");
			Elements ul=class1.select("ul");
			Elements li=ul.select("li");
			String names="";
			for(int i=0;i<li.size();i++)
			{
				String finalDate="";
				SchemeBean schemeBean=new SchemeBean();
				names=li.get(i).text();
				schemeBean.setTitle("Ministry of Human Resource Development Government of India "+names);
				Elements links=li.get(i).select("a[href]");
				String urlLinks="";
				for(Element link:links)
				{
					urlLinks=link.absUrl("href");
					schemeBean.setLink(urlLinks);
				}
				doc=Jsoup.connect(urlLinks).timeout(1000000).get();
				Elements h1=doc.select("h2[class=page-title]");
				for(int j=0;j<h1.size();j++)
				{
					Element h1Value=h1.get(j);
					Element description=h1Value.nextElementSibling();
					if(!description.tagName().equals("p"))
					{
						Element description1=description.nextElementSibling();
						schemeBean.setDescription(description1.text());
					}
					else if(description.tagName().equals("p"))
					{
						schemeBean.setDescription(description.text());
					}
				}
				
				/*if(finalDate.isEmpty())
				{
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				    Date date = new Date();
				    finalDate=dateFormat.format(date);
				}*/
			    schemeBean.setDate(finalDate);
			    schemeBean.setministry("Ministry of Human Resource Development Government of India");
				schemeBean.setId(finalDate+"Ministry of Human Resource Development Government of India "+names);
				schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Human Resource Development Government of India "+names);
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
	            SchemeBeanList.add(schemeBean);
			}
		}
		
		else if(url.equals("http://www.mofpi.nic.in/Schemes/related-schemes-other-agencies"))
		{
			Elements h3=doc.getElementsByTag("h3");
			for(int i=0;i<h3.size();i++)
			{
				String descriptionlinks="";
				String finalDate="";
				
				Element name=h3.get(i);
				SchemeBean schemeBean=new SchemeBean();
				Element description=name.nextElementSibling();
				Elements links=description.select("a[href]");
				for(Element link:links)
				{
					descriptionlinks=link.absUrl("href");	
				}
				
				/*if(finalDate.isEmpty())
				{
				   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				   Date date = new Date();
				   finalDate=dateFormat.format(date);
				}*/
				schemeBean.setDate(finalDate);
				schemeBean.setministry("Ministry of Food Processing Industries");
				schemeBean.setTitle("Ministry of Food Processing Industries "+name.text());
				schemeBean.setId(finalDate+"Ministry of Food Processing Industries "+name.text());
				schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Food Processing Industries "+name.text());
				schemeBean.setDescription(description.text());
				schemeBean.setLink(descriptionlinks);
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
	            SchemeBeanList.add(schemeBean);
			}
		}
		
		else if(url.equals("http://mdoner.gov.in/node/726"))
		{
			Elements table=doc.getElementsByTag("table");
			Elements rows=table.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				String finalDate="";
				SchemeBean schemeBean=new SchemeBean();
				Elements col1=row.select("td:eq(1)");
				Elements col2=row.select("td:eq(2)");
				Elements col3=row.select("td:eq(3)");
				Elements links=col3.select("a[href]");
				schemeBean.setTitle("Ministry of Development of North East Region "+col1.text());
				
				/*if(finalDate.isEmpty())
				{
				   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				   Date date = new Date();
				   finalDate=dateFormat.format(date);
				}*/
				///****
				//schemeBean.setField("Ministy/Department Name "+col2.text());
				schemeBean.setDate(finalDate);
				schemeBean.setministry("Ministry of Development of North East Region");
				schemeBean.setotherInformation("Ministy/Department Name "+col2.text());
				schemeBean.setId(finalDate+"Ministry of Development of North East Region "+col1.text());
                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Development of North East Region "+col1.text());
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
	            
	            SchemeBeanList.add(schemeBean);

			}
		}
		
		else if(url.equals("http://rural.nic.in/scheme-websites"))
		{
			Elements table=doc.select("table");
			Elements rows=table.select("tr");
			String finalDate="";
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				SchemeBean schemeBean=new SchemeBean();

				Elements title=row.select("td:eq(1)");
				schemeBean.setTitle("Ministry of Rural Development "+title.text());
				
				Elements td2=row.select("td:eq(2)");
				Elements links=td2.select("a[href]");
				for(Element link:links)
				{
					schemeBean.setLink(link.absUrl("href"));
				}
				/*if(finalDate.isEmpty())
				{
				   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				   Date date = new Date();
				   finalDate=dateFormat.format(date);
				}*/
				
				schemeBean.setDate(finalDate);
				schemeBean.setId(finalDate+"Ministry of Rural Development "+title.text());
				schemeBean.setministry("Ministry of Rural Development");
                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Rural Development "+title.text());
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
	            
	            SchemeBeanList.add(schemeBean);
				
				
			}
		}
		
		else if(url.equals("http://sahitya-akademi.gov.in/sahitya-akademi/projects-schemes/index.jsp"))
		{
			 Elements class1=doc.select("ol");
			    Elements p=doc.select("p");
			    for(int j=1;j<p.size();j++)
			    {
			    	Element pi=p.get(j);
			    	if(!pi.text().contains("The three Projects running successfully under Sahitya Akademi are:-"))
			    	{
			    		Element ne=pi.nextElementSibling();		
				    	Elements ol=ne.select("li");
				    	String finalDate="";
				    	for(int i=0;i<ol.size();i++)
				    	{
				    		Element olValue=ol.get(i);
							SchemeBean schemeBean=new SchemeBean();

				    		String urls="";
				    		Elements links=olValue.select("a[href]");
				    		for(Element link:links)
				    		{
				    			urls=link.absUrl("href");
				    		}
				    		doc=Jsoup.connect(urls).timeout(10000000).get();
				    		Elements class11=doc.getElementsByClass("firstletter");
				    		Elements description=doc.select("p");
				    		/*if(finalDate.isEmpty())
							{
							   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							   Date date = new Date();
							   finalDate=dateFormat.format(date);
							}*/
							schemeBean.setDate(finalDate);
				    		schemeBean.setTitle("Sahitya Akademi Scheme "+olValue.text());
				    		schemeBean.setUrl(urls);
				    		schemeBean.setDescription(description.text());
				    		schemeBean.setId(finalDate+"Sahitya Akademi Scheme "+olValue.text());
			                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Sahitya Akademi Scheme "+olValue.text());
							schemeBean.setIdCode("VA-SCH");
				            schemeBean.setResourceCode("VALUE");

				            SchemeBeanList.add(schemeBean);
				    		
				    	}
			    	}
			    	
			    }
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/Poverty_alleviation_programmes_in_India"))
		{
			doc.getElementsByClass("mw-editsection").remove();
			Elements h2=doc.getElementsByTag("h2");
			for(int i=0;i<h2.size();i++)
			{
				Element title=h2.get(i);
				String finalDate="";
				SchemeBean schemeBean=new SchemeBean();
				/*if(finalDate.isEmpty())
				{
				   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				   Date date = new Date();
				   finalDate=dateFormat.format(date);
				}*/
				
				if(!title.text().equals("Contents")&&
				   !title.text().contains("References")&&
				   !title.text().contains("External links")&&
				   !title.text().contains("Navigation menu")&&
				   !title.text().contains("Integrated Rural Development Program")&&
				   !title.text().contains("Pradhan Mantri Gramin Awaas Yojana")&&
				   !title.text().contains("National Rural Employment Guarantee Act"))
				{
					Element ne1=title.nextElementSibling();
					
					schemeBean.setTitle(title.text());
					
					Elements description2=ne1.select("p");
					if(ne1.tagName().equals("div") || ne1.tagName().equals("table"))
					{
						Element ne2=ne1.nextElementSibling();
						Elements description=ne2.select("p");
						
						
						if(ne2.tagName().equals("table"))
						{
							Element ne3=ne2.nextElementSibling();
							Elements description1=ne3.select("p");
							schemeBean.setDescription(description1.text());
						}
						else if(!ne2.tagName().equals("table"))
						{
							schemeBean.setDescription(description.text());
						}
					}
					
					else if(!ne1.tagName().equals("div") || !ne1.tagName().equals("table"))
					{
						schemeBean.setDescription(description2.text());
					}
					schemeBean.setDate(finalDate);
		    		schemeBean.setUrl(url);
		    		schemeBean.setId(finalDate+title.text());
	                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+title.text());
					schemeBean.setIdCode("VA-SCH");
		            schemeBean.setResourceCode("VALUE");

		            SchemeBeanList.add(schemeBean);
						
				}
				
			}
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/Category:Government_schemes_in_Madhya_Pradesh"))
		{
			doc.getElementsByClass("references").remove();
			Elements h2=doc.select("h3");
			String finalDate="";
			Elements class1=doc.getElementsByClass("mw-category-group");
			for(int i=0;i<class1.size();i++)
			{
				Element classul=class1.get(i);
				Elements ul=classul.select("ul");
				Elements li=ul.select("li");
				for(int j=0;j<li.size();j++)
				{
					Element title=li.get(j);
					SchemeBean schemeBean=new SchemeBean();
					schemeBean.setTitle("Government schemes in Madhya Pradesh "+title.text());
					Elements links=title.select("a[href]");
					String urls="";
					for(Element link:links)
					{
						urls=link.absUrl("href");
					}
					schemeBean.setUrl(urls);
					
					doc=Jsoup.connect(urls).timeout(1000000).get();
					doc.getElementsByClass("reference").remove();
					
					Element div=doc.select("div.mw-content-ltr").first();
					Element description=div.select("p").get(0);
					
					/*if(finalDate.isEmpty())
					{
					   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					   Date date = new Date();
					   finalDate=dateFormat.format(date);
					}*/
					schemeBean.setUrl(urls);
					schemeBean.setDate(finalDate);
					schemeBean.setDescription(description.text());
					schemeBean.setId(finalDate+"Government schemes in Madhya Pradesh "+title.text());
	                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Government schemes in Madhya Pradesh "+title.text());
					schemeBean.setIdCode("VA-SCH");
		            schemeBean.setResourceCode("VALUE");
		            
		            SchemeBeanList.add(schemeBean);

				}
			}
		}
		////addd it as
		else if(url.equals("https://en.wikipedia.org/wiki/Odisha_Government_Schemes_List"))
		{
			Elements table=doc.select("table[class=wikitable sortable]");
			doc.getElementsByClass("sortkey").remove();
			doc.getElementsByClass("reference").remove();
			Elements rows=table.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				
				Element row=rows.get(i);
				SchemeBean schemeBean=new SchemeBean();

				Elements schemeName=row.select("td:eq(0)");
				Elements launchedDate=row.select("td:eq(1)");
				Elements department=row.select("td:eq(2)");
	            Elements sector=row.select("td:eq(3)");
	            Elements provisions=row.select("td:eq(4)");
	            
	            String date="";
	            String finalDate="";
	            
	            date=launchedDate.text().replaceAll(",", "").toString();
			    date= date.replace(",", "");
			    if(date.matches("[a-zA-Z]{0,10} [0-9]{4}"))
			    {
			    	String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("MMMM yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				    finalDate = newDateParser.format(oldDateParser.parse(str));
				    schemeBean.setDate(finalDate);
			    	schemeBean.setId(finalDate+"Odisha Government Scheme "+schemeName.text());
	                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Odisha Government Scheme "+schemeName.text());
			    }
			    else if(date.matches("[A-Za-z]{0,10} [0-9]{0,2} [0-9]{4}"))
			    {
			    	String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("MMMM dd yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				    finalDate = newDateParser.format(oldDateParser.parse(str));
				    
				    schemeBean.setDate(finalDate);
			    	schemeBean.setId(finalDate+"Odisha Government Scheme "+schemeName.text());
	                schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Odisha Government Scheme "+schemeName.text());

			    }
			    else if(date.contains("Announced in Budget")|| finalDate.isEmpty())
			    {
			    	/*DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					Date date1 = new Date();*/
					finalDate="";
			    }
		    	    schemeBean.setDate(finalDate);
			        schemeBean.setTitle("Odisha Government Scheme "+schemeName.text());
					schemeBean.setId(finalDate+"Odisha Government Scheme "+schemeName.text());
		            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Odisha Government Scheme "+schemeName.text());
			    	/////*****
			    	//schemeBean.setField("Department "+department.text()+"Sector "+sector.text());
			    	schemeBean.setotherInformation("Department "+department.text()+"Sector "+sector.text());
			    	schemeBean.setDescription(provisions.text());
					schemeBean.setIdCode("VA-SCH");
		            schemeBean.setResourceCode("VALUE");
					schemeBean.setUrl(url);

		            SchemeBeanList.add(schemeBean);
	           
			}
		}
		
		else if(url.equals("https://www.nhp.gov.in/national-health-insurance-schemes_pg#Employment%20State%20Insurance%20Scheme%20(ESIS)"))
		{
			Elements class1=doc.getElementsByClass("content");
			Elements p=class1.select("p");
			Elements h4=doc.select("h4");
			for(int j=1;j<h4.size();j++)
			{
				Element h4Value=h4.get(j);
				String finalDate="";
				SchemeBean schemeBean=new SchemeBean();
				String title="";
				
				if(!h4Value.text().contains("Discussion")&&
				   !h4Value.text().contains("Write your comments") &&
				   !h4Value.text().contains("Related Pages") && h4Value.text().contains("."))
				{
					String str=h4Value.text().split("\\.")[1];
					title=str.trim();
					schemeBean.setTitle("National Health Insurance Schemes "+title);
					Element description=h4Value.nextElementSibling();
					if(description.tagName().equals("p"))
					{
						schemeBean.setDescription(description.text());
					}
					
					/*if(finalDate.isEmpty())
					{
					   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					   Date date = new Date();
					   finalDate=dateFormat.format(date);
					}*/
					schemeBean.setDate(finalDate);
					schemeBean.setId(finalDate+"National Health Insurance Schemes "+title);
		            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"National Health Insurance Schemes "+title);
					schemeBean.setIdCode("VA-SCH");
		            schemeBean.setResourceCode("VALUE");
					schemeBean.setUrl(url);
					
		            SchemeBeanList.add(schemeBean);

				}
		
			}
		}
		
		else if(url.equals("http://labour.gov.in/schemes/aam-admi-beema-yojana"))
		{
			String finalDate="";
			Elements title=doc.select("h1");
			SchemeBean schemeBean=new SchemeBean();
			
			Elements class1=doc.getElementsByClass("field-content");
			Element description=class1.select("p").get(0);
			
			/*if(finalDate.isEmpty())
			{
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			   Date date = new Date();
			   finalDate=dateFormat.format(date);
			}*/
			schemeBean.setDate(finalDate);
			schemeBean.setTitle("Ministry of Labour and employment "+title.text());
			schemeBean.setministry("Ministry of Labour and employment");
			schemeBean.setDescription(description.text());
			schemeBean.setId(finalDate+"Ministry of Labour and employment "+title.text());
            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Labour and employment "+title.text());
			schemeBean.setIdCode("VA-SCH");
            schemeBean.setResourceCode("VALUE");
            schemeBean.setUrl(url);
   
            SchemeBeanList.add(schemeBean);
		}
		
		else if(url.equals("http://labour.gov.in/schemes/national-child-labour-project-scheme"))
		{
			Elements description=doc.getElementsByClass("field-content");
			Elements title=doc.select("h2");
			String finalDate="";

			SchemeBean schemeBean=new SchemeBean();
			/*if(finalDate.isEmpty())
			{
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			   Date date = new Date();
			   finalDate=dateFormat.format(date);
			}*/
			schemeBean.setDate(finalDate);
			schemeBean.setTitle(title.text());
			schemeBean.setDescription(description.text());
			schemeBean.setUrl(url);
			schemeBean.setId(finalDate+"Ministry of Labour and employment "+title.text());
			schemeBean.setministry("Ministry of Labour and employment");
            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Labour and employment "+title.text());
			schemeBean.setIdCode("VA-SCH");
            schemeBean.setResourceCode("VALUE");
            
            SchemeBeanList.add(schemeBean);
		}
		
		else if(url.equals("http://labour.gov.in/schemes/grant-aid-child-labour-and-women-labour"))
		{
			Elements class1=doc.getElementsByClass("field-content");
			Elements h2=doc.select("h2");
			for(int i=0;i<h2.size();i++)
			{
				Element title=h2.get(i);
				SchemeBean schemeBean=new SchemeBean();
				String finalDate="";

				Element ne=title.nextElementSibling();
				Element ne1=ne.nextElementSibling();
				
				if(ne1.tagName().equals("ul"))
	            {
	              Elements description1=ne1.select("li");
	              schemeBean.setDescription(ne.text()+" "+description1.text());
	            }
	            else if (!ne1.tagName().equals("ul"))
	            {
	            	schemeBean.setDescription(ne.text());
	            }
//				if(finalDate.isEmpty())
//				{
//				   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//				   Date date = new Date();
//				   finalDate=dateFormat.format(date);
//				}
				schemeBean.setDate(finalDate);
				schemeBean.setTitle("Ministry of Labour and employment "+title.text());
				schemeBean.setId(finalDate+"Ministry of Labour and employment "+title.text());
				schemeBean.setministry("Ministry of Labour and employment");
	            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Labour and employment "+title.text());
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
	           
	            SchemeBeanList.add(schemeBean);
			}
		}
		
		else if(url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=99&lid=130&ltypeid=1&domid=6")||
			    url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=111&lid=138&ltypeid=1&domid=6")||
			    url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=95&lid=124&ltypeid=1&domid=6")||
			    url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=98&lid=129&ltypeid=1&domid=6")||
			    url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=96&lid=125&ltypeid=1&domid=6"))
		{
			String finalDate="";
			Elements p1=doc.select("p");
			Elements title=doc.select("h1");
			SchemeBean schemeBean=new SchemeBean();
			Element description=doc.getElementById("cmscontent");
			Elements p=description.select("p");			
			Elements links=description.select("a[href]");
			
			for(Element link:links)
			{
				//System.out.println(link.absUrl("href"));
				schemeBean.setLink(link.absUrl("href"));
			}
			
			/*if(finalDate.isEmpty())
			{
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			   Date date = new Date();
			   finalDate=dateFormat.format(date);
			}*/
			schemeBean.setDate(finalDate);
			schemeBean.setDescription(description.text());
			schemeBean.setTitle("Ministry of Women & Child Development "+title.text());
			schemeBean.setId(finalDate+"Ministry of Women & Child Development "+title.text());
			schemeBean.setministry("Ministry of Women & Child Development");
            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Ministry of Women & Child Development "+title.text());
			schemeBean.setIdCode("VA-SCH");
            schemeBean.setResourceCode("VALUE");
            
            if(url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=99&lid=130&ltypeid=1&domid=6"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=111&lid=138&ltypeid=1&domid=6"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=95&lid=124&ltypeid=1&domid=6"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=98&lid=129&ltypeid=1&domid=6"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("http://www.nmew.gov.in/index1.php?lang=1&level=1&sublinkid=96&lid=125&ltypeid=1&domid=6"))
            {
            	schemeBean.setUrl(url);
            }
            
            SchemeBeanList.add(schemeBean);
		}
		
		else if(url.equals("http://pharmaceuticals.gov.in/schemes")) ///ASK
		{
			Elements table=doc.select("table");
			Elements rows=table.select("tr");
			String finalDate="";
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				SchemeBean schemeBean=new SchemeBean();
				Elements title=row.select("td:eq(0)");
				Elements details=row.select("td:eq(1)");
			//	System.out.println("Department of Pharmaceuticals "+ title.text());
				Elements links=details.select("a[href]");
				
			    String urls="";
				for(Element link:links)
				{
					urls=link.absUrl("href");
				}
				/*if(finalDate.isEmpty())
				{
				   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				   Date date = new Date();
				   finalDate=dateFormat.format(date);
				}*/
				schemeBean.setDate(finalDate);
				schemeBean.setTitle("Department of Pharmaceuticals "+title.text());
				schemeBean.setLink(urls);
				schemeBean.setId(finalDate+"Department of Pharmaceuticals "+title.text());
	            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Department of Pharmaceuticals "+title.text());
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
	            
	            SchemeBeanList.add(schemeBean);

			}	
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/Soil_Health_Card_Scheme")||
				url.equals("https://en.wikipedia.org/wiki/Rashtriya_Krishi_Vikas_Yojana")||
				url.equals("https://en.wikipedia.org/wiki/Kalyana_Lakshmi")||
				url.equals("https://en.wikipedia.org/wiki/Mukhya_Mantri_Yuva_Swarozgar_Yojna_Scheme")||
				url.equals("https://en.wikipedia.org/wiki/Revised_National_Tuberculosis_Control_Program")||
				url.equals("https://en.wikipedia.org/wiki/Sukanya_Samriddhi_Account")||
				url.equals("https://en.wikipedia.org/wiki/Sanchayaka")||
				url.equals("https://en.wikipedia.org/wiki/Swarna_Jayanti_Shahari_Rozgar_Yojana")
				
				||
				url.equals("https://en.wikipedia.org/wiki/Universal_Immunization_Programme")||
				url.equals("https://en.wikipedia.org/wiki/Swavalamban")||
				url.equals("https://en.wikipedia.org/wiki/Unnat_Jeevan_by_Affordable_LEDs_and_Appliances_for_All")||
				url.equals("https://en.wikipedia.org/wiki/Swarnajayanti_Gram_Swarozgar_Yojana")||
				url.equals("https://en.wikipedia.org/wiki/Swarna_Jayanti_Shahari_Rozgar_Yojana")||
				url.equals("https://en.wikipedia.org/wiki/National_Food_For_Work_Programme")||
				url.equals("https://en.wikipedia.org/wiki/Mahatma_Gandhi_Pravasi_Suraksha_Yojana")||
				url.equals("https://en.wikipedia.org/wiki/Kamdhenu_Yojna")||
				url.equals("https://en.wikipedia.org/wiki/Jyotigram_Yojana")||
				url.equals("https://en.wikipedia.org/wiki/Jalyukt_Shivar")||
				url.equals("https://en.wikipedia.org/wiki/Jago_Grahak_Jago")||
				url.equals("https://en.wikipedia.org/wiki/Integrated_Child_Protection_Scheme")||
				url.equals("https://en.wikipedia.org/wiki/Free_laptop_distribution_scheme_of_the_Uttar_Pradesh_government")||
				url.equals("https://en.wikipedia.org/wiki/Eklavya_Model_Residential_School"))
			
			
			
		{
			doc.getElementsByClass("reference").remove();
			String finalDate="";
			SchemeBean schemeBean=new SchemeBean();
			Element title=doc.getElementById("firstHeading");
		    Elements class1=doc.getElementsByClass("mw-content-ltr");
		    Element paragraph=class1.select("p").first();
		    
		    /*if(finalDate.isEmpty())
			{
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			   Date date = new Date();
			   finalDate=dateFormat.format(date);
			}*/
			schemeBean.setDate(finalDate);
		    schemeBean.setTitle(title.text());
		    schemeBean.setDescription(paragraph.text());
		    schemeBean.setId(finalDate+title.text());
            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+title.text());
			schemeBean.setIdCode("VA-SCH");
            schemeBean.setResourceCode("VALUE");
            if(url.equals("https://en.wikipedia.org/wiki/Rashtriya_Krishi_Vikas_Yojana"))
            {
	            schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Soil_Health_Card_Scheme"))
            {
	            schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Kalyana_Lakshmi"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Mukhya_Mantri_Yuva_Swarozgar_Yojna_Scheme"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Revised_National_Tuberculosis_Control_Program"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Sukanya_Samriddhi_Account"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Sanchayaka"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Swarna_Jayanti_Shahari_Rozgar_Yojana"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Universal_Immunization_Programme"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Swavalamban"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Unnat_Jeevan_by_Affordable_LEDs_and_Appliances_for_All"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Swarnajayanti_Gram_Swarozgar_Yojana"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Swarna_Jayanti_Shahari_Rozgar_Yojana"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/National_Food_For_Work_Programme"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Mahatma_Gandhi_Pravasi_Suraksha_Yojana"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Kamdhenu_Yojna"))
            {
            	schemeBean.setUrl(url);

            }
            else if(url.equals("https://en.wikipedia.org/wiki/Jyotigram_Yojana"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Jalyukt_Shivar"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Jago_Grahak_Jago"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Integrated_Child_Protection_Scheme"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Free_laptop_distribution_scheme_of_the_Uttar_Pradesh_government"))
            {
            	schemeBean.setUrl(url);
            }
            else if(url.equals("https://en.wikipedia.org/wiki/Eklavya_Model_Residential_School"))
            {
            	schemeBean.setUrl(url);
            }
            	SchemeBeanList.add(schemeBean);
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/Welfare_schemes_for_women_in_India"))
		{
			doc.getElementsByClass("mw-editsection").remove();
			doc.getElementsByClass("reference").remove();
			Elements class1=doc.getElementsByClass("mw-body-content");
			Elements h2Value = class1.select("h2");
			String finalDate="";
			for(int i=0;i<h2Value.size();i++)
			{
				Element title=h2Value.get(i);
				
				SchemeBean schemeBean=new SchemeBean();
				
				if(!title.text().contains("References") && !title.text().contains("Contents"))
				{
					
					Element description1=title.nextElementSibling();
					if(description1.tagName().equals("div"))
					{
						Element description2=description1.nextElementSibling();
						schemeBean.setDescription(description2.text());
					}
					else if(!description1.tagName().equals("div"))
					{
						schemeBean.setDescription(description1.text());
					}
					
					/*if(finalDate.isEmpty())
					{
					   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					   Date date = new Date();
					   finalDate=dateFormat.format(date);
					}*/
					schemeBean.setDate(finalDate);
					schemeBean.setTitle("Welfare schemes for women in India "+title.text());
					schemeBean.setId(finalDate+"Welfare schemes for women in India "+title.text());
		            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Welfare schemes for women in India "+title.text());
					schemeBean.setIdCode("VA-SCH");
		            schemeBean.setResourceCode("VALUE");
		            schemeBean.setUrl(url);
		            
		            SchemeBeanList.add(schemeBean);
					
				}
			}
		}
		else if(url.equals("https://en.wikipedia.org/wiki/Swachh_Bharat_Abhiyan"))
		{
			doc.getElementsByClass("mw-editsection").remove();
			doc.getElementsByClass("reference").remove();
			Element title=doc.getElementById("firstHeading");
			String finalDate="";
			
			SchemeBean schemeBean=new SchemeBean();
		
			Elements class1=doc.select("table[class=infobox vcard]");
			for(int i=0;i<class1.size();i++)
			{
				Element class11=class1.get(i);
				Element description=class11.nextElementSibling();
				
				/*if(finalDate.isEmpty())
				{
				   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				   Date date = new Date();
				   finalDate=dateFormat.format(date);
				}*/
				
				if(description.tagName().equals("p"))
				{
					schemeBean.setDescription(description.text());
				}
			}
			schemeBean.setDate(finalDate);
			schemeBean.setTitle(title.text());
			schemeBean.setId(finalDate+title.text());
            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+title.text());
			schemeBean.setIdCode("VA-SCH");
            schemeBean.setResourceCode("VALUE");
            schemeBean.setUrl(url);
            
            SchemeBeanList.add(schemeBean);
			
			
		}
		
		else if(url.equals("http://www.skilldevelopment.gov.in/star.html"))
		{
			Elements titles=doc.getElementsByTag("h3");
			Elements titleHead=doc.getElementsByTag("title");
			String finalDate="";
			for(int i=0;i<titles.size();i++)
			{
				Element title=titles.get(i);
			    SchemeBean schemeBean=new SchemeBean();
			    schemeBean.setTitle(titleHead.text()+" "+title.text().replace(":", ""));
			    Element paragraph1=title.nextElementSibling();
			    Element paragraph2=paragraph1.nextElementSibling();
			    if(paragraph1.tagName().equals("p") && paragraph2.tagName().equals("p"))
			    {
				  schemeBean.setDescription(paragraph1.text()+" "+paragraph2.text());
			    }
			   
			    /*if(finalDate.isEmpty())
			    {
				  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				  Date date = new Date();
				  finalDate=dateFormat.format(date);
			    }*/
				schemeBean.setDate(finalDate);
			    schemeBean.setId(finalDate+titleHead.text()+" "+title.text().replace(":", ""));
	            schemeBean.setIdentifier("VALUE_"+finalDate+"_"+titleHead.text()+" "+title.text().replace(":", ""));
				schemeBean.setIdCode("VA-SCH");
	            schemeBean.setResourceCode("VALUE");
	            schemeBean.setUrl(url);
	            
	            SchemeBeanList.add(schemeBean);
			}
			
		}
		
		else if(url.equals("http://chemicals.gov.in/schemes"))
		{
			Elements table=doc.select("table");
			Elements rows=table.select("tr");
			for(int i=1;i<rows.size();i++)
			{
				Element row=rows.get(i);
				Elements title=row.select("td:eq(0)");
				Elements details=row.select("td:eq(1)");
				Elements date1=row.select("td:eq(2)");
				String date="";
				String finalDate="";
				
				SchemeBean schemeBean=new SchemeBean();
				schemeBean.setTitle("Department of Chemicals and Petro-Chemicals "+title.text());

				Elements links=details.select("a[href]");
				for(Element link:links)
				{
					schemeBean.setLink(link.absUrl("href"));
				}
				
				date=date1.text().replaceAll(",", "").toString();
			    date= date.replace(",", "");
			    String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
				SimpleDateFormat oldDateParser= new SimpleDateFormat("dd/MM/yyyyy");
				SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				finalDate = newDateParser.format(oldDateParser.parse(str));
				
				schemeBean.setDate(finalDate);
				schemeBean.setId(finalDate+"Department of Chemicals and Petro-Chemicals "+title.text());
		        schemeBean.setIdentifier("VALUE_"+finalDate+"_"+"Department of Chemicals and Petro-Chemicals "+title.text());
			    schemeBean.setIdCode("VA-SCH");
		        schemeBean.setResourceCode("VALUE");
		        schemeBean.setUrl(url);
		        
	            SchemeBeanList.add(schemeBean);	
			}
		}
		
		else if(url.equals("https://www.standupmitra.in/Home/SubsidySchemesForAll"))
		{
			Elements head=doc.select("h3");
			Element id=doc.getElementById("Central");
			Elements title1=id.select("div[class=tab-head]");
			Elements table1=id.select("table");
			Elements rows1=table1.select("tr");
			for(int j=1;j<rows1.size();j++)
			{
				String finalDate="";
				Element row1=rows1.get(j);
				SchemeBean schemeBean=new SchemeBean();
				
				Elements title=row1.select("td:eq(1)");
				Elements ministry=row1.select("td:eq(2)");
				Elements description=row1.select("td:eq(3)");
				Elements hreflinks=row1.select("td:eq(4)");
				Elements applicability=row1.select("td:eq(5)");
				Elements links=hreflinks.select("a[href]");
				
				for(Element link:links)
				{
					schemeBean.setLink(link.absUrl("href"));
				}
				
				/*if(finalDate.isEmpty())
				{
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					Date date = new Date();
					finalDate=dateFormat.format(date);
				}*/
				
				schemeBean.setTitle(head.text()+ " "+title1.text()+" "+title.text());
				schemeBean.setDate(finalDate);
				////****
				//schemeBean.setField(ministry.text()+ " "+applicability.text());
				schemeBean.setotherInformation("Applicability "+applicability.text());
				schemeBean.setDescription(description.text());
				schemeBean.setministry(ministry.text());
				schemeBean.setId(finalDate+head.text()+ " "+title1.text()+" "+title.text());
		        schemeBean.setIdentifier("VALUE_"+finalDate+"_"+head.text()+ " "+title1.text()+" "+title.text());
			    schemeBean.setIdCode("VA-SCH");
		        schemeBean.setResourceCode("VALUE");
		        schemeBean.setUrl(url);
		        
	            SchemeBeanList.add(schemeBean);	
			}
		}
	
		
		LOG.info("Generated SchemeBeanList is==>"+SchemeBeanList);
		return SchemeBeanList;
	}

}
