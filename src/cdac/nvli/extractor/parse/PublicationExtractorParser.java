package cdac.nvli.extractor.parse;

import java.util.ArrayList;

import cdac.nvli.extractor.bean.PublicationBean;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.LinkLoopException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;



import com.wuman.jreadability.Readability;

import cdac.nvli.extractor.bean.AwardBean;
import cdac.nvli.extractor.bean.ExaminationBean;
import cdac.nvli.extractor.parse.MysqlAwardExtractorParser;









public class PublicationExtractorParser {

	private static final String XPATH_EXPRESSION_EVENT_TITLE = "/html/head/title";
	private Document doc;
	private W3CDom w3cDom;
	private ArrayList<String> nodeList;
	private ArrayList<PublicationBean> PublicationBeanList; 
	private boolean flag=false;
	private String publicationDocTitle;
	
	private String publicationTitle;
	private String publicationDescription;
	private String publicationUrl;
	
	private String publicationId;
	private String publicationIdentifier;
	/**
	 * Logger
	 */
	private static final Log LOG = LogFactory
			.getLog(AwardExtractorParser.class.getName());
	
	
	public ArrayList<PublicationBean> urlParser(String url) throws IOException, ParseException
	{
		PublicationBeanList=new ArrayList<PublicationBean>();
		URL url1 = new URL(url);
		//System.out.println(url1);
		Document doc = Jsoup.parse(url1, 1000000);
		
		if(url.equals("https://india.gov.in/my-government/publications/archive")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=1")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=2")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=3")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=4")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=5")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=6")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=7")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=8")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=9")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=10")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=11")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=12")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=13")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=14")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=15")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=16")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=17")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=18")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=19")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=20")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=21")||
		   url.equals("https://india.gov.in/my-government/publications/archive?page=22"))
		   {
	        
			 Elements ultag = doc.select("ul.recent_publication li.views-row");
	            for (Element elementli : ultag) 
	            	
	            {
	            	String finalDate="";
	            	PublicationBean publicationBean=new PublicationBean();
	            	
	                Element title= elementli.select("h2.field-content").get(0);
	                Elements info = elementli.select("ul li");
	            
	                String str=info.get(3).text().split(":")[1].replaceFirst(String.valueOf((char) 160),"").trim();
					SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				    finalDate=newDateParser.format(oldDateParser.parse(str));
				    
	                String description = elementli.select("div.field-content").get(2).text();
	               
	                publicationBean.setTitle(title.text());
	               // publicationBean.setAwardRecipientName(info.get(0).text().split(":")[1]);
	                publicationBean.setauthor(info.get(0).text().split(":")[1]);
	               // System.out.println(info.get(0).text().split(":")[1]);
	                publicationBean.setotherInformation("Subject "+info.get(1).text().split(":")[1] + " Language " + info.get(2).text().split(":")[1]);
	                //  publicationBean.setField(info.get(1).text().split(":")[1] + " " + info.get(2).text().split(":")[1]);
	                publicationBean.setDate(finalDate);
	                publicationBean.setDescription(description);
	                publicationBean.setId(finalDate+title.text());
	                publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title.text());
	                publicationBean.setIdCode("VA-PUB");
	                publicationBean.setResourceCode("VALUE");
	                publicationBean.setUrl(url);
	  
	                PublicationBeanList.add(publicationBean);
	              
	            }
		}
		
		else if(url.equals("https://en.wikipedia.org/wiki/Backbencher_(magazine)")||
		   url.equals("https://en.wikipedia.org/wiki/Asana_Journal")||
		   url.equals("https://en.wikipedia.org/wiki/Bike_India")||
		   url.equals("https://en.wikipedia.org/wiki/Building_Giants")||
		   url.equals("https://en.wikipedia.org/wiki/Businessworld")||
		   url.equals("https://en.wikipedia.org/wiki/The_CEO_Magazine_-_India")||
		   url.equals("https://en.wikipedia.org/wiki/CFO_India")||
		   url.equals("https://en.wikipedia.org/wiki/Children%27s_World_(magazine)")||
		   url.equals("https://en.wikipedia.org/wiki/Cine_Blitz")||
		   url.equals("https://en.wikipedia.org/wiki/Civil_Lines_(magazine)"))
		{
			doc.getElementsByClass("reference").remove();
			doc.getElementsByClass("external autonumber").remove();
			Elements titleText=doc.select("title");
			String[] splittedParts = titleText.text().split("-");
			String title="";
			title=splittedParts[0];
			String description="";
			Element div=doc.select("div.mw-content-ltr").first();
			 for(int j=0;j<div.children().size();j++)
				{
					for(Element tagname:div.child(j).getElementsByTag("p"))
					{
						if(tagname.text()!=null||!tagname.text().equals("null")||!tagname.text().equalsIgnoreCase("null")||!tagname.text().isEmpty())
						 description+=tagname.text();
					}
				}
			Elements tables=doc.select("table[class=infobox hproduct]");
			if(tables.size() == 0)
			{
				tables=doc.select("table[class=infobox vcard]");	
			}
			
			if(tables.size() == 0)
			{
            	String finalDate="";
				PublicationBean publicationBean=new PublicationBean();
				/*if(finalDate.isEmpty())
				{
					 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
				}*/
				if(url.equals("https://en.wikipedia.org/wiki/Building_Giants"))
				{
					publicationBean.setUrl(url);
				}
				
				publicationBean.setDate(finalDate);
			    publicationBean.setId(finalDate+title);
		        publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
            	publicationBean.setDescription(description);
            	publicationBean.setTitle(title);
	            publicationBean.setIdCode("VA-PUB");
	            publicationBean.setResourceCode("VALUE");
				PublicationBeanList.add(publicationBean);
			}
			
			else
			{
				Elements rows = tables.get(0).select("tr");
				PublicationBean publicationBean=new PublicationBean();
				int count=0;
				for(Element row : rows)
				{
					Elements td = row.select("td");
					Elements th = row.select("th");
					String date="";
					String finalDate="";
					if(th.size() == 1 && td.size() == 1)
					{
						String thStr = th.get(0).text().toString();
						if(thStr.contains("Editor"))
						{
							//publicationBean.setAwardRecipientName(td.get(0).text().toString());
							publicationBean.seteditor(td.get(0).text().toString());
						}
						/*else if(thStr.contains("Publisher"))
						{
							//publicationBean.setField(td.get(0).text().toString());
							publicationBean.setotherInformation("Publisher "+td.get(0).text().toString());
						}*/
						else if(thStr.contains("First issue"))
						{
							date=td.get(0).text().replaceAll(",", "").toString();
							if(date.matches("^\\d{4}"))
							{
								String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							    finalDate = newDateParser.format(oldDateParser.parse(str)); 
							    publicationBean.setDate(finalDate);
							    publicationBean.setId(finalDate+title);
					            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
							}
							else if(date.matches("[a-zA-Z]{0,10} [0-9]{4}"))
							{
								String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("MMMM yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							    finalDate = newDateParser.format(oldDateParser.parse(str));
							    publicationBean.setDate(finalDate);
							    publicationBean.setId(finalDate+title);
					            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
							}
							else if(date.matches("[0-9]{0,10} [a-zA-Z]{0,10} [0-9]{4}"))
							{
								String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMMM yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							    finalDate = newDateParser.format(oldDateParser.parse(str));
							    publicationBean.setDate(finalDate);
							    publicationBean.setId(finalDate+title);
					            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
							}
							else if(date.matches("[0-9–0-9]{0,10} [a-zA-Z]{0,10} [0-9]{4}"))
							{
								String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("dd–dd MMMM yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							    finalDate = newDateParser.format(oldDateParser.parse(str));  
							    publicationBean.setDate(finalDate);
							    publicationBean.setId(finalDate+title);
					            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
							}
							
						}
						/*else if(finalDate.isEmpty())
						{
							 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						     Date date1 = new Date();
							// finalDate=dateFormat.format(date1);
						}*/
						
					    
						else if(thStr.contains("Website"))
						{
							publicationBean.setLink(td.get(0).text().toString());
						}
						else if(thStr.contains("Language"))
						{
							//publicationBean.setField(td.get(0).text().toString());
							publicationBean.setotherInformation("Language "+td.get(0).text().toString());
						}
						
						publicationBean.setDate(finalDate);
					    publicationBean.setId(finalDate+title);
				        publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
						publicationBean.setDescription(description);
						publicationBean.setIdCode("VA-PUB");
				        publicationBean.setResourceCode("VALUE");
						
					}
					
					if(count==1)
					{
						publicationBean.setTitle(title);
					}
					count++;
					
				}
				
				if(url.equals("https://en.wikipedia.org/wiki/Backbencher_(magazine)"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/Asana_Journal"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/Bike_India"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/Businessworld"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/The_CEO_Magazine_-_India"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/CFO_India"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/Children%27s_World_(magazine)"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/Cine_Blitz"))
				{
					publicationBean.setUrl(url);
				}
				if(url.equals("https://en.wikipedia.org/wiki/Civil_Lines_(magazine)"))
				{
					publicationBean.setUrl(url);
				}

				PublicationBeanList.add(publicationBean);
			}
		}
		
		///complete
		else if(url.equals("https://en.wikipedia.org/wiki/List_of_magazines_in_India"))
		{
			doc.getElementsByClass("reference").remove();
			doc.getElementsByClass("mw-editsection").remove();
		    Elements h2Elements = doc.getElementsByTag("h2");
		    for(Element h2Element : h2Elements)
			{
		    	Element nextElement = h2Element.nextElementSibling();
				if(nextElement != null)
				{
				   boolean isTagNotReached = true;
				   while(isTagNotReached) 
				   {
					 if(nextElement.nodeName().equalsIgnoreCase("ul"))
					   {
						  String languages=h2Element.text();
						  
						  String urls="";
						  String language="";
						  language=languages;
						 // System.out.println("language-: "+language);
						  Elements liList=nextElement.select("li");
						   
						  for(int i=0;i<liList.size();i++)
						  {
							  
							  ArrayList<String> magazinesLink=new ArrayList<String>();
							  Element li=liList.get(i);
							  String title="";
							 
							  title=li.text();
							 // System.out.println("title-: "+title);  
							  
							  if(!title.equals("Time Out – with editions in Bombay, Delhi and Bangalore; a guide to culture in the city") && !title.equals("Reader's Digest – monthly general interest family magazine")&&
								 !title.equals("Time Magazine Asia – weekly"))
							  {
							
							  Elements links=li.select("a[href]");
							  for(Element link:links)
							  {
								  
								  urls=link.absUrl("href");
								  if(!urls.contains("_language"))
								  {
									  magazinesLink.add(urls);
								  }
								  
							  }
							  
							  String magazinesUrl="";
							  for(String str:magazinesLink)
							  {
								  magazinesUrl +=str;
							  }
							// System.out.println("URL_: "+magazinesUrl);
							 doc = Jsoup.connect(magazinesUrl).get();	 
							 doc.getElementsByClass("reference").remove();
							 String description="";
							 Element div=doc.select("div.mw-content-ltr").first();
							 for(int j=0;j<div.children().size();j++)
								{
									for(Element tagname:div.child(j).getElementsByTag("p"))
									{
										if(tagname.text()!=null||!tagname.text().equals("null")||!tagname.text().equalsIgnoreCase("null")||!tagname.text().isEmpty())
										 description+=tagname.text();
									}
								}
							    Elements tableObj=doc.select("table[class=infobox hproduct]");
								if(tableObj.size() == 0)
								{
									tableObj=doc.select("table[class=infobox vcard]");	
								}
								
								if(tableObj.size() == 0)
								{
									String finalDate="";
									PublicationBean publicationBean=new PublicationBean();
									/*if(finalDate.isEmpty())
									{
										 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									     Date date = new Date();
									     finalDate=dateFormat.format(date);
									     publicationBean.setDate(finalDate);
									}*/
									
									publicationBean.setDescription(description);
									//publicationBean.setField(language);
									publicationBean.setotherInformation("Language "+language);
									publicationBean.setTitle(title);
									publicationBean.setUrl(magazinesUrl);
									publicationBean.setId(finalDate+title);
						            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
						            publicationBean.setIdCode("VA-PUB");
						            publicationBean.setResourceCode("VALUE");
						            
									PublicationBeanList.add(publicationBean);
									
								}
								else{
									String date="";
									String finalDate="";
									Elements trObjArr = tableObj.get(0).select("tr");
									PublicationBean publicationBean=new PublicationBean();
									 int count=0;
									 for(Element tr : trObjArr)
									{
										Elements tdObjArr = tr.select("td");
										Elements thObjArr = tr.select("th");
										if(thObjArr.size() == 1 && tdObjArr.size() == 1)
										{
											String thStr = thObjArr.get(0).text().toString();
										
											if(thStr.contains("Editor"))
											{
												//publicationBean.setAwardRecipientName(tdObjArr.get(0).text().toString());
												publicationBean.seteditor(tdObjArr.get(0).text().toString());

											}
//											else if(thStr.contains("Publisher"))
//											{
//												publicationBean.setField(tdObjArr.get(0).text().toString());
//											}
											else if(thStr.contains("First issue"))
											{
											  date=tdObjArr.get(0).text().replaceAll(",", "").toString();
											  if(date.matches("^\\d{4}"))
												{
													String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
													SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
													SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
													newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
												    finalDate = newDateParser.format(oldDateParser.parse(str)); 
												    publicationBean.setDate(finalDate);
												    publicationBean.setId(finalDate+title);
										            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
												}
											  else if(date.matches("[a-zA-Z]{0,10} [0-9]{4}"))
												{
													String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
													SimpleDateFormat oldDateParser= new SimpleDateFormat("MMMM yyyy");
													SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
													newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
												    finalDate = newDateParser.format(oldDateParser.parse(str));
												    publicationBean.setDate(finalDate);
												    publicationBean.setId(finalDate+title);
										            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
												}
												else if(date.matches("[0-9]{0,10} [a-zA-Z]{0,10} [0-9]{4}"))
												{
													String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
													SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMMM yyyy");
													SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
													newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
												    finalDate = newDateParser.format(oldDateParser.parse(str));
												    publicationBean.setDate(finalDate);
												    publicationBean.setId(finalDate+title);
										            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
												}
												else if(date.matches("[0-9–0-9]{0,10} [a-zA-Z]{0,10} [0-9]{4}"))
												{
													String str=date.replaceFirst(String.valueOf((char) 160),"").trim();
													SimpleDateFormat oldDateParser= new SimpleDateFormat("dd–dd MMMM yyyy");
													SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
													newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
												    finalDate = newDateParser.format(oldDateParser.parse(str));  
												    publicationBean.setDate(finalDate);
												    publicationBean.setId(finalDate+title);
										            publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
												}	
											}
											/*else if(finalDate.isEmpty())
											{
											   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											   Date date1 = new Date();
											   finalDate=dateFormat.format(date1);  
											}*/
											 
											/*else if(thStr.contains("Website"))
											{
												publicationBean.setLink(tdObjArr.get(0).text().toString());
											}*/
											 publicationBean.setDate(finalDate);
										     publicationBean.setId(finalDate+title);
									         publicationBean.setIdentifier("VALUE_"+finalDate+"_"+title);
											 publicationBean.setDescription(description);
											//publicationBean.setField(language);
											 publicationBean.setotherInformation("Language "+language);
											 publicationBean.setUrl(magazinesUrl);
								             publicationBean.setIdCode("VA-PUB");
								             publicationBean.setResourceCode("VALUE");
											
										}
										if(count==1)
										{
											publicationBean.setTitle(title);
										}
										count++;
									}
									PublicationBeanList.add(publicationBean);
								}		 
				 }  
						
					 }
					   }
					 else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
					 {
						isTagNotReached = false;
					 }
					 else
					 {
						if(nextElement.nextElementSibling() != null)
						{
						  nextElement = nextElement.nextElementSibling();
						}
					 }
					 break;
				   }
				   
			    }
			 }
		}
		
		else if(url.equals("http://www.iegindia.org/facultypublications/Books"))
		{
			Elements class1=doc.getElementsByClass("publicationHolder");
			for(int i=0;i<class1.size();i++)
			{
				Element class1i=class1.get(i);
				PublicationBean publicationBean=new PublicationBean();
				Elements h5=class1i.select("h5");
				publicationBean.setTitle(h5.text());
				Elements p=class1i.select("p");
				String finalDate="";
				for(int j=0;j<p.size();j++)
				{
					Element pi=p.get(j);
					if(pi.text().contains("Authors:"))
					{
						//publicationBean.setAwardRecipientName(pi.text().replace("Authors:", "").trim());
						publicationBean.setauthor(pi.text().replace("Authors:", "").trim());
					}
					if(pi.text().contains("Year:"))
					{
						String str=pi.text().replace("Year:", "").replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					    finalDate = newDateParser.format(oldDateParser.parse(str)); 
						publicationBean.setDate(finalDate);
						publicationBean.setId(finalDate+h5.text());
						publicationBean.setIdentifier("VALUE_"+finalDate+" "+h5.text());
					}
					if(pi.text().contains("Research Theme:"))
					{
						//publicationBean.setField(pi.text());
						publicationBean.setotherInformation(pi.text());
					}
					
				}
				 publicationBean.setIdCode("VA-PUB");
		         publicationBean.setResourceCode("VALUE");
				 publicationBean.setUrl(url);
		        
				PublicationBeanList.add(publicationBean);
			}
		}
		
		else if(url.equals("http://publicationsdivision.nic.in/NewArrival.aspx")||
		   url.equals("http://publicationsdivision.nic.in/bookshop.aspx"))
		{
			Elements dclass=doc.getElementsByClass("col-lg-12");
			Elements h2Tag=dclass.select("h2");
			String finalDate="";
			for(int j=0;j<h2Tag.size();j++)
			{
				Element h2Values=h2Tag.get(j);
				PublicationBean publicationBean=new PublicationBean();
				publicationBean.setTitle(h2Values.text());
				//System.out.println("title-: "+h2i.text());
				Element h2=h2Values.nextElementSibling();
				//publicationBean.setAwardRecipientName(h2.text());	
				publicationBean.setauthor(h2.text());
				Element h3=h2.nextElementSibling();
				Element h4=h3.nextElementSibling();
				//System.out.println(h4ii.text());
				if(h4!=null)
				{
					//System.out.println("Description-: "+h4ii.text());
					publicationBean.setDescription(h4.text());
				}
			    if(url.equals("http://publicationsdivision.nic.in/NewArrival.aspx"))
			    {
			    	publicationBean.setUrl(url);
			    }
			    else if(url.equals("http://publicationsdivision.nic.in/bookshop.aspx"))
			    {
			    	publicationBean.setUrl(url);
			    }
			   /* if(finalDate.isEmpty())
				{
					 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
				}*/
			    publicationBean.setDate(finalDate);
				publicationBean.setUrl(url);
				publicationBean.setId(finalDate+h2Values.text());
				publicationBean.setIdentifier("VALUE_"+finalDate+"_"+h2Values.text());
				publicationBean.setIdCode("VA-PUB");
				publicationBean.setResourceCode("VALUE");
				
				PublicationBeanList.add(publicationBean);
				//System.out.println(h3ii.text());
				
			}
		}
		
		///error in loading webpage
		else if(url.equals("https://dag.gujarat.gov.in/publications.htm"))
		{
			Elements tab1=doc.select("table");
			Elements rows=tab1.select("tr");
			String finalDate="";
			for(int i=1;i<rows.size();i++)
			{
				String urlLinks="";
				Element row=rows.get(i);
				Elements col1=row.select("td:eq(1)");
				Elements col2=row.select("td:eq(2)");
				PublicationBean publicationBean=new PublicationBean();
				Elements links=col2.select("a[href]");
				for(Element link:links)
				{
					urlLinks=link.absUrl("href");
				}
				/*if(finalDate.isEmpty())
				{
					 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
				     publicationBean.setDate(finalDate);
				}*/
				
				publicationBean.setTitle(col1.text());
				publicationBean.setLink(urlLinks);
				
				//PublicationBeanList.add(publicationBean);
				//System.out.println("Title-: "+col1.text());
				//System.out.println("urlLinks-: "+urlLinks);
			}
		}
		
		else if(url.equals("http://ndpublisher.in/ndpjournals.php?pageno=0")||
		        url.equals("http://ndpublisher.in/ndpjournals.php?pageno=1")||
			    url.equals("http://ndpublisher.in/ndpjournals.php?pageno=2"))
			
		{
			Elements class1=doc.getElementsByClass("jpurnalpageouter");
			for(int i=0;i<class1.size();i++)
			{
				Element classi=class1.get(i);
				PublicationBean publicationBean=new PublicationBean();
				String urlLink="";
				Elements li=classi.select("li");
				Elements ul=classi.select("ul");
				for(int j=0;j<li.size();j++)
				{
					Element lii=li.get(j);
					if(lii.text().contains("Editor In Chief: "))
					{
						//publicationBean.setAwardRecipientName(lii.text());
						publicationBean.seteditor(lii.text());
					}
					if(lii.text().contains("Publisher:"))
					{
						//publicationBean.setField(lii.text());
						publicationBean.setotherInformation(lii.text());
					}
					Elements links=lii.select("a[href]");
					String title="";
					String finalDate="";
					/*
					if(finalDate.isEmpty())
					{
						 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					     Date date = new Date();
					     finalDate=dateFormat.format(date);
					}*/
					
					for(Element link:links)
					{
						urlLink=link.absUrl("href");
						publicationBean.setLink(urlLink);
						Document doc2=Jsoup.connect(urlLink).timeout(100000).get();
						Elements innerClass=doc2.getElementsByClass("heading-bar");
						Elements innerClass2Des=doc2.getElementsByClass("aboutthejournl");
					    
						publicationBean.setDate(finalDate);
						publicationBean.setDescription(innerClass2Des.text());
						publicationBean.setTitle(innerClass.text());
						publicationBean.setId(finalDate+innerClass.text());
						publicationBean.setIdentifier("VALUE_"+finalDate+"_"+innerClass.text());
					}
				}
				publicationBean.setIdCode("VA-PUB");
				publicationBean.setResourceCode("VALUE");
				if(url.equals("http://ndpublisher.in/ndpjournals.php?pageno=0"))
				{
					publicationBean.setUrl(url);
				}
				else if(url.equals("http://ndpublisher.in/ndpjournals.php?pageno=1"))
				{
					publicationBean.setUrl(url);
				}
				else if(url.equals("http://ndpublisher.in/ndpjournals.php?pageno=2"))
				{
					publicationBean.setUrl(url);
				}
				PublicationBeanList.add(publicationBean);
			}
		}
		
		else if(url.equals("http://publicationsdivision.nic.in/Books_freedom.aspx"))
		{
			Elements tables=doc.select("table.dataTables-example");
			Element id=doc.getElementById("dataTables-example");
			Elements tablerows=id.select("tr");
			String finalDate="";
			for(int k=1;k<tablerows.size();k++)
			{
				Element row=tablerows.get(k);
				PublicationBean publicationBean=new PublicationBean();

				Elements col1=row.select("td:eq(1)");
				Elements col2=row.select("td:eq(2)");
				Elements col3=row.select("td:eq(3)");
				
				/*if(finalDate.isEmpty())
				{
					 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
				     publicationBean.setDate(finalDate);
				}*/
				
				publicationBean.setTitle(col1.text());
				//publicationBean.setAwardRecipientName(col2.text());
				publicationBean.setauthor(col2.text());
				publicationBean.setIdentifier("Value_"+finalDate+"_"+col1.text());
				publicationBean.setId(finalDate+col1.text());
				publicationBean.setIdCode("VA-PUB");
				publicationBean.setResourceCode("VALUE");
				publicationBean.setUrl(url);
				
				PublicationBeanList.add(publicationBean);
				
			}
		}
		//SEVERE: Excecption occurred---->null
		else if(url.equals("http://www.ipindia.nic.in/journal-patents.htm"))
		{
			Elements table = doc.select("table");
			System.out.println(table.text());
			Elements rows=doc.select("tr");
			String download="";
			String download2="";
			String download3="";
			for(int j=1;j<rows.size();j++)
			{
				Element row=rows.get(j);
				PublicationBean publicationBean=new PublicationBean();
				
				Elements col1=row.select("td:eq(1)");
				Elements col2=row.select("td:eq(2)");
				Elements col3=row.select("td:eq(3)");
				Elements col4=row.select("td:eq(4)");
				
				Element a=doc.getElementById("ContentPlaceHolder1_JournalPatent1_rptAnnualMaster_rpChildPatentJournal_0_hlTitle_0");
				Element a2=doc.getElementById("ContentPlaceHolder1_JournalPatent1_rptAnnualMaster_rpChildPatentJournal_0_hlTitle_1");
				Element a3=doc.getElementById("ContentPlaceHolder1_JournalPatent1_rptAnnualMaster_rpChildPatentJournal_3_hlTitle_2");
				
				Elements links=a.select("a[href]");
				for(Element link:links)
				{
					download= link.absUrl("href");
				}
				
				Elements links2=a2.select("a[href]");
				for(Element link:links2)
				{
				   download2=link.absUrl("href");
				}
				
				if(col4.text().contains("Part-3_Designs"))
				{
				   Elements links3=a3.select("a[href]");
				   for(Element link:links3)
				   {
					download3=link.absUrl("href");
				   }
				}
				publicationBean.setTitle("Journal No.-: "+col1.text());
				publicationBean.setField("Date of Publication-: "+col2.text()+"Date of Availability-: "+col3.text());
				publicationBean.setLink(download+" "+download2+" "+download3);
				
				//PublicationBeanList.add(publicationBean);

			}
		}
		else if(url.equals("https://www.india.gov.in/my-government/documents/e-books"))
		{
			Elements h3Tag=doc.getElementsByTag("h3");
			String finalDate="";
			for(int i=0;i<h3Tag.size();i++)
			{
				Element title=h3Tag.get(i);
				PublicationBean publicationBean=new PublicationBean();
				publicationBean.setTitle(title.text());
				Elements hreflink=title.select("a[href]");
				for(Element link:hreflink)
				{
					publicationBean.setLink(link.absUrl("href"));
				}
				/*if(finalDate.isEmpty())
				{
					 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
				}*/
				
				Element nextElement=title.nextElementSibling();
				Element description=nextElement.nextElementSibling();
			    
				publicationBean.setDate(finalDate);
				publicationBean.setDescription(description.text());
				publicationBean.setIdentifier("Value_"+finalDate+"_"+title.text());
				publicationBean.setId(finalDate+title.text());
				publicationBean.setIdCode("VA-PUB");
				publicationBean.setResourceCode("VALUE");
				publicationBean.setUrl(url);
				
				PublicationBeanList.add(publicationBean);
			}
		}
		
		else if(url.equals("http://www.nbtindia.gov.in/catalogues__online-index.aspx"))
		{
			Elements div=doc.select("div[style=padding-left: 10px]");
			for(int i=0;i<div.size();i++)
			{
				String data="";
				String finalDate="";
				data=div.get(i).text();
				Elements title=div.get(i).select("font[class=colorredhead]");
				PublicationBean publicationBean=new PublicationBean();
				
				Elements links=title.select("a[href]");
				for(Element link:links)
				{
					publicationBean.setLink(link.absUrl("href"));
				}
				
				/*if(finalDate.isEmpty())
				{
					 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				     Date date = new Date();
				     finalDate=dateFormat.format(date);
				     publicationBean.setDate(finalDate);
				}*/
				
				publicationBean.setTitle(title.text().replace("...", ""));
				publicationBean.setDescription(div.get(i).text().replace(title.text(), "").trim());
				publicationBean.setIdentifier("Value_"+finalDate+"_"+title.text().replace("...", ""));
				publicationBean.setId(finalDate+title.text().replace("...", ""));
				publicationBean.setIdCode("VA-PUB");
				publicationBean.setResourceCode("VALUE");
				publicationBean.setUrl(url);
				
				PublicationBeanList.add(publicationBean);
			}
		}
		
		
				
		
		LOG.info("Generated PublicationBeanList is==>"+PublicationBeanList);
		return PublicationBeanList;
	}
	
	
}
