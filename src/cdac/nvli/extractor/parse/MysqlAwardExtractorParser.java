package cdac.nvli.extractor.parse;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.wuman.jreadability.Readability;

import cdac.nvli.extractor.bean.AwardBean;

import cdac.nvli.extractor.parse.AwardExtractorParser;

public class MysqlAwardExtractorParser 
{
	
	private static final String dbClassName = "com.mysql.jdbc.Driver";
	private static final String CONNECTION ="jdbc:mysql://localhost:3306/testdb?autoReconnect=true&useSSL=false";
   
	
	private String awardTitle;
	private String awardId;
	private String awardIdentifier;

 public void sqlFunction(String awardDocTitle,Document doc,ArrayList<AwardBean> awardBeanList,String awardDescription,String awardUrl) throws ClassNotFoundException, SQLException, ParseException
 {
		
	 if(awardDocTitle.contains("Sangeetha Kalanidhi")||
			awardDocTitle.contains("Sangeetha Kalasikhamani")||
			awardDocTitle.contains("Swaralaya Kairali Yesudas Award")||
			awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Male")||
			awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Female")||
			awardDocTitle.contains("Isai Perarignar")||
			awardDocTitle.contains("Lata Mangeshkar Award")||
			awardDocTitle.contains("Kamal Kumari National Award")||
			awardDocTitle.contains("Jakanachari Award")||
			awardDocTitle.contains("Nishagandhi Puraskaram")||
			awardDocTitle.contains("Vijay Music Awards")||
			awardDocTitle.equalsIgnoreCase("Mirchi Music Awards South - Wikipedia")||
			awardDocTitle.equalsIgnoreCase("Mirchi Music Awards Bangla - Wikipedia")||
			awardDocTitle.equalsIgnoreCase("Mirchi Music Awards - Wikipedia")||
			awardDocTitle.contains("Shilp Guru"))
	{
		 
	  Class.forName(dbClassName);
	  ResultSet rs=null;    
    // Properties for user and password
	Properties p = new Properties();
    p.put("user","root");
	p.put("password","Megha2017");
	// Now try to connect
   Connection c = DriverManager.getConnection(CONNECTION,p);
   Statement st = (Statement) c.createStatement(); 
   
   doc.getElementsByClass("reference").remove();
   doc.getElementsByClass("sortkey").remove();
   doc.getElementsByTag("sup").remove();
   doc.getElementsByTag("small").remove();
   doc.getElementsByClass("mw-editsection").remove();
   
    String colYear=null;
    String colName=null;
    String colField=null;
    String finalDate=null;
    String listName=null;
    String col3=null;
    String col4=null;
    String date1=null;
    Element heading= null;
    
    AwardBean awardBean=new AwardBean();
	String awardId="";
	String awardIdentifier="";
	String awardYear="";
	String awardRecipientName="";
	String awardField="";
	String awardRecipientLocation="";
	String awardLink="";
	String headList="";
	
	Elements tables=doc.getElementsByTag("table");
	for(int j=0;j<tables.size();j++)
	{
		Element table=tables.get(j);
	   
	Elements rows=table.select("tr");
	for(int i=1;i<rows.size();i++)
	{
		Element row=rows.get(i);
		
		if(!table.text().contains("Kathakali")&&!table.text().contains("Tanjavur-style Tambura") &&
		   !table.text().contains("Awarded for") && !table.text().contains("1st Mirchi Music Awards"))
		{
		
		Elements cols=row.select("td");
		
	    int count = 0;
		for(Element col:cols)
		{
			if(col.text().matches("^\\d{4}-\\d{2}"))
		    {
				
				 String[] sp=col.text().split("-");
				 for(String spi:sp)
				 {
					 if(!spi.equals(sp[0]))
					 {
						date1=spi;
						String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));  
						
					 }
				 }
			  }
			
			else if(col.text().matches("^\\d{4}"))
			{
			    String str=col.text().replaceFirst(String.valueOf((char) 160),"").trim();
				SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
				SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				finalDate=newDateParser.format(oldDateParser.parse(str));  
				
			}
	
			String colText=col.text();
			if(count==0)
			{
				 colYear=colText;
			}
			else if(count==1)
			{
				 colName=colText;
			}
			else if(count==2)
			{
				 colField=colText;
			}
			else if(count==3)
			{
				col3=colText;
			}
			else if(count==4)
			{
				col4=colText;
			}
			count++; 
		}
		try
		{
		   if(awardDocTitle.contains("Sangeetha Kalanidhi"))
		   {
		      st.execute("INSERT INTO `sangeethaKalanidhi`(Year,Recipient,Field) VALUE ('"+finalDate+"','"+colName+"','"+colField+"')");  
		   }
		    
		   if(awardDocTitle.contains("Sangeetha Kalasikhamani"))
		   {
		      st.execute("INSERT INTO `sangeethaKalasikhamani`(Year,Recipient) VALUE ('"+finalDate+"','"+colField+"')");
		   }
		   if(awardDocTitle.contains("Swaralaya Kairali Yesudas Award"))
		   {
			  st.execute("INSERT INTO `swaralayaKairaliYesudasAward`(Year,Recipient) VALUE ('"+finalDate+"','"+colName+"')");
		   }
		   if(awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Male"))
		   {
			  st.execute("INSERT INTO `bollywoodMovieAwardMale`(Year,Singer,Film,Song) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"')");
		   }
		   if(awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Female"))
		   {
			  st.execute("INSERT INTO `bollywoodMovieAwardFeMale`(Year,Singer,Film,Song) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"')");
		   }
		   if(awardDocTitle.contains("Isai Perarignar"))
		   {
			  st.execute("INSERT INTO `isaiPerarignar`(Year,Artist,Field) VALUE ('"+finalDate+"','"+colName+"','"+colField+"')");
		   }
		   if(awardDocTitle.contains("Lata Mangeshkar Award"))
		   {
			  st.execute("INSERT INTO `lataMangeshkarAward`(Year,Recipient,Notes) VALUE ('"+finalDate+"','"+colField+"','"+col3+"')");
		   }
		   if(awardDocTitle.contains("Kamal Kumari National Award"))
		   {
			  st.execute("INSERT INTO `kamalKumariNationalAward1`(col1,col2,col3,col4,col5) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"','"+col4+"')");
		   }
		   if(awardDocTitle.contains("Jakanachari Award"))
		   {
			   st.execute("INSERT INTO `jakanachariAward`(col1,col2,col3,col4) VALUE ('"+colName+"','"+colField+"','"+finalDate+"','"+col4+"')");
		   }
		   if(awardDocTitle.contains("Nishagandhi Puraskaram"))
		   {
			   st.execute("INSERT INTO `nishagandhi`(col1,col2,col3) VALUE ('"+finalDate+"','"+colField+"','"+col3+"')");
		   }
		   if(awardDocTitle.contains("Shilp Guru"))
		   {
			  Element para=table.previousElementSibling();
			  Element year=para.previousElementSibling();
			   
			  String str=year.text().replaceFirst(String.valueOf((char) 160),"").trim();
			  SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
			  SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			  newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
			  finalDate=newDateParser.format(oldDateParser.parse(str)); 
			   
			  st.execute("INSERT INTO `shilp`(col1,col2,col3,col4) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"')");
		   }
		   
		   if(awardDocTitle.contains("Vijay Music Awards"))
		   {
			   Element h3=table.previousElementSibling();
				 if(!h3.text().equals("Best Lyricist"))
				 {
				          if(cols.size()==5)
						   { 
							   st.execute("INSERT INTO `vijayMusicAward1`(col1,col2,col3,col4,col5,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"','"+col4+"','"+h3.text()+"')");
						   }
						   if(cols.size()==4)
						   {
							   st.execute("INSERT INTO `vijayMusicAward2`(col1,col2,col3,col4,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"','"+h3.text()+"')"); 
						   }
						   if(cols.size()==3)
						   {
							   st.execute("INSERT INTO `vijayMusicAward3`(col1,col2,col3,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+h3.text()+"')"); 
						   }
						   if(cols.size()==2)
						   {
							   st.execute("INSERT INTO `vijayMusicAward4`(col1,col2,Title) VALUE ('"+finalDate+"','"+colName+"','"+h3.text()+"')"); 
						   }
					
				 } 
		   }
		   if(awardDocTitle.equalsIgnoreCase("Mirchi Music Awards South - Wikipedia"))
		   {
			   String Year="";
			   Element d1=table.previousElementSibling();
			   if(d1.text().matches("^\\d{4}"))
			   {  
				  heading=d1.previousElementSibling();
			
				  String str=d1.text().replaceFirst(String.valueOf((char) 160),"").trim();
				  SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
				  SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				  newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
				  Year=newDateParser.format(oldDateParser.parse(str));
				  
				  if(headList.equalsIgnoreCase("") || !heading.nodeName().equalsIgnoreCase("table")) 
				  {
				     headList=heading.text();
				  }
				  if(cols.size()==4)
				   {
					 st.execute("INSERT INTO `mirchiMusicSouth1`(Year,col1,col2,col3,col4,Title) VALUE ('"+Year+"','"+colYear+"','"+colName+"','"+colField+"','"+col3+"','"+headList+"')");
				   }
				   if(cols.size()==3)
				   {
					  st.execute("INSERT INTO `mirchiMusicSouth2`(Year,col1,col2,col3,Title) VALUE ('"+Year+"','"+colYear+"','"+colName+"','"+colField+"','"+headList+"')"); 
				   }
				   if(cols.size()==2)
				   {
					  st.execute("INSERT INTO `mirchiMusicSouth3`(Year,col1,col2,Title) VALUE ('"+Year+"','"+colYear+"','"+colName+"','"+headList+"')"); 
				   }
				  
		       }
			   else
			   if(cols.size()==4)
			   {
				 heading=d1.previousElementSibling();
				 st.execute("INSERT INTO `mirchiMusicSouth4`(col1,col2,col3,col4,Title) VALUE ('"+finalDate+"','"+colYear+"','"+colName+"','"+colField+"','"+heading.text()+"')");
			   }
		   }
		   
		   if(awardDocTitle.equalsIgnoreCase("Mirchi Music Awards Bangla - Wikipedia"))
		   {
			   if(cols.size()==4)
			   {
				   Element h3=table.previousElementSibling();
				   String title1="";
				   ArrayList<String> title=new ArrayList<String>();
				   
				   if(table.equals(tables.get(1))||table.equals(tables.get(3))||
							table.equals(tables.get(4))||table.equals(tables.get(5))||
							table.equals(tables.get(6))||table.equals(tables.get(7))||
							table.equals(tables.get(8))||table.equals(tables.get(9))||
							table.equals(tables.get(10)))
						  {
							  title1=h3.text()+"-CriticChoiceAwards";
							  title.add(title1);
						  }
					   if(table.equals(tables.get(11)))
						  {
							  title1=h3.text()+"-Popular Awards";
							  title.add(title1);
						  }
					   String titleList="";
						  for(String str1:title)
						  {
							  titleList += str1;
						  }
						 
					titleList =titleList.trim();
					st.execute("INSERT INTO `mirchiMusicAwardBangla1`(Year,col1,col2,col3,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"','"+titleList+"')");

			   }
			   if(cols.size()==3)
			   {
				  Element h3=table.previousElementSibling();
				  String title1="";
				  ArrayList<String> title=new ArrayList<String>();
				  if(table.equals(tables.get(2)))
				   {
					 title1=h3.text()+"-CriticChoiceAwards";
					 title.add(title1);
				   }
				  if(table.equals(tables.get(12)))
				   {
					 title1=h3.text()+"-Popular Awards";
					 title.add(title1);
				   }
				   String titleList="";
				   for(String str1:title)
				   {
					 titleList += str1;
				   }
					 
				 titleList =titleList.trim();
				 st.execute("INSERT INTO `mirchiMusicAwardBangla2`(Year,col1,col2,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+titleList+"')");
			   }
		   }
		    
		   else if(awardDocTitle.equalsIgnoreCase("Mirchi Music Awards - Wikipedia"))
		   {
			   if(cols.size()==5)
			   {
				   Element h3=table.previousElementSibling();
				   String title1="";
				   ArrayList<String> title=new ArrayList<String>();
					  if(table.equals(tables.get(2)))
					  {
						  title1=h3.text()+"-CriticChoiceAwards";
						  title.add(title1);
					  }
					  if(table.equals(tables.get(16)))
					  {
						  title1=h3.text()+"-ListenersChoiceAwards";
						  title.add(title1);
					  }
					  String titleList="";
					  for(String str1:title)
					  {
						  titleList += str1;
					  }
					  titleList =titleList.trim();
				   
				  st.execute("INSERT INTO `mirchiMusicAward1`(Year,Song,Film,Composer,Writer,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"','"+col4+"','"+titleList+"')");
			   }
			  if(cols.size()==3)
			   {
				  ArrayList<String> h3Element=new ArrayList<String>();
				  Element h3=table.previousElementSibling();
				  String title1="";
				  ArrayList<String> title=new ArrayList<String>();
				   
				   if(!h3.text().contains("The award was given to renowned Indian playback singer Lata Mangeshkar in 2008."))
				   {
					  h3Element.add(h3.text());
				   }
				   if(h3.text().contains("The award was given to renowned Indian playback singer Lata Mangeshkar in 2008."))
				   {
					  Element h3a=h3.previousElementSibling();
					  h3Element.add(h3a.text());
				   }
				   String h3List="";
				   for(String str:h3Element)
				   {
					   h3List +=str;
				   }
				  
				   if(awardDocTitle.contains("Mirchi Music Awards"))
				   {
				   if(table.equals(tables.get(3))||table.equals(tables.get(6)))
					  {
						  title1=h3List+"-CriticChoiceAwards";
						  title.add(title1);
					  }
					  if(table.equals(tables.get(15)))
					  {
						  title1=h3List+"-TechnicalAwards";
						  title.add(title1);
					  }
					  if(table.equals(tables.get(17)))
					  {
						  title1=h3List+"-ListenersChoiceAwards";
						  title.add(title1);
					  }
					  if(table.equals(tables.get(18))||
						 table.equals(tables.get(20))  )
					  {
						  title1=h3List+"-Specialawards";
						  title.add(title1);
					  }
				   }
				    String titleList="";
				    for(String str1:title)
				    {
						  titleList += str1;
					}
				    
				   titleList =titleList.trim();
				   st.execute("INSERT INTO `mirchiMusicAward2`(Year,col1,col2,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+titleList+"')");
				   
			   }
			   if(cols.size()==4)
			   {
				   Element h3=table.previousElementSibling();
				   String title1="";
				   ArrayList<String> title=new ArrayList<String>(); 
				  if(awardDocTitle.contains("Mirchi Music Awards"))
				  {
					  if(table.equals(tables.get(4)) || table.equals(tables.get(5))  ||
						 table.equals(tables.get(7)) || table.equals(tables.get(8))  ||
						 table.equals(tables.get(9)) || table.equals(tables.get(10)) ||
						 table.equals(tables.get(11)))
							  {
								  title1=h3.text()+"-CriticChoiceAwards";
								  title.add(title1);
							  }
							  if(table.equals(tables.get(12))||table.equals(tables.get(13))||
								 table.equals(tables.get(14)))
							  {
								  title1=h3.text()+"-TechnicalAwards";
								  title.add(title1);
							  }
							  if(table.equals(tables.get(19)))
							  {
								  title1=h3.text()+"-Specialawards";
								  title.add(title1);
							  }
				  }
				   String titleList="";
				   for(String str1:title)
				   {
						  titleList += str1;
				   }
					 
				 titleList =titleList.trim();
				 st.execute("INSERT INTO `mirchiMusicAward3`(Year,col1,col2,col3,Title) VALUE ('"+finalDate+"','"+colName+"','"+colField+"','"+col3+"','"+titleList+"')");
			   }
			   
		   }
		    
		}//try bracket
		
		catch (SQLException se)
		{
			 if(se instanceof MySQLIntegrityConstraintViolationException)
			     continue;
		   
		}
		
	  } 
			
  }//bracket of rows url;
}//bracket of table
	if(awardDocTitle.equalsIgnoreCase("Mirchi Music Awards South - Wikipedia"))
	{
		String query="SELECT * FROM mirchiMusicSouth1";
		rs=st.executeQuery(query);
		while(rs.next())
		{
			 String Year = rs.getString("Year");
			 String cols1 = rs.getString("col1");
	         String cols2 = rs.getString("col2");
	         String cols3=rs.getString("col3");
	         String cols4=rs.getString("col4");
	         String title=rs.getString("Title");
	         
	         awardYear=Year;
	         awardRecipientName=cols2;
	         awardField=cols3+" "+cols4+" "+cols1;
	         awardTitle="Mirchi Music Awards South "+title;
	         
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
		}
		String query1="SELECT * FROM mirchiMusicSouth2";
		rs=st.executeQuery(query1);
		while(rs.next())
		{
			 String Year=rs.getString("Year");
			 String cols1 = rs.getString("col1");
	         String cols2 = rs.getString("col2");
	         String cols3=rs.getString("col3");
	         String title=rs.getString("Title");
	         
	         awardYear=Year;
	         awardRecipientName=cols2;
	         awardField=cols3+" "+cols1;
	         awardTitle="Mirchi Music Awards South "+title;
	         
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
		}
		String query2="SELECT * FROM mirchiMusicSouth3";
		rs=st.executeQuery(query2);
		while(rs.next())
		{
			 String Year=rs.getString("Year");
			 String cols1 = rs.getString("col1");
	         String cols2 = rs.getString("col2");
	         String title=rs.getString("Title");
	         
	         awardYear=Year;
	         awardRecipientName=cols2;
	         awardField=cols1;
	         awardTitle="Mirchi Music Awards South "+title;
	         
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
		}
		String query3="SELECT * FROM mirchiMusicSouth4";
		rs=st.executeQuery(query3);
		while(rs.next())
		{
			 String cols1 = rs.getString("col1");
	         String cols2 = rs.getString("col2");
	         String cols3=rs.getString("col3");
	         String cols4=rs.getString("col4");
	         String title=rs.getString("Title");
	         
	         awardYear=cols1;
	         awardRecipientName=cols2;
	         awardField=cols3+" "+cols4;
	         awardTitle="Mirchi Music Awards South "+title;
	         
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
		}
		
 }
	
	
	if(awardDocTitle.contains("Shilp Guru"))
	{
		String query="SELECT * FROM shilp";
		rs=st.executeQuery(query);
		while(rs.next())
		{
			 String cols1 = rs.getString("col1");
	         String cols2 = rs.getString("col2");
	         String cols3=rs.getString("col3");
	         String cols4=rs.getString("col4");
	         
	         awardYear=cols1;
	         awardRecipientName=cols2;
	         awardField=cols3+" "+cols4;
	         awardTitle="Shilp Guru";
	         
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
		}
	}
	if(awardDocTitle.contains("Nishagandhi Puraskaram"))
	{
		String query="SELECT * FROM nishagandhi";
		rs=st.executeQuery(query);
		while(rs.next())
		{
			 String cols1 = rs.getString("col1");
	         String cols2 = rs.getString("col2");
	         String cols3=rs.getString("col3");
	         
	         awardYear=cols1;
	         awardRecipientName=cols2;
	         awardField=cols3;
	         awardTitle="Nishagandhi Puraskaram";
	         
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
		}
	}
	 if(awardDocTitle.contains("Jakanachari Award"))
	 {
		 String query="SELECT * FROM jakanachariAward";
			rs=st.executeQuery(query);
			while(rs.next())
			{
				 String cols1 = rs.getString("col1");
		         String cols2 = rs.getString("col2");
		         String cols3=rs.getString("col3");
		         String cols4=rs.getString("col4");
		         
		         awardYear=cols3;
		         awardRecipientName=cols1;
		         awardField=cols2+" "+cols4;
		         awardTitle="Jakanachari Award";
		         
		         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE"));
			}
	 }
	
	
    if(awardDocTitle.contains("Kamal Kumari National Award"))
	{
		String query="SELECT * FROM kamalKumariNationalAward1";
		rs=st.executeQuery(query);
		while(rs.next())
		{
			 String cols1 = rs.getString("col1");
	         String cols2 = rs.getString("col2");
	         String cols3=rs.getString("col3");
	         String cols4=rs.getString("col4");
	         String cols5=rs.getString("col5");
	         
	         awardYear=cols1;
	         awardRecipientName=cols2;
	         awardField=cols3+" "+cols4+" "+cols5;
	         awardTitle="Kamal Kumari National Award";
	         
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
		}
		
	}
	if(awardDocTitle.contains("Vijay Music Awards"))
	{
		String query = "SELECT * FROM vijayMusicAward1";
	    rs = st.executeQuery(query);
	    while (rs.next())
		  {
		         String cols1 = rs.getString("col1");
		         String cols2 = rs.getString("col2");
		         String cols3=rs.getString("col3");
		         String cols4=rs.getString("col4");
		         String cols5=rs.getString("col5");
		         String title=rs.getString("Title");
		         
		         awardYear=cols1;
		         awardRecipientName=cols2;
		         awardField=cols3+" "+cols4+" "+cols5;
		         awardTitle="Vijay Music Awards "+title;
		         
		         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE"));
		       
		  }
	}
    if(awardDocTitle.contains("Vijay Music Awards"))
	{
		String query1 = "SELECT * FROM vijayMusicAward2";
	    rs = st.executeQuery(query1);
	    while (rs.next())
		  {
		         String cols1 = rs.getString("col1");
		         String cols2 = rs.getString("col2");
		         String cols3=rs.getString("col3");
		         String cols4=rs.getString("col4");
		         String title=rs.getString("Title");
		         
		         awardYear=cols1;
		         awardRecipientName=cols2;
		         awardField=cols3+" "+cols4;
		         awardTitle="Vijay Music Awards "+title;
		         
		         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE"));
		       
		  }
	}
    if(awardDocTitle.contains("Vijay Music Awards"))
	{
		String query2 = "SELECT * FROM vijayMusicAward3";
	    rs = st.executeQuery(query2);
	    while (rs.next())
		  {
		         String cols1 = rs.getString("col1");
		         String cols2 = rs.getString("col2");
		         String cols3=rs.getString("col3");
		         String title=rs.getString("Title");
		         
		         awardYear=cols1;
		         awardRecipientName=cols2;
		         awardField=cols3;
		         awardTitle="Vijay Music Awards "+title;
		         
		         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE"));
		       
		  }
	}
    if(awardDocTitle.contains("Vijay Music Awards"))
	{
		String query3 = "SELECT * FROM vijayMusicAward4";
	    rs = st.executeQuery(query3);
	    while (rs.next())
		  {
		         String cols1 = rs.getString("col1");
		         String cols2 = rs.getString("col2");
		         String title=rs.getString("Title");
		         
		         awardYear=cols1;
		         awardRecipientName=cols2;
		         awardTitle="Vijay Music Awards "+title;
		         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE"));
		       
		  }
	}
 //}
	
    //////////////////
    
	if(awardDocTitle.contains("Sangeetha Kalanidhi"))
	{
       awardTitle="Sangeetha Kalanidhi";
	   String query = "SELECT * FROM sangeethaKalanidhi";
       rs = st.executeQuery(query);
       while (rs.next())
	       {
	         String Year = rs.getString("Year");
	         String Recipient = rs.getString("Recipient");
	         String Field=rs.getString("Field"); 
	         
	         awardYear=Year;
	         awardRecipientName=Recipient;
	         awardField=Field;
	         awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE"));
	       
	       }
	}
	
	 if(awardDocTitle.contains("Sangeetha Kalasikhamani"))
	 {
	   
		awardTitle="Sangeetha Kalasikhamani"; 
		String query1="SELECT * FROM sangeethaKalasikhamani";
	    rs=st.executeQuery(query1);
	    while (rs.next())
	    {
	        String Year = rs.getString("Year");
	        String Recipient = rs.getString("Recipient");
	        
	        awardYear=Year;
	        awardRecipientName=Recipient;
	        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
					awardDescription, awardField.trim(),awardRecipientName.trim(),
					awardRecipientLocation,awardYear,
					awardUrl, "VA-AWD", "VALUE"));
	       
	    }
	 }
	
	 if(awardDocTitle.contains("Swaralaya Kairali Yesudas Award"))
	 {
	    awardTitle="Swaralaya Kairali Yesudas Award";
		String query1="SELECT * FROM swaralayaKairaliYesudasAward";
	    rs=st.executeQuery(query1);
	    while (rs.next())
	    {
	       String Year = rs.getString("Year");
	       String Recipient = rs.getString("Recipient");
	        
	       awardYear=Year;
	       awardRecipientName=Recipient;
	       awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
					awardDescription, awardField.trim(),awardRecipientName.trim(),
					awardRecipientLocation,awardYear,
					awardUrl, "VA-AWD", "VALUE"));
	    }
	 }
	 
	 if(awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Male"))
	 {
		awardTitle="Bollywood Movie Award – Best Playback Singer Male";
		String query1="SELECT * FROM bollywoodMovieAwardMale";
	    rs=st.executeQuery(query1);
	    while (rs.next())
	    {
	        String Year = rs.getString("Year");
	        String Singer = rs.getString("Singer");
	        String Film=rs.getString("Film");
	        String Song=rs.getString("Song");
	        
	        awardYear=Year;
	        awardRecipientName=Singer;
	        awardField=Film+" "+Song;
	        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
					awardDescription, awardField.trim(),awardRecipientName.trim(),
					awardRecipientLocation,awardYear,
					awardUrl, "VA-AWD", "VALUE"));
	    }
	 }
	 if(awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Female"))
	 {
	    awardTitle="Bollywood Movie Award – Best Playback Singer Female";
		String query1="SELECT * FROM bollywoodMovieAwardFeMale";
	    rs=st.executeQuery(query1);
	    while (rs.next())
	    {
	        String Year = rs.getString("Year");
	        String Singer = rs.getString("Singer");
	        String Film=rs.getString("Film");
	        String Song=rs.getString("Song");
	       
	        awardYear=Year;
	        awardRecipientName=Singer;
	        awardField=Film+" "+Song;
	        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
					awardDescription, awardField.trim(),awardRecipientName.trim(),
					awardRecipientLocation,awardYear,
					awardUrl, "VA-AWD", "VALUE"));
	       
	    }
	 }
	 if(awardDocTitle.contains("Isai Perarignar"))
	 {
	    awardTitle="Isai Perarignar" ;
		String query1="SELECT * FROM isaiPerarignar";
	    rs=st.executeQuery(query1);
	    while (rs.next())
	    {
	        String Year = rs.getString("Year");
	        String Artist = rs.getString("Artist");
	        String Field=rs.getString("Field");
	
	        awardYear=Year;
	        awardRecipientName=Artist;
	        awardField=Field;
	        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
					awardDescription, awardField.trim(),awardRecipientName.trim(),
					awardRecipientLocation,awardYear,
					awardUrl, "VA-AWD", "VALUE"));
	       
	    }
	 }
	 if(awardDocTitle.contains("Lata Mangeshkar Award"))
	 {
	    awardTitle="Lata Mangeshkar Award";
		String query1="SELECT * FROM lataMangeshkarAward";
	    rs=st.executeQuery(query1);
	    while (rs.next())
	    {
	        String Year = rs.getString("Year");
	        String Recipient = rs.getString("Recipient");
	        String Notes=rs.getString("Notes");
	        
	        awardYear=Year;
	        awardRecipientName=Recipient;
	        awardField=Notes;
	        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
					awardDescription, awardField.trim(),awardRecipientName.trim(),
					awardRecipientLocation,awardYear,
					awardUrl, "VA-AWD", "VALUE"));
	    }
	 }
	 
	if(awardDocTitle.equalsIgnoreCase("Mirchi Music Awards Bangla - Wikipedia"))
	 {
		 String query="SELECT * FROM mirchiMusicAwardBangla1";
		 rs=st.executeQuery(query);
			    while (rs.next())
			    {
			       String Year = rs.getString("Year");
			       String col1 = rs.getString("col1");
			       String col2=rs.getString("col2");
			       String cols3=rs.getString("col3");
			       String title=rs.getString("Title");
			       
			       awardYear=Year;
			       awardTitle="Mirchi Music Awards Bangla"+" "+title;
			       awardRecipientName=col1;
			       awardField=col2+" "+col3; 
			       
			       awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE")); 
			    }
		 
		 String query1="SELECT * FROM mirchiMusicAwardBangla2";
		 rs=st.executeQuery(query1);
			    while (rs.next())
			    {
			      String Year = rs.getString("Year");
			      String col1 = rs.getString("col1");
			      String col2=rs.getString("col2");
			      String title=rs.getString("Title");
			        
			      awardYear=Year;
			      awardTitle="Mirchi Music Awards Bangla"+" "+title;
			      awardRecipientName=col1;
			      awardField=col2;
		
			       awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
						awardDescription, awardField.trim(),awardRecipientName.trim(),
						awardRecipientLocation,awardYear,
						awardUrl, "VA-AWD", "VALUE")); 
			    }
	
	 }
	
	
	 //// Mirchi Music Awards
	 else if(awardDocTitle.equalsIgnoreCase("Mirchi Music Awards - Wikipedia"))
	 {
		 String query1="SELECT * FROM mirchiMusicAward1";
		 rs=st.executeQuery(query1);
			    while (rs.next())
			    {
			        String Year = rs.getString("Year");
			        String Song = rs.getString("Song");
			        String Film=rs.getString("Film");
			        String Composer=rs.getString("Composer");
			        String Writer=rs.getString("Writer");
			        String title=rs.getString("Title");
			        
			        awardYear=Year;
			        awardTitle="Mirchi Music Awards"+" "+title;
			        awardRecipientName=Song;
			        awardField=Film+" "+Composer+" "+Writer;
			       
			        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE")); 
			    }
		 String query2="SELECT * FROM mirchiMusicAward3";
		 rs=st.executeQuery(query2);
			    while (rs.next())
			    {
			        String Year = rs.getString("Year");
			        String col1 = rs.getString("col1");
			        String col2=rs.getString("col2");
			        String cols3=rs.getString("col3");
			        String title=rs.getString("Title");
			       
			        awardYear=Year;
			        awardTitle="Mirchi Music Awards"+" "+title;
			        awardRecipientName=col1;
			        awardField=col2+" "+col3; 
			       
			        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE")); 
			    }
	
		 String query3="SELECT * FROM mirchiMusicAward2";
		 rs=st.executeQuery(query3);
			    while (rs.next())
			    {
			        String Year = rs.getString("Year");
			        String col1 = rs.getString("col1");
			        String col2=rs.getString("col2");
			        String title=rs.getString("Title");
			        
			        awardYear=Year;
			        awardTitle="Mirchi Music Awards"+" "+title;
			        awardRecipientName=col1;
			        awardField=col2;
		
			        awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE")); 
			        
			    }
	
	 }
	 
	 
	 ////Bangla

	  
    try
    {
        if(c != null)
           c.close();
    } 
    catch(SQLException se) 
    {
        se.printStackTrace();
    }
   
    st.close();	
   
	}
	
}
public  AwardBean awardBeanValue(String id,String link,String Identifier,String title,String description,String field,String name,String location,String year,String url,String VA_AWD,String VALUE)
{
		AwardBean awardBean=new AwardBean();
		
		awardId=year+name.replaceAll(" ", "");
		awardIdentifier="VALUE_"+year+"_"+name.replaceAll(" ", "");
		
		awardBean.setId(awardId);
		awardBean.setLink(link);
		awardBean.setIdentifier(awardIdentifier);
		awardBean.setTitle(title);
		awardBean.setDescription(description);
		awardBean.setField(field);
		awardBean.setAwardRecipientName(name);
		awardBean.setLocation(location);
		awardBean.setDate(year);
		awardBean.setUrl(url);
		awardBean.setIdCode("VA-AWD");
		awardBean.setResourceCode("VALUE"); 
		return awardBean;
		
}	
	
}
	
