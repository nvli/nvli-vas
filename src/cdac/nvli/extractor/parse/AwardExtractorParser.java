package cdac.nvli.extractor.parse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.LinkLoopException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;



import com.wuman.jreadability.Readability;

import cdac.nvli.extractor.bean.AwardBean;

import cdac.nvli.extractor.parse.MysqlAwardExtractorParser;



/**
 * This class provides common methods for implementations of
 * Award extraction and parsing 
 * @author pragya
 *
 */
public class AwardExtractorParser{
	
	////
	//private static final String dbClassName = "com.mysql.jdbc.Driver";
	//private static final String CONNECTION ="jdbc:mysql://localhost:3306/testdb?autoReconnect=true&useSSL=false";
	////
	private static final String XPATH_EXPRESSION_EVENT_TITLE = "/html/head/title";
	private Document doc;
	private W3CDom w3cDom;
	private ArrayList<String> nodeList;
	private ArrayList<AwardBean> awardBeanList; 
	private boolean flag=false;
	private String awardDocTitle;
	//////
	private String awardStateTitle;
	private String award_Title;
	
	private String awardTitle;
	private String awardDescription;
	private String awardUrl;
	
	private String awardId;
	private String awardIdentifier;
	/**
	 * Logger
	 */
	private static final Log LOG = LogFactory
			.getLog(AwardExtractorParser.class.getName());
	
		
	/**
	 * 
	 * @param url from the seed url list that 
	 * will be extracted and parsed 
	 * @return
	 * ArrayList of AwardBean model class
	 */
	public ArrayList<AwardBean> parseIt(String url) 
	{
		awardBeanList=new ArrayList<AwardBean>();
		try {
				doc=Jsoup.connect(url).get();
				
				
				awardUrl=url;
				Element tableData=doc.select("table.sortable").first();
				awardDocTitle=doc.select("title").text();
				System.out.println("title of url "+awardUrl+" is ***"+awardDocTitle); 
				awardDescription="";
				Element div=doc.select("div.mw-content-ltr").first();
				
				
				award_Title=doc.select("title").text();
				awardUrl=url;
				awardStateTitle=doc.select("h3").text();
				
				ArrayList<String> nodeList=new ArrayList<>();
				
				boolean flagTobreak=false;
			if(awardDocTitle.contains("Wikipedia"))	
			{	
				doc.getElementsByClass("reference").remove();
				for(Element tagname:div.child(0).getElementsByTag("th"))
				{
					if(tagname.text()!=null||!tagname.text().equals("null")||!tagname.text().equalsIgnoreCase("null")||!tagname.text().isEmpty())
					 {
						
						LOG.info("Extracted Title is==>"+tagname.text());
						awardTitle=tagname.text();
						break;
					 }
				}
				
							
				for(int i=0;i<div.children().size();i++)
				{
					for(Element tagname:div.child(i).getElementsByTag("p"))
					{
						if(tagname.text()!=null||!tagname.text().equals("null")||!tagname.text().equalsIgnoreCase("null")||!tagname.text().isEmpty())
						 awardDescription+=tagname.text();
					}
				}
				LOG.info("Extracted awardDescription is==>"+awardDescription);
				
				if(awardDocTitle.contains("Param Vir Chakra"))
				{	
					awardTitle="Param Vir Chakra";
					System.out.println("title is "+awardTitle); 
				for(int i=0;i<tableData.children().size();i++)
					{
						Element trData=tableData.child(i);
						
						for(int j=1;j<trData.children().size();j++)
						{
							
							LOG.info("Extracted child is==>"+trData.child(j).text());
							Element tdData=trData.child(j);
							for(int k=0;k<tdData.children().size();k+=6)
							{
								
								LOG.info("Extracted tddata==> "+tdData.child(k).text());
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								awardRecipientName=tdData.child(k).text();
								String date=tdData.child(k+2).text(); 
								System.out.println("date is"+date); 
								date=date.substring(date.lastIndexOf("0"),date.length());
								System.out.println("new date is"+date); 
								
								SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMMM yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								
								
								LOG.info("olddateparser"+oldDateParser.parse(date));
								awardYear=newDateParser.format(oldDateParser.parse(date)); 
								
								awardField=tdData.child(k+3).text();
								awardRecipientLocation=tdData.child(k+4).text();
								
								  awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
							
								if(k==tdData.children().size()-1)
									break;
							}
						}
					}
				}
				else if(awardDocTitle.contains("Ashoka Chakra"))
					{	
					awardTitle="Ashoka Chakra";
					System.out.println("title is "+awardTitle); 
					for(int i=0;i<tableData.children().size();i++)
						{
							Element trData=tableData.child(i);
							System.out.println("trdata: "+trData.text());
							for(int j=1;j<trData.children().size();j++)
							{
								System.out.println("tddata: "+trData.child(j).text());
								Element tdData=trData.child(j);
								for(int k=0;k<tdData.children().size();k+=3)
								{
									System.out.println("tddata at 126: "+tdData.child(k).text());
									AwardBean awardBean=new AwardBean();
									String awardId="";
									String awardYear="";
									String awardIdentifier="";
									String awardRecipientName="";
									String awardField="";
									String awardRecipientLocation="";
									String awardLink=null;
									
									String date=tdData.child(k).text();
									SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
									SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
									
									System.out.println("olddateparser"+oldDateParser.parse(date)); 
									
									awardYear=newDateParser.format(oldDateParser.parse(date)); 
									System.out.println("awardYear"+awardYear); 
									
								
									awardRecipientName=tdData.child(k+1).text();
									  awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
													awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
								
									if(k==tdData.children().size()-1)
										break;
								}
							}
						}
					}
				else if(awardDocTitle.contains("Maha Vir Chakra"))
				{	
					awardTitle="Maha Vir Chakra";
					System.out.println("title is "+awardTitle); 
					Element tableDataMV=doc.select("table.wikitable").get(1);
				for(int i=0;i<tableDataMV.children().size();i++)
					{
						Element trData=tableDataMV.child(i);
						System.out.println("trdata: "+trData.text());
						for(int j=1;j<trData.children().size();j++)
						{
							System.out.println("tddata: "+trData.child(j).text());
							Element tdData=trData.child(j);
							for(int k=0;k<tdData.children().size();k+=5)
							{
								System.out.println("tddata at 126: "+tdData.child(k+2).text());
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								
								
								String date=tdData.child(k+4).text();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("dd-MMM-yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								
								System.out.println("olddateparser"+oldDateParser.parse(date)); 
								
								awardYear=newDateParser.format(oldDateParser.parse(date)); 
								System.out.println("awardYear"+awardYear); 
								
								
								awardRecipientName=tdData.child(k+2).text();
								awardRecipientLocation=tdData.child(k+3).text();
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
							
								if(k==tdData.children().size()-1)
									break;
							}
						}
					}
				}
				else if(awardDocTitle.contains("Bharat Ratna"))
				{
					awardTitle="Bharat Ratna";
					System.out.println("title is "+awardTitle); 
					Element tableDataBharatRatna=doc.select("table.plainrowheaders").first();
					for(int i=1;i<tableDataBharatRatna.children().size();i++)
					{
						
						Element trDataBharatRatna=tableDataBharatRatna.child(i);
						for(int j=1;j<trDataBharatRatna.children().size();j++)
						{
							Element tdDataBharatRatna = trDataBharatRatna.child(j);
							
							for(int tdChildNode=0;tdChildNode<tdDataBharatRatna.children().size();tdChildNode++)
							{
								boolean flag=true;
								System.out.println("node value at 74 is"+tdDataBharatRatna.child(tdChildNode).text()+"size"+tdDataBharatRatna.children().size()); 
								String tdData=tdDataBharatRatna.child(tdChildNode).text().trim();
								if(tdData.isEmpty() || tdData.equalsIgnoreCase(""))
								{
									flag=false;
								}
								else if(tdData.equals(" –"))
								{
									
									flag=false;
								}
								if(flag==true)
								{
									nodeList.add(tdData);
								}
								
							}
						}
					}
				    for(int i=0;i<nodeList.size();i++)
				    {
				    	System.out.println("list value is "+nodeList.get(i).trim());  
				    	
				    	for(int x=(i+1);x<nodeList.size();x+=2)
						{
				    		AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							String date=nodeList.get(i);
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							
							System.out.println("olddateparser"+oldDateParser.parse(date)); 
							
							awardYear=newDateParser.format(oldDateParser.parse(date)); 
							System.out.println("awardYear"+awardYear); 
							
							
							
							System.out.println("node value is 82"+nodeList.get(x)); 
							
							if((x+1)==nodeList.size()-1)
								flagTobreak=true;
							else if(nodeList.get(x).matches("^[0-9]*$") && flagTobreak==false)
							{
								System.out.println("in if"+nodeList.get(x));
								i=x-1;
								break;
							}
								awardRecipientName=nodeList.get(x);
								awardField=nodeList.get(x+1);
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
						}
				    	
				    	if(flagTobreak==true)
				    		break;
				    }
				}
				
///##############################################
////////#############################
				else if(awardDocTitle.contains("Indira Gandhi Prize"))
				{
					awardTitle="Indira Gandhi Prize";
					doc.getElementsByClass("flagicon").remove();
					System.out.println("title is "+awardTitle); 
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					 for(int i=1;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 Elements col_date=row.select("td:eq(0)");
						 Elements col_name=row.select("td:eq(1)");
						 Elements col_country=row.select("td:eq(4)");
						 
						 
						 Elements link_name=col_name.select("a[href]");
						 String list_name=null;
						 for(Element links:link_name)
						 {
							 list_name =links.absUrl("href");
						 }
						
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name.text();
							String awardField="";
							String awardRecipientLocation=col_country.text();
							String awardLink=list_name;
							
						 
						 String date1=null;
						 String finalDate=null;
						 date1=col_date.text();
						 
						 String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
			  		     SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						 System.out.println("olddateparser"+oldDateParser.parse(str)); 
						 awardYear=newDateParser.format(oldDateParser.parse(str)); 
			  		    
						 System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;	
			  		     
					 }
					
				}
				
				else if (awardDocTitle.contains("Jnanpith Award"))
				{	
					awardTitle="Jnanpith Award";
					System.out.println("title is "+awardTitle); 
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=2;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 
						 Elements col_date=row.select("td:eq(0)");
						 Elements col_name=row.select("th");
						 Elements col_name_final=col_name.select("a");
						
						 Elements link_name=col_name_final.select("a[href]");
						 String list_name=null;
						 for(Element links:link_name)
						 {
							 list_name =links.absUrl("href");
						 }
						 Elements col_language=row.select("td:eq(2)");
						 Elements col_lang=col_language.select("a");
						 doc.select("small").remove();
						
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name_final.text();
							String awardField=col_lang.text();
							String awardRecipientLocation="";
							String awardLink=list_name;
						 
						 String date1=null;
						 String finalDate=null;
						 date1=col_date.text();
						 String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
						
						 if(date1.matches("[0-9]{4}"))
						{
	
				  		    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							System.out.println("olddateparser"+oldDateParser.parse(str)); 
							awardYear=newDateParser.format(oldDateParser.parse(str)); 
				  		    
						}
						else
						{
					
				  		    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy †"); 
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							System.out.println("olddateparser"+oldDateParser.parse(str)); 
							awardYear=newDateParser.format(oldDateParser.parse(str)); 
				  		    
						}
						 System.out.println("awardYear"+awardYear); 
						 awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;		
						 
				 }
				}
				
				else if (awardDocTitle.contains("Sarvottam Yudh Seva Medal"))
				{	
					awardTitle="Sarvottam Yudh Seva Medal";
					System.out.println("title is "+awardTitle); 
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 Elements col_name=row.select("td:eq(1)");
						 Elements col_corps=row.select("td:eq(2)");
						
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name.text();
							String awardField=col_corps.text();
							String awardRecipientLocation="";
							String awardLink="";
						
							 System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;	
			
					 }
				}
				else if (awardDocTitle.contains("List of Sahitya Akademi fellows"))
				{	
					awardTitle="List of Sahitya Akademi fellows";
					System.out.println("title is "+awardTitle); 
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					
					for(int i=5;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 Elements col_date=row.select("td:eq(0)");
						 Elements col=row.select("th");
						 Elements col_name=col.select("a");
						 Elements links=col_name.select("a[href]");
						 String list_name=null;
						 for(Element link: links )
						 {
							 list_name=link.absUrl("href");
						 }
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name.text();
							String awardField="";
							String awardRecipientLocation="";
							String awardLink=list_name;
							
						 String date1=null;
						 String finalDate=null;
						 date1=col_date.text();
						 String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
						 if(date1.matches("[0-9]{4}"))
						 {
							    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								System.out.println("olddateparser"+oldDateParser.parse(str)); 
								awardYear=newDateParser.format(oldDateParser.parse(str)); 
						 }
						 System.out.println("awardYear"+awardYear); 
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;		
						
						 
					 }
				}
				else if (awardDocTitle.contains("Dronacharya Award"))
				{	
					doc.getElementsByClass("reference").remove();
					doc.getElementsByClass("sortkey").remove();
					doc.text().replaceAll("#", "");
					awardTitle="Dronacharya Award";
					System.out.println("title is "+awardTitle); 
					
					Element table1=doc.select("table").get(3);
					Elements rows=table1.select("tr");
					String finalDate=null;
					
					for(int i=2;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 if(!row.text().contains("No award"))
						 {
						 Elements col_date=row.select("td:eq(0)");
						 Elements col_field=row.select("td:eq(2)");
						 
						 Elements col=row.select("th");
				  		 Elements col_name=col.select("a");
				  		 Elements link=col_name.select("a[href]");
				  		 String list_name=null; 
				  		  
				  		 for(Element links:link)
				  		   {
				  			   list_name=links.absUrl("href");
				  		   }
				  		   
				  		 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col.text();
							String awardField=col_field.text();
							String awardRecipientLocation="";
							String awardLink=list_name;
							
						String str=col_date.text().replaceFirst(String.valueOf((char) 160),"").trim();
			  		     
			  		     SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");   
						 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						 System.out.println("olddateparser"+oldDateParser.parse(str)); 
						 awardYear=newDateParser.format(oldDateParser.parse(str)); 
						 
						 System.out.println("awardYear"+awardYear); 
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;	
						 }

					 }
					
				}
				else if (awardDocTitle.contains("Dhyan Chand Award"))
				{	
					doc.getElementsByClass("sortkey").remove();
					awardTitle="Dhyan Chand Award";
					System.out.println("title is "+awardTitle);
					Element table1=doc.select("table").get(2);
					 Elements rows=table1.select("tr");
					 String finalDate=null;
					 
					 for(int i=1;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 Elements col_date=row.select("td:eq(0)");
						 Elements col_field=row.select("td:eq(2)");
						 Elements col_name=row.select("th");
			  		     doc.getElementsByClass("sortkey").remove();
			  		     Elements col_list=col_name.select("a");
			  		     Elements links=col_name.select("a[href]");
			  		     String list_name=null;
			  		     for(Element link:links)
			  		     {
			  		    	 list_name=link.absUrl("href");
			  		     }
						 
			  		   AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName=col_name.text();
						String awardField=col_field.text();
						String awardRecipientLocation="";
						String awardLink=list_name;
			  		     
						String str=col_date.text().replaceFirst(String.valueOf((char) 160),"").trim();
			  		    
						SimpleDateFormat oldDateParser=new SimpleDateFormat("yyyy"); 
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						System.out.println("olddateparser"+oldDateParser.parse(str)); 
						awardYear=newDateParser.format(oldDateParser.parse(str)); 
			  		    
						 System.out.println("awardYear"+awardYear); 
							System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;		
						
					 }
				}
				
				else if (awardDocTitle.contains("Rajiv Gandhi Khel Ratna"))
				{	
					 doc.getElementsByClass("reference").remove();
					 awardTitle="Rajiv Gandhi Khel Ratna";
					 System.out.println("title is "+awardTitle);
					 Element table1=doc.select("table").get(4);
					 Elements rows=table1.select("tr");
					 String finalDate=null;
					 
					 for(int i=1;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 if(!row.text().contains("No award"))
						 {
						 Elements col_date=row.select("td:eq(0)");
					
						 Elements col=row.select("th");
						 Elements col_name=col.select("a");
						 Elements link=col_name.select("a[href]");
						 String list_name=null;
						 for(Element links:link)
						 {
							 list_name=links.absUrl("href");
						 }
						Elements col_field=row.select("td:eq(2)");
						
						 
						 
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name.text();
							String awardField=col_field.text();
							String awardRecipientLocation="";
							String awardLink=list_name;
							
							String[] splittedParts1 = col_date.text().split("–");
							String date1=null;
							ArrayList<String> dateList = new ArrayList<String>();
							for(String s:splittedParts1)
							 {
								 date1=s;
								 if(date1.matches("[0-9]{4}"))
								 {
						     		    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
										SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
										System.out.println("olddateparser"+oldDateParser.parse(date1)); 
										awardYear=newDateParser.format(oldDateParser.parse(date1)); 
										dateList.add(awardYear);
								 }
								 
								 else if(date1.matches("[0-9]{2}"))
								 {
						     		    SimpleDateFormat oldDateParser=  new SimpleDateFormat("yy"); 
										SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
										System.out.println("olddateparser"+oldDateParser.parse(date1)); 
										awardYear=newDateParser.format(oldDateParser.parse(date1)); 
										dateList.add(awardYear);
						     		    
								 }
								 else 
								 {  
									    SimpleDateFormat oldDateParser=  new SimpleDateFormat("yyyy †"); 
										SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
										System.out.println("olddateparser"+oldDateParser.parse(date1)); 
										awardYear=newDateParser.format(oldDateParser.parse(date1));
										dateList.add(awardYear);   
								 } 
							 }
							 System.out.println("awardYear"+dateList); 
							 System.out.println("awardYear"+awardYear); 
								  awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;		
						 }
					 }
				}
				else if(awardDocTitle.contains("Gandhi Peace Prize"))
				{
					 doc.getElementsByClass("flagicon").remove();
					 doc.getElementsByClass("reference").remove();
					 awardTitle="Gandhi Peace Prize";
					 System.out.println("title is "+awardTitle);
					 Elements table=doc.getElementsByClass("wikitable");
					 Elements rows=table.select("tr");
					 for(int i=2;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 Elements col_date=row.select("td:eq(0)");
						 Elements col_name=row.select("td:eq(1)");
						 Elements col_country=row.select("td:eq(4)");
						 Elements col_description=row.select("td:eq(5)");
						 Elements link_name=col_name.select("a[href]");
						
						 String list_name=null;
						
						 for(Element links:link_name)
						 {
							 list_name =links.absUrl("href");
						 }
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name.text();
							String awardField="";
							String awardRecipientLocation=col_country.text();
							String awardLink=list_name;
						
						 String date1=null;
						 String finalDate=null;
						 date1=col_date.text();
						 String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
						 if(date1.matches("[0-9]{4}"))
						 {
				     		    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								System.out.println("olddateparser"+oldDateParser.parse(date1)); 
								awardYear=newDateParser.format(oldDateParser.parse(date1)); 
						 }
						 System.out.println("awardYear"+awardYear); 
						
								  awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;	
						 
						 
						 
					 }
				}
				else if(awardDocTitle.contains("Kirti Chakra"))
				{
					 awardTitle="Kirti Chakra";
					 System.out.println("title is "+awardTitle);
					 Elements table=doc.getElementsByClass("wikitable");
					 Elements rows=table.select("tr");
					 for(int i=1;i<rows.size();i++)
					 {
						 Element row=rows.get(i);
						 Elements col_name=row.select("td:eq(2)");
						 Elements link_name=col_name.select("a[href]");
						 String link_list_name=null;
						 
						 for(Element links : link_name)
						 {
							 link_list_name=links.absUrl("href");
						 }
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name.text();
							String awardField="";
							String awardRecipientLocation="";
							String awardLink=link_list_name;
						 
							System.out.println(col_name.text()+link_list_name);
						 Elements col_regiment=row.select("td:eq(3)");
						 Elements col_date=row.select("td:eq(4)");
						 
						 String str=col_date.text().replaceFirst(String.valueOf((char) 160),"").trim();
			  		     
			  		    SimpleDateFormat oldDateParser=  new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						System.out.println("olddateparser"+oldDateParser.parse(str)); 
						awardYear=newDateParser.format(oldDateParser.parse(str)); 
						
						System.out.println("awardYear"+awardYear); 
						
						//awardBeanList.add(awardBean);
						//System.out.println("awardYear"+awardYear); 
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;	
			  		  
					 }
						 
				}
				
				else if (awardDocTitle.contains("Arjuna Award"))
				{	
					 awardTitle="Arjuna Award";
					 System.out.println("title is "+awardTitle);
					 Elements h2Elements = doc.getElementsByTag("h2");
					 doc.getElementsByClass("mw-editsection-bracket").remove();
					 String list_name=null;
					 for(Element h3Element : h2Elements)
					 {
							Element nextElement = h3Element.nextElementSibling();
							if(nextElement != null)
							{
								boolean isTableNotReached = true;
								while(isTableNotReached) 
								{
									if(nextElement.nodeName().equalsIgnoreCase("table"))
									{
										String tableTitle = h3Element.text();
										if(!tableTitle.equals("External links[edit]"))
										{
										
										Element table = nextElement;
										Elements rows = table.select("tr");
										for (int i = 1; i < rows.size(); i++) 
										{
										    Element row = rows.get(i);
										    Elements col_date=row.select("td:eq(1)");
										    Elements col_name=row.select("td:eq(2)");
										    
										    Elements link=col_name.select("a[href]");
											 for(Element links:link)
											 {
												 list_name=links.absUrl("href");
											 }
											 
											 AwardBean awardBean=new AwardBean();
												String awardId="";
												String awardIdentifier="";
												String awardYear="";
												String awardRecipientName=col_name.text();
												String awardField="";
												String awardRecipientLocation="";
												String awardLink=list_name;
										    
												awardField=tableTitle.replaceAll("edit", "");
											 String[] splittedParts1 = col_date.text().split("-");
											 ArrayList<String> dateList = new ArrayList<String>();
											 String date1=null;
												 for(String s:splittedParts1)
												 {
													 date1=s;
													
													 if(date1.matches("[0-9]{4}"))
													 {
														    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy"); 
															SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
															newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
															System.out.println("olddateparser"+oldDateParser.parse(date1)); 
															awardYear=newDateParser.format(oldDateParser.parse(date1));
											     		    dateList.add(awardYear);
											     		    break;
											   
													 }
													 else if(date1.matches("[0-9]{2}"))
													 {
														    SimpleDateFormat oldDateParser = new SimpleDateFormat("yy"); 
														    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
															newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
															System.out.println("olddateparser"+oldDateParser.parse(date1)); 
															awardYear=newDateParser.format(oldDateParser.parse(date1));
											     		    dateList.add(awardYear);		 
											     		    
													 }
												 
												 }
												
												 System.out.println("awardYear"+dateList); 
													 System.out.println("awardYear"+awardYear); 
													  awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
																	awardDescription, awardField.trim(),awardRecipientName.trim(),
																	awardRecipientLocation,awardYear,
																	awardUrl, "VA-AWD", "VALUE"));
													
													if(flagTobreak==true)
											    		break;	
										}
										
										}
									}
									else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
									{
										isTableNotReached = false;
									}
									else
									{
										if(nextElement.nextElementSibling() != null)
										{
											nextElement = nextElement.nextElementSibling();
										}
										
									}
									break;
								}
									
								
							}
					 }
				}
				
				else if (awardDocTitle.contains("National Bravery Award"))
				{	
					doc.getElementsByClass("reference").remove();
					awardTitle="National Bravery Award" ;
					 Elements h3Elements = doc.getElementsByTag("h3");
					 doc.getElementsByClass("mw-editsection-bracket").remove();
					 String finalDate=null;
					 
						for(Element h3Element : h3Elements)
						{
							Element nextElement = h3Element.nextElementSibling();
							if(nextElement != null) 
							{
								boolean isTableNotReached = true;
								while(isTableNotReached)
								{
									if(nextElement.nodeName().equalsIgnoreCase("table"))
									{
										String tableTitle = h3Element.text();
										Element table = nextElement;
										Elements rows = table.select("tr");
										for (int i = 1; i < rows.size(); i++)
										{
										    Element row = rows.get(i);
										    Elements col_date = row.select("td:eq(0)");
										    String str=col_date.text().replaceFirst(String.valueOf((char) 160),"").trim();
										   
										    AwardBean awardBean=new AwardBean();
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
										    if(str.matches("[0-9]{4}"))
											 {
											 SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");      
											 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											 awardYear=newDateParser.format(oldDateParser.parse(str));
											 }
											 else
												 awardYear=null;
										    Elements col_name = row.select("td:eq(1)");
										    Elements col_awarded_for = row.select("td:eq(2)");
										    Elements col_state = row.select("td:eq(4)");
										   
										   awardTitle="National Bravery Award "+tableTitle.replaceAll("edit", "");
										    
										    awardRecipientName=col_name.text();
										    awardField=col_awarded_for.text();
										    awardRecipientLocation=col_state.text();
												  awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
																awardDescription, awardField.trim(),awardRecipientName.trim(),
																awardRecipientLocation,awardYear,
																awardUrl, "VA-AWD", "VALUE"));
												
												
										}
										 isTableNotReached = false;
									}
									else if(nextElement.nodeName().equalsIgnoreCase("h3")) 
									{
										isTableNotReached = false;
									}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
							   }
									
						}
								
				}
				
				if(flagTobreak==true)
				break;
			}
		}
				else if(awardDocTitle.contains("National Child Award for Exceptional Achievement"))
				{
					awardTitle="National Child Award for Exceptional Achievement";
					System.out.println("title is "+awardTitle);
					doc.getElementsByClass("mw-editsection-bracket").remove();
					doc.getElementsByClass("reference").remove();
					 Elements h3Elements = doc.getElementsByTag("h3");
					 for(Element h3Element : h3Elements)
						{
							Element nextElement = h3Element.nextElementSibling();
							if(nextElement != null) 
							{
								boolean isTableNotReached = true;
								while(isTableNotReached)
								{
									if(nextElement.nodeName().equalsIgnoreCase("ol"))
									{
										String tableTitle = h3Element.text();
										Element ol = nextElement;
										Elements li=ol.select("li");
										
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
										
										for(int i=0;i<li.size();i++)
										{
											AwardBean awardBean=new AwardBean();

											String name=null;
											String place=null;
											String field=null;
											
										Element li_value=li.get(i);
										String[] splittedParts1 = li_value.text().split(",",2);	
						             
										  String date1=null;
			                              date1=tableTitle.replaceAll("edit", "");
			                             
			                              if(date1.matches("[0-9]{4}"))
			                              {

			  							    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
			  								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			  								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
			  								System.out.println("olddateparser"+oldDateParser.parse(date1)); 
			  								awardYear=newDateParser.format(oldDateParser.parse(date1)); 
			  								
			                              }
			                           
			                              if(!tableTitle.contains("2003") &&!tableTitle.contains("1995"))
			                              {
			                            	  name=splittedParts1[0];
			                            	  awardRecipientName=name.trim();
			                              }
									      for(String s:splittedParts1)
											 {
									    	 if(!s.equalsIgnoreCase(splittedParts1[0]) &&!tableTitle.contains("2003")
									    			 &&!tableTitle.contains("1995"))
									    	 {
									    	    place=s;
									    	    awardRecipientLocation=place.trim();
									    	    if(tableTitle.contains("2011"))
									    	    {
									    	    	String[] sp=place.split("\\),\\(|\\)|\\(");
											    	place=sp[0];
											    	awardRecipientLocation=place.trim();
											    	 for(String spi:sp)
											    	 {
											    		 if(!spi.equals(sp[0]))
									    				  {
									    				 field=spi.trim();
									    				 awardField=field.trim();
									    				  }
											    	 }
									    	    }
					
									    	 }
											}
									      
									      if(tableTitle.contains("1995"))
								    	    {
								    	    	String[] sp=li_value.text().split("\\),\\(|\\)|\\(");
								    	    	name=sp[0];
								    	    	awardRecipientName=name.trim();
								    	    	for(String spi:sp)
								    	    	{
								    	    		 if(!spi.equals(sp[0]))
								    				  {
								    	    			 place=spi;
								    	    			 awardRecipientLocation=place.trim();
								    				  }
								    	    	}
								    	    	
								    	    }
									      
									      if(tableTitle.contains("2003"))
									      {
									    	  String[] sp=li_value.text().split("from");
									    	  String[] sp2=li_value.text().split("of");
									    	  String[] sp3=li_value.text().split(",[A-Za-z0-9]{1}");
									    	  
									    	  name=sp[0];
									    	  awardRecipientName=name.trim();
									    	
									    	  for(String str2:sp2)
										    	{
										    		if(!str2.equals(sp2[0]))
										    		{
										    			 
										    			 String p=str2;
										    			 String [] sp4=p.split("\\),\\(|\\)|\\(");
										    			 place=sp4[0];
										    			 awardRecipientLocation=place.trim();
										    			 for(String sp4i:sp4)
										    			  {
										    				  if(!sp4i.equals(sp4[0]))
										    				  {
										    					  field=sp4i.trim();
										    					  awardField=field.trim();
										    				  }
										    			  }
										    			 
										    	   
										    		}
										    	} 
									    	  for(String str:sp)
									    	  {
									    		  if(!str.equals(sp[0]))
									    		  {
									    			
									    			  String p=str;
									    			  String [] sp4=p.split("\\),\\(|\\)|\\(");
									    			  place=sp4[0];
									    			  awardRecipientLocation=place.trim();
									    			  for(String sp4i:sp4)
									    			  {
									    				  if(!sp4i.equals(sp4[0]) && !sp4i.contains("."))
									    				  {
									    					  field=sp4i.trim();
									    					  awardField=field.trim();
									    				  }
									    			  }
									    			  
									    		  }
									    	  }
									    	  
									    	  for(String str3:sp3)
									    	  {
									    		  if(!str3.equals(sp3[0]))
									    		  {
									    			  String p=str3;
									    			  String [] sp4=p.split("\\),\\(|\\)|\\(");
									    			  place=sp4[0];
									    			  awardRecipientLocation=place.trim();
									    			  for(String sp4i:sp4)
									    			  {
									    				  if(!sp4i.equals(sp4[0]))
									    				  {
									    					  field=sp4i.trim();
									    					  awardField=field.trim();
									    				  }
									    			  }   
									    			
									    		  }
									    	  }
									    
									      }
									      System.out.println("awardYear"+awardYear); 
										  awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
														awardDescription, awardField.trim(),awardRecipientName.trim(),
														awardRecipientLocation,awardYear,
														awardUrl, "VA-AWD", "VALUE"));
												
												if(flagTobreak==true)
										    		break;		
									    }
										isTableNotReached = false;
									}
									else
									{
										if(nextElement.nextElementSibling() != null)
										{
											nextElement = nextElement.nextElementSibling();
										}
										break;
									}
									
								}
							}
						}
				}
				
				//##########
				
				else if (awardDocTitle.contains("National Bal Shree Honour"))
				{
					awardTitle="National Bal Shree Honour";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByClass("mw-editsection").remove();
					Elements title=doc.getElementsByTag("title");
					Elements tables = doc.select("table[class=wikitable sortable]");
					for(int i=0;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements row=table.select("tr");
						Elements heading=table.select("caption");
						String finalDate=null;
						for(int k=0;k<heading.size();k++)
						{
							Element tableTitle=heading.get(k);
							String[] sp=tableTitle.text().split("National Balshree Honour");
							String[] sp2=sp[0].split("Year");
							for(String spi:sp2)
							{
								if(!spi.equals(sp2[0]))
								{
				     		      SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							      SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							      newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							      System.out.println("olddateparser"+oldDateParser.parse(spi)); 
							      finalDate=newDateParser.format(oldDateParser.parse(spi)); 
								}
							}	
							
						}
						for(int j=1;j<row.size();j++)
						{
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Element r=row.get(j);
							Elements colName=r.select("td:eq(0)");
							Elements colField=r.select("td:eq(1)");
							Elements colStateOfOrigin=r.select("td:eq(4)");
							
							awardRecipientName=colName.text();
							awardField=colField.text();
							awardRecipientLocation=colStateOfOrigin.text();
							awardYear=finalDate;
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;	
							
						}
						
					}
					Elements h3Elements=doc.getElementsByTag("h3");
					String finalDate1=null;
					for(Element h3Element : h3Elements)
					{
						Element nextElement = h3Element.nextElementSibling();
						if(nextElement != null) 
						{
							boolean isTableNotReached = true;
							while(isTableNotReached)
							{
								if(nextElement.nodeName().equalsIgnoreCase("table"))
								{
									String year = h3Element.text();
									String[] sp=year.split("Year");
									
									String awardId="";
									String awardIdentifier="";
									String awardYear="";
									String awardRecipientName="";
									String awardField="";
									String awardRecipientLocation="";
									String awardLink="";
									
									for(String spi:sp)
									{
										if(!spi.equalsIgnoreCase(sp[0]))
										{
							     		   SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
										   SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										   newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
										   System.out.println("olddateparser"+oldDateParser.parse(spi)); 
										   finalDate1=newDateParser.format(oldDateParser.parse(spi)); 
										}
									}
									
									Element table = nextElement;
									Elements rows = table.select("tr");
									for (int i = 1; i < rows.size(); i++)
									{
										 AwardBean awardBean=new AwardBean();
										 
										 Element row=rows.get(i);
										 if(!finalDate1.contains("2007"))
										 {
											 
										 Elements colName=row.select("td:eq(0)");
										 Elements colField=row.select("td:eq(1)");
										 Elements colStateOfOrigin=row.select("td:eq(2)");
										 
										 awardRecipientName=colName.text();
										 awardField=colField.text();
										 awardRecipientLocation=colStateOfOrigin.text();
										 awardYear=finalDate1;
				
												awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
														awardDescription, awardField.trim(),awardRecipientName.trim(),
														awardRecipientLocation,awardYear,
														awardUrl, "VA-AWD", "VALUE"));
												
												if(flagTobreak==true)
										    		break;
										 
										 }
										 
										 if(!finalDate1.contains("2006"))
										 {
											
											 Elements colName=row.select("td:eq(0)");
											 Elements colField=row.select("td:eq(2)");
											 Elements colStateOfOrigin=row.select("td:eq(4)");
											 
											 awardRecipientName=colName.text();
											 awardField=colField.text();
											 awardRecipientLocation=colStateOfOrigin.text();
											 awardYear=finalDate1;
											 
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
															awardDescription, awardField.trim(),awardRecipientName.trim(),
															awardRecipientLocation,awardYear,
															awardUrl, "VA-AWD", "VALUE"));
													
													if(flagTobreak==true)
											    		break;
										 }
									    
									}
									isTableNotReached = false;
								}
								else if(nextElement.nodeName().equalsIgnoreCase("h3")) 
								{
									isTableNotReached = false;
								}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
								}
							}
						}
						
					}
					Elements paragraph=doc.getElementsByTag("p");
					String finalDate2=null;
					for(Element para : paragraph)
					{
						Element nextElement = para.nextElementSibling();
						if(nextElement != null) 
						{
							boolean isTableNotReached = true;
							while(isTableNotReached)
							{
								if(nextElement.nodeName().equalsIgnoreCase("table"))
								{
									Element year=para.previousElementSibling();
									String[] sp3=year.text().split("Year");
									
								
									String awardId="";
									String awardIdentifier="";
									String awardYear="";
									String awardRecipientName="";
									String awardField="";
									String awardRecipientLocation="";
									String awardLink="";
									
									for(String sp3i:sp3)
									{
										if(!sp3i.equals(sp3[0]))
										{ 		    
							     		    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											System.out.println("olddateparser"+oldDateParser.parse(sp3i)); 
											finalDate2=newDateParser.format(oldDateParser.parse(sp3i)); 
										}
									}
									
									
									
									Element table = nextElement;
									Elements rows = table.select("tr");
									for (int i = 1; i < rows.size(); i++)
									{
									  AwardBean awardBean=new AwardBean();
										
									   Element row=rows.get(i);
									   Elements colName=row.select("td:eq(0)");
									   Elements colField=row.select("td:eq(1)");
									   Elements colStateOfOrigin=row.select("td:eq(3)");
									   awardRecipientName=colName.text();
									   awardField=colField.text();
									   awardRecipientLocation=colStateOfOrigin.text();
									   awardYear=finalDate2;
									  
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
													awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
											
											if(flagTobreak==true)
									    		break;		
									    
									}
									isTableNotReached = false;
								}
								else if(nextElement.nodeName().equalsIgnoreCase("h3")) 
								{
									isTableNotReached = false;
								}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
								}
							}
						}
					}
				}
				
				else if(awardDocTitle.contains("List of Lalit Kala Akademi fellows"))
				{
					awardTitle="List of Lalit Kala Akademi fellows";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					Elements h2Elements=doc.getElementsByTag("h2");
					for(Element h2Element : h2Elements)
					{
						Element nextElement = h2Element.nextElementSibling();
						if(nextElement != null) 
						{
							boolean isTagNotReached = true;
							while(isTagNotReached)
							{
								if(nextElement.nodeName().equalsIgnoreCase("p"))
								{
									Element list=nextElement.nextElementSibling();
									Elements li=list.select("li");
									for(int i=0;i<li.size();i++)
									{
										 AwardBean awardBean=new AwardBean();
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
										
										Element liList=li.get(i);
										String listname=null;
										String date1=null;
										String finalDate=null;
										String[] sp=liList.text().split("[0-9]{4}");
										Elements links=liList.select("a[href]");
										
										for(Element link:links)
										{
											listname=link.absUrl("href");
										}
										
										String[] sp2=liList.text().split("[A-Za-z.]{0,10} [A-Za-z.]{0,10}");
											for(String spi:sp2)
											{
												if(!spi.equals(sp2[0]) && !spi.contains(".") && !spi.contains("(")
											    && !spi.contains(")"))
												{
													date1=spi.trim();
												}
											}
											String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
											SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											finalDate=newDateParser.format(oldDateParser.parse(str)); 
				                            
											awardYear=finalDate;
											awardRecipientName=sp[0];
											awardLink=listname;
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
													awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
											
											if(flagTobreak==true)
									    		break;		
											
											
											
										
									}
								    isTagNotReached = false;
								}
								else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
								{
									isTagNotReached = false;
								}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
								}
							}
						}
					
						
					}
				}
				
				else if(awardDocTitle.contains("Dadasaheb Phalke Award"))
				{
					awardTitle="Dadasaheb Phalke Award";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("small").remove();
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(i);
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("th");
						Elements colField=row.select("td:eq(3)");
						Elements links=colDate.select("a[href]");
						String yearCeremony=null;
						String finalDate=null;
						String namelink=null;
						for(Element link:links)
						{
							yearCeremony=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 
					    
						Elements linksname=colName.select("a[href]");
						for(Element link:linksname)
						{
							namelink=link.absUrl("href");
						}
						
						awardYear=finalDate;
						awardRecipientName=colName.text().replace("•", "").trim();
						awardLink=namelink;
						awardField=colField.text().replace("•", "").trim();
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
						
						if(flagTobreak==true)
				    		break;	
						
						
					}
				}
				
				else if(awardDocTitle.contains("Padmabhushan Dr. Moturi Satyanarayan Award"))
				{
					awardTitle="Padmabhushan Dr. Moturi Satyanarayan Award";
					System.out.println("title is "+awardTitle);
					Elements table=doc.getElementsByClass("wikitable");
					doc.getElementsByClass("reference").remove();
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
						String finalDate=null;
						Element row=rows.get(i);
						Elements colName=row.select("td:eq(1)");
						Elements colDate=row.select("td:eq(0)");
						Elements colLocation=row.select("td:eq(2)");
				    	String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
				    	
				    	SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 
						
						awardYear=finalDate;
						awardRecipientName=colName.text();
						awardRecipientLocation=colLocation.text();
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;		
					}
				}
				
				else if(awardDocTitle.contains("Subramanyam Bharti Award"))
				{
					awardTitle="Subramanyam Bharti Award";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("br").prepend("/n");
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String name=null;
						
						
						
						Element row=rows.get(i);
						String finalDate=null;
						Elements colDate=row.select("td:eq(0)");
					    Elements colName=row.select("td:eq(1)");
						
					    String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 
					  
						
						if(colName.text().contains("/n"))
					    {
					    	String [] sp=colName.text().split("/n");
					    	
					    	for(String spi:sp)
					    	{
					    		AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
					    		
					    	awardRecipientName=spi.trim();
					        awardYear=finalDate;
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;		
					    		
					    	}
					    }
					}
				}
				
				else if (awardDocTitle.contains("Mahapandit Rahul Sankrityayan Award"))
				{	
					awardTitle="Mahapandit Rahul Sankrityayan Award";
					System.out.println("title is "+awardTitle); 
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					String finalDate=null;
					
					for(int i=1;i<rows.size();i++)
					{
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
						
						String nameLinks=null;
						Element row=rows.get(i);
						Elements colDate=row.select("td:eq(0)");
				    	Elements colName=row.select("td:eq(1)");
				    	
				    	Elements links=colName.select("a[href]");
				    	for(Element link:links)
				    	{
				    		nameLinks=link.absUrl("href");
				    	}
				    	
				    	
				    	String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
				    	
				    	SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 
				    	
				    	awardYear=finalDate;
				    	awardRecipientName=colName.text();
				    	awardLink=nameLinks;
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;		
				    	
					}
				}
				
				else if(awardDocTitle.contains("Dr. George Grierson Award"))
				{
					awardTitle="Dr. George Grierson Award";
					System.out.println("title is "+awardTitle); 
					
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					String finalDate=null;
					for(int i=1;i<rows.size();i++)
					{
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
						
						Element row=rows.get(i);
						Elements colDate=row.select("td:eq(0)");
				    	Elements colName=row.select("td:eq(1)");
				    	Elements colLocation=row.select("td:eq(3)");
				    	Elements links=colName.select("a[href]");
				    	String namelist=null;
				    	for(Element link:links)
				    	{
				    		namelist=link.absUrl("href");
				    	}
				    	
				    	String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
				    	
				    	SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 

						awardYear=finalDate;
						awardRecipientName=colName.text();
						awardRecipientLocation=colLocation.text();
						
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
						
							if(flagTobreak==true)
					    		break;	
						}
				}
				
				else if(awardDocTitle.contains("Kalinga Prize"))
				{
					awardTitle="Kalinga Prize";
					System.out.println("title is "+awardTitle);
					doc.getElementsByTag("dl").prepend("/n");
					 Elements li=doc.getElementsByClass("plainlist");
					 Elements lList=li.select("li");
					 
					 for(int i=0;i<lList.size();i++)
					 {
						 Element lListContent=lList.get(i);
						
						 String name=null;
						 String city=null;
						 String name1=null;
						 String list=null;
						 
						 String finalName=null;
						 String finalDate=null;
						 String finalPlace=null;
						 
						
						 
						 if(!lListContent.text().contains("no award") &&!lListContent.text().contains("No award") )
						 {
							 Elements links=lListContent.select("a[href]");
								for(Element link:links)
								{
									list=link.absUrl("href");
									System.out.println(list);
									
								}
							 
							 
							 String[] sp1=lListContent.text().split("[A-Za-z]{0,10} [A-Za-z]{0,10}");
							   
							 String[] sp=lListContent.text().split("/n");
							 for(String spi:sp)
							 {
								String str=sp1[0].replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								
								finalDate=newDateParser.format(oldDateParser.parse(str)); 
								 name=spi.trim();
								 String[] sp2=name.split("–");
								
							
								name1=sp2[0];
								if(!name1.startsWith(sp1[0]))
								{
									finalName=name1;
									
								}
								if(name1.startsWith(sp1[0]))
								{
								 String[] sp3=name1.split("[0-9]{4}");
								 for(String sp3i:sp3)
								 {
									 if(!sp3i.equals(sp3[0]))
									 {
										 finalName=sp3i.trim();
										
									 }
								 }
								}
								 for(String sp2i:sp2)
								 {
									 
									 if(!sp2i.equals(sp2[0]))
									 {
										 finalPlace=sp2i.trim();
									 }
									
								 }
								 AwardBean awardBean=new AwardBean();
									String awardId="";
									String awardIdentifier="";
									String awardYear=finalDate;
									String awardRecipientName=finalName;
									String awardField="";
									String awardRecipientLocation=finalPlace.replace(";", "");
									String awardLink="";
								 
										awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
										
										if(flagTobreak==true)
								    		break;		
								
							 } 
							
						}
						
						
					 }
				}
				
				else if(awardDocTitle.contains("Tagore Award"))
				{
					awardTitle="Tagore Award";
					System.out.println("title is "+awardTitle);
					doc.getElementsByClass("reference").remove();
					Elements table=doc.getElementsByClass("wikitable");
					
					Elements rows=table.select("tr");
					
					for(int i=2;i<rows.size();i++)
					{
						String finalDate=null;
						String nameList=null;
						
						Element row=rows.get(i);
						
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("td:eq(1)");
						Elements colCity=row.select("td:eq(4)");
						Elements links=colName.select("a[href]");
						
						for(Element link:links)
						{
							nameList=link.absUrl("href");
						}
						
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear=finalDate;
						String awardRecipientName=colName.text();
						String awardField="";
						String awardRecipientLocation=colCity.text();
						String awardLink=nameList;
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;	
					
					}
				}
				
				else if(awardDocTitle.contains("List of Shanti Swarup Bhatnagar Prize recipients"))
				{
					awardTitle="List of Shanti Swarup Bhatnagar Prize recipients";
					System.out.println("title is "+awardTitle);
					Elements tables=doc.getElementsByClass("wikitable");
					 Elements table=tables.select("table");
					 for(int i=1;i<table.size();i++)
					 {
						 Element t=table.get(i);
						 Elements rows=t.select("tr");
						 
						 for(int j=1;j<rows.size();j++)
						 {
							 String finalDate=null;
							 String nameList=null;
							 String fieldList=null;
							 
							 Element row=rows.get(j);
							 
							 Elements colDate=row.select("td:eq(0)");
							 Elements colName=row.select("td:eq(1)");
							 Elements colLocation=row.select("td:eq(2)");
							 Elements colField=row.select("td:eq(3)");
							
							 Elements links=colName.select("a[href]");
							 
							 for(Element link:links)
							 {
								 nameList=link.absUrl("href");
								 
							 }
							
							 Elements linksField=colField.select("a[href]");
							 
							 for(Element link:linksField)
							 {
								 fieldList=link.absUrl("href");
							 }
							 
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
							 
							AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear=finalDate;
								String awardRecipientName=colName.text();
								String awardField=colField.text();
								String awardRecipientLocation=colLocation.text();
								String awardLink=nameList;
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;		
						
						}
						 
					 }
				}
				
				else if(awardDocTitle.equals("National Film Award for Best Feature Film - Wikipedia"))
				{
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listLinks=null;
							
							 AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colFilmName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							Elements colDirector=row.select("td:eq(4)");
							
						    Elements links=colFilmName.select("a[href]");
						    
						    for(Element link:links)
						    {
						    	listLinks=link.absUrl("href");
						    
						    }
			
						    String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
							
							awardYear=finalDate;
							awardLink=listLinks;
							
							if(awardDocTitle.equals("National Film Award for Best Feature Film - Wikipedia"))
							{
								awardTitle="National Film Award for Best Feature Film";
								System.out.println("title is "+awardTitle);
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								awardField=colLanguage.text().replace("•", "").replace(" ", "").trim()+" "+colDirector.text().replace("•", "").replace(" ", "").trim()+" "+colProducer.text().replace("•", "").replace(" ", "").trim();
							}
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
						}
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Direction"))
				{
					awardTitle="National Film Award for Best Direction";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
					    for(int j=2;j<rows.size();j++)
					    {
					    	String listNames=null;
					    	String finalDate=null;
					    	
					    	 AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
					    	
					    	Element row=rows.get(j);
					    	Elements colDate=row.select("td:eq(0)");
					    	Elements colName=row.select("th");
					    	Elements colFilmName=row.select("td:eq(3)");
					    	Elements colFilmLanguage=row.select("td:eq(4)");
					    	
					    	Elements links=colName.select("a[href]");
					    	
					    	for(Element link:links)
					    	{
					    		listNames=link.absUrl("href");
					    	}
					    
					    	String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							if(str.matches("[0-9]{4}"))
							{
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
							}
							
							awardYear=finalDate;
							awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colFilmLanguage.text().replace("•", "").replace(" ", "").trim();
							awardLink=listNames;
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;		
							
							
							
					    }
					}
				}
				
				else if (awardDocTitle.contains("National Film Award for Best Popular Film Providing Wholesome Entertainment")||
						 awardDocTitle.equals("National Film Award for Best Non-Feature Film - Wikipedia"))
				{
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					
					for(int i=1;i<tables.size();i++)
					{
					   Element table=tables.get(i);
					   
					   Elements rows=table.select("tr");
					   for(int j=2;j<rows.size();j++)
					   {
						  String finalDate=null;
						  String listNames=null;
						  
						  AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
						  
						  Element row=rows.get(j);
						  Elements colDate=row.select("td:eq(0)");
						  Elements colFilmName=row.select("th");
						  Elements colLanguage=row.select("td:eq(2)");
						  Elements colProducer=row.select("td:eq(3)");
						  Elements colDirector=row.select("td:eq(4)");
						 
						  String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						  SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						  SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						  newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						  finalDate=newDateParser.format(oldDateParser.parse(str)); 
						  
						  
						  Elements links=colFilmName.select("a[href]");
					    	
					    	for(Element link:links)
					    	{
					    		listNames=link.absUrl("href");
					    	}
						 
					    	awardYear=finalDate;
					    	awardField=colDirector.text().replace("•", "").replace(" ", "").trim()+" "+colProducer.text().replace("•", "").replace(" ", "").trim() +" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
					    	awardLink=listNames;
					    	
					    	if(awardDocTitle.contains("National Film Award for Best Popular Film Providing Wholesome Entertainment"))
							{
					    		awardTitle="National Film Award for Best Popular Film Providing Wholesome Entertainment";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
					    	
					    	if(awardDocTitle.equals("National Film Award for Best Non-Feature Film - Wikipedia"))
							{
					    		awardTitle="National Film Award for Best Non-Feature Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
					    	
					    	System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
						  
					   }
					}
				}
				
				else if (awardDocTitle.contains("National Film Award for Best Children's Film"))
				{	
					awardTitle="National Film Award for Best Children's Film";
					doc.getElementsByClass("reference").remove();
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listFilm=null;
						
							 AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							Elements colDirector=row.select("td:eq(4)");
							
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
							
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								
								listFilm=link.absUrl("href");
							}
						    
							awardYear=finalDate;
							awardRecipientName=colName.text().trim();
							awardField=colLanguage.text().replace("•", "").replace(" ", "").trim() +" "+colProducer.text().replace("•", "").replace(" ", "").trim()+" "+ colDirector.text().replace("•", "").replace(" ", "").trim();
							awardLink=listFilm;
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;	
							
						}
					}
					
				}
				
				else if(awardDocTitle.contains("Indira Gandhi Award for Best First Film of a Director"))
				{
					awardTitle="Indira Gandhi Award for Best First Film of a Director";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listDirector=null;
							
							 AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th");
							Elements colDirector=row.select("td:eq(4)");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
							
							Elements links=colDirector.select("a[href]");
							for(Element link:links)
							{
								
								listDirector=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardRecipientName=colDirector.text().replace("•", "").replace(" ", "").trim();
							awardField=colName.text().replace("•", "").replace(" ", "").trim()
							+" "+colLanguage.text().replace("•", "").replace(" ", "").trim()+" "+colProducer.text().replace("•", "").replace(" ", "").trim();
							awardLink=listDirector;
							
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;	
						}
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Animated Film"))
				{
					awardTitle="National Film Award for Best Animated Film";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=0;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listFilm=null;
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colFilmName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							Elements colDirector=row.select("td:eq(4)");
							Elements colAnimator=row.select("td:eq(5)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
							
							Elements links=colFilmName.select("a[href]");
							for(Element link:links)
							{
								
								listFilm=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							awardField=colLanguage.text().replace("•", "").replace(" ", "").trim()+
							" "+colProducer.text().replace("•", "").replace(" ", "").trim()+" "
							+colDirector.text().replace("•", "").replace(" ", "").trim()+" "+colAnimator.text().replace("•", "").replace(" ", "").trim();
							awardLink=listFilm;
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;	
							
						}
						
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Actor") 
						|| awardDocTitle.contains("National Film Award for Best Actress") ||
						awardDocTitle.contains("National Film Award for Best Supporting Actor")
						|| awardDocTitle.contains("National Film Award for Best Supporting Actress"))
				{
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=1;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th");
							Elements colRoleName=row.select("td:eq(2)");
							Elements colFilmName=row.select("td:eq(3)");
							Elements colLanguage=row.select("td:eq(4)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
					
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+ 
									" "+colLanguage.text().replace("•", "").replace(" ", "").trim()+" "+colRoleName.text().replace("•", "").replace(" ", "").trim();
							awardLink=listName;
							
							if(awardDocTitle.contains("National Film Award for Best Actor"))
							{
								awardTitle="National Film Award for Best Actor";
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Actress"))
							{
								awardTitle="National Film Award for Best Actress";
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Supporting Actor"))
							{
								awardTitle="National Film Award for Best Supporting Actor";
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Supporting Actress"))
							{
								awardTitle="National Film Award for Best Supporting Actress";
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;	
							
						}
					}
				}
				
				else if (awardDocTitle.contains("National Film Award for Best Child Artist")||
						awardDocTitle.contains("National Film Award for Best Screenplay"))
				{	
					
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th");
							Elements colFilmName=row.select("td:eq(2)");
							Elements colLanguage=row.select("td:eq(3)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
					
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
							awardLink=listName;
							
							if(awardDocTitle.contains("National Film Award for Best Child Artist"))
							{
								awardTitle="National Film Award for Best Child Artist";
								System.out.println("title is "+awardTitle);
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Screenplay"))
							{
								awardTitle="National Film Award for Best Screenplay";
								System.out.println("title is "+awardTitle);
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;	
							
							/*System.out.println(finalDate);
							System.out.println("Name of the Child Artist Awarded National Film Award for Best Child Artist-: "+colName.text());
							System.out.println("Name of the film-: "+colFilmName.text()+" Language of the film-: "+colLanguage.text());
						    System.out.println(listName);*/
						    
						    
						}
					}
				}
				
				else if (awardDocTitle.contains("National Film Award for Best Music Direction"))
				{	
					awardTitle="National Film Award for Best Music Direction";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=0;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th");
							Elements colFilmName=row.select("td:eq(3)");
							Elements colLanguage=row.select("td:eq(4)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str)); 
					
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							
							awardYear=finalDate;
							awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
							awardLink=listName;
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
							/*System.out.println(finalDate);
							System.out.println("Name of the Music Director Awarded National Film Award for Best Music Direction-: "+colName.text());
							System.out.println("Name of the film-: "+colFilmName.text()+" Language of the song-: "+colLanguage.text());
							System.out.println(listName);*/
						}
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Male Playback Singer")
					 || awardDocTitle.contains("National Film Award for Best Female Playback Singer") )
				{
					 doc.getElementsByTag("small").remove();
					 doc.getElementsByClass("reference").remove();
						Elements tables=doc.getElementsByClass("wikitable");
						for(int i=0;i<tables.size();i++)
						{
							Element table=tables.get(i);
							Elements rows=table.select("tr");
							for(int j=2;j<rows.size();j++)
							{
								String finalDate=null;
								String listName=null;
								
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								Element row=rows.get(j);
								Elements colDate=row.select("td:eq(0)");
								Elements colName=row.select("th");
								Elements colSongName=row.select("td:eq(3)");
								Elements colFilmName=row.select("td:eq(4)");
								Elements colLanguage=row.select("td:eq(5)");
								
								String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								finalDate=newDateParser.format(oldDateParser.parse(str)); 
						
								Elements links=colName.select("a[href]");
								for(Element link:links)
								{
									
									listName=link.absUrl("href");
								}
								
								awardYear=finalDate;
								awardField=colSongName.text().replace("•", "").replace(" ", "").trim() +
										" "+colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace(" ", "").replace("•", "").trim();
								awardLink=listName;
								
								if(awardDocTitle.contains("National Film Award for Best Male Playback Singer"))
								{
									awardTitle="National Film Award for Best Male Playback Singer";
									System.out.println("title is "+awardTitle); 
									awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Female Playback Singer"))
								{
									awardTitle="National Film Award for Best Female Playback Singer";
									System.out.println("title is "+awardTitle); 
									awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								}
								
								System.out.println("awardYear"+awardYear); 
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
							
							}
						}
					
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Lyrics")||
					awardDocTitle.contains("National Film Award for Best Choreography"))
				{
					
					  doc.getElementsByTag("small").remove();
					  doc.getElementsByClass("reference").remove();
					    Elements tables=doc.getElementsByClass("wikitable");
						for(int i=0;i<tables.size();i++)
						{
							Element table=tables.get(i);
							Elements rows=table.select("tr");
							for(int j=2;j<rows.size();j++)
							{
								String finalDate=null;
								String listName=null;
								
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								Element row=rows.get(j);
								Elements colDate=row.select("td:eq(0)");
								Elements colName=row.select("th");
								Elements colSongName=row.select("td:eq(2)");
								Elements colFilmName=row.select("td:eq(3)");
								Elements colLanguage=row.select("td:eq(4)");
								
								String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								finalDate=newDateParser.format(oldDateParser.parse(str)); 
						
								Elements links=colName.select("a[href]");
								for(Element link:links)
								{
									
									listName=link.absUrl("href");
								}
								
								awardYear=finalDate;
								awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colSongName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
								awardLink=listName;
								
								if(awardDocTitle.contains("National Film Award for Best Lyrics"))
								{
									awardTitle="National Film Award for Best Lyrics";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Choreography"))
								{
									awardTitle="National Film Award for Best Choreography";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								}
								
								System.out.println("awardYear"+awardYear); 
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
							}
						}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Cinematography"))
				{
					awardTitle="National Film Award for Best Cinematography";
					System.out.println("title is "+awardTitle); 
					 doc.getElementsByTag("small").remove();
					 doc.getElementsByClass("reference").remove();
					    Elements tables=doc.getElementsByClass("wikitable");
						for(int i=0;i<tables.size();i++)
						{
							Element table=tables.get(i);
							Elements rows=table.select("tr");
							for(int j=2;j<rows.size();j++)
							{
								String finalDate=null;
								String listName=null;
								
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								Element row=rows.get(j);
								Elements colDate=row.select("td:eq(0)");
								Elements colName=row.select("th:eq(1)");
								Elements colLaboratory=row.select("th:eq(2)");
								Elements colFilmName=row.select("td:eq(3)");
								Elements colLanguage=row.select("td:eq(4)");
								
								String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								finalDate=newDateParser.format(oldDateParser.parse(str)); 
						
								Elements links=colName.select("a[href]");
								for(Element link:links)
								{
									
									listName=link.absUrl("href");
								}
								
								awardYear=finalDate;
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								awardField=colFilmName.text().replace("•", "").trim()+" "+colLanguage.text().replace("•", "").trim()
								+" "+colLaboratory.text().replace("•", "").replace(" ", "").trim();
								awardLink=listName;
								
								 System.out.println("awardYear"+awardYear); 
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
							}
						}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Costume Design")||
						awardDocTitle.contains("National Film Award for Best Editing")
					 || awardDocTitle.contains("National Film Award for Best Special Effects"))
				{
					
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("reference").remove();
					 Elements tables=doc.getElementsByClass("wikitable");
						for(int i=0;i<tables.size();i++)
						{
							Element table=tables.get(i);
							Elements rows=table.select("tr");
							for(int j=2;j<rows.size();j++)
							{
								String finalDate=null;
								String listName=null;
								
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								Element row=rows.get(j);
								Elements colDate=row.select("td:eq(0)");
								Elements colName=row.select("th:eq(1)");
								Elements colFilmName=row.select("td:eq(2)");
								Elements colLanguage=row.select("td:eq(3)");
								
								
								String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								finalDate=newDateParser.format(oldDateParser.parse(str)); 
								
								Elements links=colName.select("a[href]");
								for(Element link:links)
								{
									
									listName=link.absUrl("href");
								}
								
								awardYear=finalDate;
								awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
								awardLink=listName;
								
								if(awardDocTitle.contains("National Film Award for Best Costume Design"))
								{
									awardTitle="National Film Award for Best Costume Design";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Editing"))
								{
									awardTitle="National Film Award for Best Editing";
									System.out.println("title is "+awardTitle); 
									awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if( awardDocTitle.contains("National Film Award for Best Special Effects"))
								{
									awardTitle="National Film Award for Best Special Effects";
									System.out.println("title is "+awardTitle); 
									awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
								}
								
								 System.out.println("awardYear"+awardYear); 
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
							}
						}
				}
				
	            
				else if(awardDocTitle.equals("National Film Award – Special Jury Award / Special Mention (Feature Film) - Wikipedia"))
				{
					awardTitle="National Film Award – Special Jury Award / Special Mention (Feature Film)";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByTag("small").remove();
				    doc.getElementsByClass("reference").remove();
				    Elements tables=doc.getElementsByClass("wikitable");
				  		for(int i=0;i<tables.size();i++)
				  		{
				  			Element table=tables.get(i);
				  			Elements rows=table.select("tr");
				  			for(int j=2;j<rows.size();j++)
				  			{
				  				String finalDate=null;
				  				String listName=null;
				  				
				  				AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
				  				
				  				Element row=rows.get(j);
				  				Elements colDate=row.select("td:eq(0)");
				  				Elements colName=row.select("th");
				  				Elements colAwardedAs=row.select("td:eq(2)");
				  				Elements colFilmName=row.select("td:eq(3)");
				  				Elements colLanguage=row.select("td:eq(4)");
				  				
				  				String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								finalDate=newDateParser.format(oldDateParser.parse(str)); 
				  				
								Elements links=colName.select("a[href]");
								for(Element link:links)
								{
									
									listName=link.absUrl("href");
								}
								
								
				  				
				  				if(table.equals(tables.get(0)))
				  				{
				  					awardYear=finalDate;
									awardField=colAwardedAs.text().replace("•", "").replace(" ", "").trim()+" "+colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
									awardLink=listName;
				  				    awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
				  				}
				  				
				  				if(table.equals(tables.get(1)))
				  				{
				  					awardYear=finalDate;
									awardField=colAwardedAs.text().replace("•", "").replace(" ", "").trim()+" "+colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
									awardLink=listName;
				  					awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
				  				}	
				  				
				  				System.out.println("awardYear"+awardYear); 
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
				  			}
				  		}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Film on Environment Conservation/Preservation")||
						awardDocTitle.contains("National Film Award for Best Film on Family Welfare")||
						awardDocTitle.contains("Nargis Dutt Award for Best Feature Film on National Integration")
					||  awardDocTitle.contains("National Film Award for Best Film on Other Social Issues")
					|| awardDocTitle.contains("National Film Award for Second Best Feature Film")
					|| awardDocTitle.contains("National Film Award for Best First Non-Feature Film of a Director")
					|| awardDocTitle.contains("National Film Award for Best Agriculture Film")
					|| awardDocTitle.contains("National Film Award for Best Anthropological/Ethnographic Film")
					|| awardDocTitle.contains("National Film Award for Best Arts/Cultural Film"))
				{
					
					doc.getElementsByTag("small").remove();
				    doc.getElementsByClass("reference").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=0;i<tables.size();i++)
						{
							Element table=tables.get(i);
							Elements rows=table.select("tr");
							for(int j=2;j<rows.size();j++)
							{
								String finalDate=null;
								String listFilmName=null;
								
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								Element row=rows.get(j);
								
								Elements colDate=row.select("td:eq(0)");
				  				Elements colFilmName=row.select("th");
				  				Elements filmLanguage=row.select("td:eq(2)");
				  				Elements colProducer=row.select("td:eq(3)");
				  				Elements colDirector=row.select("td:eq(4)");
				  				
				  				String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								finalDate=newDateParser.format(oldDateParser.parse(str));
				  				
								Elements links=colFilmName.select("a[href]");
								for(Element link:links)
								{
									
									listFilmName=link.absUrl("href");
								}
								
								awardYear=finalDate;
								awardField=colDirector.text().replace("•", "").replace(" ", "").trim()+" "+colProducer.text().replace("•", "").replace(" ", "").trim()+" "+filmLanguage.text().replace("•", "").replace(" ", "").trim();
								awardLink=listFilmName;
							
								if(awardDocTitle.contains("National Film Award for Best Film on Environment Conservation/Preservation"))
								{
									awardTitle="National Film Award for Best Film on Environment Conservation/Preservation";
									System.out.println("title is "+awardTitle); 
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Film on Family Welfare"))
								{
									awardTitle="National Film Award for Best Film on Family Welfare";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("Nargis Dutt Award for Best Feature Film on National Integration"))
								{
									awardTitle="Nargis Dutt Award for Best Feature Film on National Integration";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Film on Other Social Issues"))
								{
									awardTitle="National Film Award for Best Film on Other Social Issues";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								if(awardDocTitle.contains("National Film Award for Second Best Feature Film"))
								{
									awardTitle="National Film Award for Second Best Feature Film";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								if(awardDocTitle.contains("National Film Award for Best First Non-Feature Film of a Director"))
								{
									awardTitle="National Film Award for Best First Non-Feature Film of a Director";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								if(awardDocTitle.contains("National Film Award for Best Agriculture Film"))
								{
									awardTitle="National Film Award for Best Agriculture Film";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								if(awardDocTitle.contains("National Film Award for Best Anthropological/Ethnographic Film"))
								{
									awardTitle="National Film Award for Best Anthropological/Ethnographic Film";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								if(awardDocTitle.contains("National Film Award for Best Arts/Cultural Film"))
								{
									awardTitle="National Film Award for Best Arts/Cultural Film";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								System.out.println("awardYear"+awardYear); 
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
								
							}
						
						}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Feature Film in Assamese")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Bengali")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Hindi")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Kannada")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Malayalam")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Marathi")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Odia")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Tamil")
					|| awardDocTitle.contains("National Film Award for Best Feature Film in Telugu"))
				{
					
					doc.getElementsByTag("small").remove();
				    doc.getElementsByClass("reference").remove();
				    Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
						{
							Element table=tables.get(i);
							Elements rows=table.select("tr");
							for(int j=2;j<rows.size();j++)
							{
								String finalDate=null;
								String listFilmName=null;
								
								Element row=rows.get(j);
								
								AwardBean awardBean=new AwardBean();
								String awardId="";
								String awardIdentifier="";
								String awardYear="";
								String awardRecipientName="";
								String awardField="";
								String awardRecipientLocation="";
								String awardLink="";
								
								Elements colDate=row.select("td:eq(0)");
								Elements colFilmName=row.select("th");
								Elements colProducer=row.select("td:eq(2)");
								Elements colDirector=row.select("td:eq(3)");
								
								String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
								SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								finalDate=newDateParser.format(oldDateParser.parse(str));
								
								Elements links=colFilmName.select("a[href]");
								for(Element link:links)
								{
									
									listFilmName=link.absUrl("href");
								}
								
								awardYear=finalDate;
								
								awardField=colProducer.text().replace("•", "").replace(" ", "").trim()+" "+colDirector.text().replace("•", "").replace(" ", "").trim();
								awardLink=listFilmName;
								
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Bengali"))
								{
									awardTitle="National Film Award for Best Feature Film in Bengali";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Assamese"))
								{
									awardTitle="National Film Award for Best Feature Film in Assamese";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Hindi"))
								{
									awardTitle="National Film Award for Best Feature Film in Hindi";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Kannada"))
								{
									awardTitle="National Film Award for Best Feature Film in Kannada";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
							
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Malayalam"))
								{
									awardTitle="National Film Award for Best Feature Film in Malayalam";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
							
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Marathi"))
								{
									awardTitle="National Film Award for Best Feature Film in Marathi";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Odia"))
								{
									awardTitle="National Film Award for Best Feature Film in Odia";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Tamil"))
								{
									awardTitle="National Film Award for Best Feature Film in Tamil";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								if(awardDocTitle.contains("National Film Award for Best Feature Film in Telugu"))
								{
									awardTitle="National Film Award for Best Feature Film in Telugu";
									System.out.println("title is "+awardTitle);
									awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								}
								
								System.out.println("awardYear"+awardYear); 
									awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
								
							}
						}
				}
				
				else if (awardDocTitle.contains("Discontinued and Intermittent National Film Awards"))
				{	
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection-bracket").remove();
					Elements title=doc.select("title");
					Elements h4Elements=doc.getElementsByTag("h4");
					for(Element h4Element : h4Elements)
					{
						Element nextElement = h4Element.nextElementSibling();
						if(nextElement != null) 
						{
							boolean isTableNotReached = true;
							while(isTableNotReached)
							{
								if(nextElement.nodeName().equalsIgnoreCase("p"))
								{
									String h4Title=h4Element.text();
									
									
									if(!h4Title.equals("Best Storyedit") &&
									   !h4Title.equals("Special Jury Award / Special Mention (Book on Cinema)edit")	)
									{
								
									Element tables=h4Element.nextElementSibling();
									Element table=tables.nextElementSibling();
									Elements rows=table.select("tr");
									for(int i=2;i<rows.size();i++)
									{
									    String finalDate=null;
									    String listFilmName=null;
									    String directorName=null;
									    
									    AwardBean awardBean=new AwardBean();
										String awardId="";
										String awardIdentifier="";
										String awardYear="";
										String awardRecipientName="";
										String awardField="";
										String awardRecipientLocation="";
										String awardLink="";
									    
									   Element row=rows.get(i);
									   awardTitle=h4Title.replaceAll("edit", "");
									   //System.out.println("TITLE IS-:"+h4Title.replaceAll("edit", ""));
									   Elements colDate=row.select("td:eq(0)");
									   Elements colFilmName=row.select("th");
									   Elements colLanguage=row.select("td:eq(2)");
									   Elements colProducer=row.select("td:eq(3)");
									   
									   String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
									   SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
									   SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									   newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
									   finalDate=newDateParser.format(oldDateParser.parse(str));
									   
									   Elements links=colFilmName.select("a[href]");
										for(Element link:links)
										{
											
											listFilmName=link.absUrl("href");
										}
									   
									   if(!h4Title.equals("Best Newsreel Cameramanedit"))
									   {
										   Elements colDirector=row.select("td:eq(4)");
										   directorName=colDirector.text().replace("•", "").replace(" ", "").trim();
										  
									   }
									   
									   awardYear=finalDate;
									   awardRecipientName=colFilmName.text();
									   awardField=colProducer.text().replace("•", "").replace(" ", "").trim()+" "+directorName+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
									   awardLink=listFilmName;
									   
									   System.out.println(awardRecipientName);
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
													awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
											
											if(flagTobreak==true)
									    		break;
									}
									
									}
									if(h4Title.equals("Best Storyedit") ||
									   h4Title.equals("Special Jury Award / Special Mention (Book on Cinema)edit"))
									{
									Element para=h4Element.nextElementSibling();
									Element tables=para.nextElementSibling();
									Element table=tables.nextElementSibling();
									Elements rows=table.select("tr");
									
									for(int j=2;j<rows.size();j++)
									{
									  String finalDate=null;
									  String listName=null;
									  String languageFilm=null;
									  String filmName=null;
									  
									  AwardBean awardBean=new AwardBean();
										String awardId="";
										String awardIdentifier="";
										String awardYear="";
										String awardRecipientName="";
										String awardField="";
										String awardRecipientLocation="";
										String awardLink="";
										
									  Element row=rows.get(j);
									  awardTitle=h4Title.replaceAll("edit", "");
									  Elements colDate=row.select("td:eq(0)");
									  Elements colName=row.select("th");
									  
									  String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
									  SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
									  SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
									  newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
									  finalDate=newDateParser.format(oldDateParser.parse(str));
									  
									  Elements links=colName.select("a[href]");
										for(Element link:links)
										{
											
											listName=link.absUrl("href");
										}
									  
									  if(h4Title.equals("Best Storyedit"))
									  {
										  Elements colFilmName=row.select("td:eq(2)");
										  Elements colLanguage=row.select("td:eq(3)");
										  languageFilm=colLanguage.text().replace("•", "").replace(" ", "").trim();
										  filmName=colFilmName.text().replace("•", "").replace(" ", "").trim();
										  
									  }
									  if(h4Title.equals("Special Jury Award / Special Mention (Book on Cinema)edit"))
									  {
										  Elements colLanguage=row.select("td:eq(2)");
										  languageFilm=colLanguage.text().replace("•", "").replace(" ", "").trim();
									  }
									 
									  awardYear=finalDate;
									  awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
									  awardField=filmName+
											  " "+languageFilm;
									  awardLink=listName;
									  
									  System.out.println("awardYear"+awardYear); 
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
													awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
											
											if(flagTobreak==true)
									    		break;
									 
									}
									}
									isTableNotReached = false;
								}
								else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
								{
									isTableNotReached = false;
								}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
								}
							}
						}
						
					}
					Elements h5Elements=doc.getElementsByTag("h5");
					for(Element h5Element : h5Elements)
					{
						Element nextElement = h5Element.nextElementSibling();
						if(nextElement != null) 
						{
							boolean isTableNotReached = true;
							while(isTableNotReached)
							{
								if(nextElement.nodeName().equalsIgnoreCase("p"))
								{
									String h4Title=h5Element.text();
									
									
								 
									
									Element tables=h5Element.nextElementSibling();
									Element table=tables.nextElementSibling();
									Elements rows=table.select("tr");
									for(int i=2;i<rows.size();i++)
									{
										AwardBean awardBean=new AwardBean();
										String awardId="";
										String awardIdentifier="";
										String awardYear="";
										String awardRecipientName="";
										String awardField="";
										String awardRecipientLocation="";
										String awardLink="";
										
										String finalDate=null;
										String listFilmName=null;
										
										
										Element row=rows.get(i);
										awardTitle=h4Title.replaceAll("edit", "");
										Elements colDate=row.select("td:eq(0)");
										Elements colFilmName=row.select("th");
										Elements colProducer=row.select("td:eq(2)");
										Elements colDirector=row.select("td:eq(3)");
										
										String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
										SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
										SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
										finalDate=newDateParser.format(oldDateParser.parse(str));
										
										Elements links=colFilmName.select("a[href]");
										for(Element link:links)
										{
											
											listFilmName=link.absUrl("href");
										}
										
										awardYear=finalDate;
										awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
										awardField=colProducer.text().replace("•", "").replace(" ", "").trim()+" "+colDirector.text().replace("•", "").replace(" ", "").trim();
										awardLink=listFilmName;
										
										System.out.println("awardYear"+awardYear); 
										awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
													awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
										
											if(flagTobreak==true)
									    		break;
									   
									}
								
									
									isTableNotReached = false;
								}
								else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
								{
									isTableNotReached = false;
								}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
								}
							}
						}
						
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Feature Film in English"))
				{
					awardTitle="National Film Award for Best Feature Film in English";
					doc.getElementsByClass("reference").remove();
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection-bracket").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=0;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listFilmName=null;
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Element row=rows.get(j);
							Elements colDate=row.select("td:eq(0)");
							Elements colFilmName=row.select("th");
							Elements colProducer=row.select("td:eq(2)");
							Elements colDirector=row.select("td:eq(3)");
							

							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							Elements links=colFilmName.select("a[href]");
							for(Element link:links)
							{
								
								listFilmName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							awardField=colProducer.text().replace("•", "").replace(" ", "").trim()+" "+colDirector.text().replace("•", "").replace(" ", "").trim();
							awardLink=listFilmName;
							
							System.out.println("awardYear"+awardYear);
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
							
						}
					}
					
				}
				
				else if (awardDocTitle.equals("National Film Award for Best Non-Feature Film Direction - Wikipedia")||
						awardDocTitle.equals("National Film Award for Best Non-Feature Film Audiography - Wikipedia")
						|| awardDocTitle.equals("National Film Award for Best Non-Feature Film Narration / Voice Over")
						|| awardDocTitle.equals("National Film Award for Best Non-Feature Film Music Direction - Wikipedia"))
				{	
					
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=0;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Element row=rows.get(j);
							
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th:eq(1)");
							Elements colFilmName=row.select("td:eq(2)");
							Elements colLanguage=row.select("td:eq(3)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							
							awardYear=finalDate;
							awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
							awardLink=listName;
							
							if(awardDocTitle.equals("National Film Award for Best Non-Feature Film Audiography - Wikipedia"))
							{
								awardTitle="National Film Award for Best Non-Feature Film Audiography";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							if(awardDocTitle.equals("National Film Award for Best Non-Feature Film Narration / Voice Over"))
							{
								awardTitle="National Film Award for Best Non-Feature Film Narration / Voice Over";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.equals("National Film Award for Best Non-Feature Film Direction - Wikipedia"))
							{
								awardTitle="National Film Award for Best Non-Feature Film Direction";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
								
							if(awardDocTitle.equals("National Film Award for Best Non-Feature Film Music Direction - Wikipedia"))
							{
								awardTitle="National Film Award for Best Non-Feature Film Music Direction";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							}
							
							System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
						}
					}
				}
				else if (awardDocTitle.contains("National Film Award for Best Non-Feature Film Cinematography"))
				{	
					awardTitle="National Film Award for Best Non-Feature Film Cinematography";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection-bracket").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(j);
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th");
							Elements colFilmName=row.select("td:eq(3)");
							Elements colLanguage=row.select("td:eq(4)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardRecipientName=colName.text().replace("•", "");
							awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
							awardLink=listName;
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
						}
					}
				}
				
				else if (awardDocTitle.contains("National Film Award for Best Book on Cinema"))
				{
					awardTitle="National Film Award for Best Book on Cinema";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection-bracket").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(j);
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Elements colDate=row.select("td:eq(0)");
							Elements colBookName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colAuthor=row.select("td:eq(3)");
							Elements colPublisher=row.select("td:eq(4)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							Elements links=colAuthor.select("a[href]");
							for(Element link:links)
							{
								listName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardRecipientName=colAuthor.text().replace("•", "").trim();
							awardField=colBookName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
							awardLink=listName;
							
							System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
						}
					}
				}
				else if(awardDocTitle.contains("National Film Award for Best Film Critic"))
				{
					awardTitle="National Film Award for Best Film Critic";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection-bracket").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(j);
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
							awardField=colLanguage.text().replace("•", "").replace(" ", "").trim();
							awardLink=listName;
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
						}
					}
				}
				
				else if (awardDocTitle.contains("National Film Award for Best Non-Feature Animation Film"))
				{	
					awardTitle="National Film Award for Best Non-Feature Animation Film";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=2;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(j);
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
						
							
							Elements colDate=row.select("td:eq(0)");
							Elements colFilmName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							Elements colDirector=row.select("td:eq(4)");
							Elements colAnimator=row.select("td:eq(5)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							awardYear=finalDate;
							awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							awardField=colProducer.text().replace("•", "").replace(" ", "").trim()+" "+colDirector.text().replace(" ", "").replace("•", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim()+" "+colAnimator.text().replace("•", "").replace(" ", "").trim();
							
							
							System.out.println("awardYear"+awardYear); 
							
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
						
						}
					}
					
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Biographical Film") ||
						awardDocTitle.contains("National Film Award for Best Educational/Motivational/Instructional Film")||
						awardDocTitle.contains("National Film Award for Best Non-Feature Environment/Conservation/Preservation Film")||
						awardDocTitle.contains("National Film Award for Best Historical Reconstruction/Compilation Film")||
						awardDocTitle.contains("National Film Award for Best Promotional Film")||
						awardDocTitle.contains("National Film Award for Best Scientific Film")||
						awardDocTitle.contains("National Film Award for Best Film on Social Issues"))
						
				{
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=1;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(j);
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
						
							
							Elements colDate=row.select("td:eq(0)");
							Elements colFilmName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							Elements colDirector=row.select("td:eq(4)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							Elements links=colFilmName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							awardField=colProducer.text().replace("•", "").replace(" ", "").trim()+" "+colDirector.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace(" ", "").replace("•", "").trim();
							
							if(awardDocTitle.contains("National Film Award for Best Biographical Film"))
							{
								awardTitle="National Film Award for Best Biographical Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Educational/Motivational/Instructional Film"))
							{
								awardTitle="National Film Award for Best Educational/Motivational/Instructional Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							if(awardDocTitle.contains("National Film Award for Best Non-Feature Environment/Conservation/Preservation Film"))
							{
								awardTitle="National Film Award for Best Non-Feature Environment/Conservation/Preservation Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Historical Reconstruction/Compilation Film"))
							{
								awardTitle="National Film Award for Best Historical Reconstruction/Compilation Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Promotional Film"))
							{
								awardTitle="National Film Award for Best Promotional Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							if(awardDocTitle.contains("National Film Award for Best Scientific Film"))
							{
								awardTitle="National Film Award for Best Scientific Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Film on Social Issues"))
							{
								awardTitle="National Film Award for Best Film on Social Issues";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
						}
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Exploration/Adventure Film")||
						awardDocTitle.contains("National Film Award for Best Investigative Film"))		
				{
					
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=0;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(j);
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
						
							
							Elements colDate=row.select("td:eq(0)");
							Elements colFilmName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							Elements colDirector=row.select("td:eq(4)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							Elements links=colFilmName.select("a[href]");
							for(Element link:links)
							{
								
								listName=link.absUrl("href");
							}
							
							awardYear=finalDate;
							
							awardField=colProducer.text().replace("•", "").replace(" ", "").trim()+" "+colDirector.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
							
							if(awardDocTitle.contains("National Film Award for Best Exploration/Adventure Film"))
							{
								awardTitle="National Film Award for Best Exploration/Adventure Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							
							if(awardDocTitle.contains("National Film Award for Best Investigative Film"))
							{
								awardTitle="National Film Award for Best Investigative Film";
								System.out.println("title is "+awardTitle); 
								awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
							}
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
							
						}
					}
				}
				
				else if (awardDocTitle.contains("National Film Award for Best Non-Feature Film on Family Welfare")||
						awardDocTitle.contains("National Film Award for Best Short Fiction Film"))
				{	
					
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					Elements tables=doc.getElementsByClass("wikitable");
					for(int i=2;i<tables.size();i++)
					{
						Element table=tables.get(i);
						Elements rows=table.select("tr");
						for(int j=2;j<rows.size();j++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(j);
							
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
						
							
							Elements colDate=row.select("td:eq(0)");
							Elements colFilmName=row.select("th");
							Elements colLanguage=row.select("td:eq(2)");
							Elements colProducer=row.select("td:eq(3)");
							Elements colDirector=row.select("td:eq(4)");
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							awardYear=finalDate;
							
							awardField=colProducer.text().replace("•", "").trim()+" "+colDirector.text().replace("•", "").trim()+"  "+colLanguage.text().replace("•", "").trim();

							if(awardDocTitle.contains("National Film Award for Best Non-Feature Film on Family Welfare"))
							{
							   awardTitle="National Film Award for Best Non-Feature Film on Family Welfare";
							   System.out.println("title is "+awardTitle); 
							   awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();
								
							}
							
							if(awardDocTitle.contains("National Film Award for Best Short Fiction Film"))
							{
							   awardTitle="National Film Award for Best Short Fiction Film";
							   System.out.println("title is "+awardTitle); 
							   awardRecipientName=colFilmName.text().replace("•", "").replace(" ", "").trim();	
							}
							
							System.out.println("awardYear"+awardYear); 
								awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
						
						}
					}
					
				}
				
				else if(awardDocTitle.contains("Dhanji Kanji Gandhi Suvarna Chandrak"))
				{
					awardTitle="Dhanji Kanji Gandhi Suvarna Chandrak";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					Elements table=doc.getElementsByClass("wikitable");
			        Elements rows=table.select("tr");
			        for(int i=1;i<rows.size();i++)
			        {
			        	AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
			        	
			        	String finalDate=null;
			        	String list=null;
			        	
			        	Element row=rows.get(i);
			        	Elements colDate=row.select("td:eq(0)");
			        	Elements colName=row.select("td:eq(1)");
			        	
			        	String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
			            
						Elements links=colDate.select("a[href]");
						for(Element link:links)
						{
							list=link.absUrl("href");
						}
						
						awardYear=finalDate;
						awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
						awardLink=list;
						
						System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
			        }
				}
				
				else if(awardDocTitle.contains("Bankim Puraskar"))
				{
					awardTitle="Bankim Puraskar";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("mw-editsection").remove();
			        doc.getElementsByClass("reference").remove();
			        Elements h2Elements=doc.getElementsByTag("h2");
					for(Element h2Element : h2Elements)
					{
						Element nextElement = h2Element.nextElementSibling();
						if(nextElement != null) 
						{
							boolean isTableNotReached = true;
							while(isTableNotReached)
							{
								if(nextElement.nodeName().equalsIgnoreCase("ul"))
								{
									String h2Title=h2Element.text();
								
									if(!h2Title.equals("See also"))
									{
									   Elements liList=nextElement.select("li");
									   for(int i=0;i<liList.size();i++)
									   {
										   AwardBean awardBean=new AwardBean();
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
										   
										  String date1=null;
										  String name=null;
										  String finalDate=null;
										  String listLink=null;
										   
										  Element li=liList.get(i);
										  String[] sp=li.text().split("–",2);
										  date1=sp[0];
										  
										  for(String spi:sp)
										   {
											   if(!spi.equals(sp[0]))
											   {
												   name=spi.trim();
												   Elements links=li.select("a[href]");
												   for(Element link:links)
												   {
													  listLink=link.absUrl("href");
												   }
											   }
										   }
										  
										  if(date1.trim().matches("[A-Za-z0-9]{4}"))
										  {
											
											String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
											SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											finalDate=newDateParser.format(oldDateParser.parse(str));
											
										  }  
										  
										  awardYear=finalDate;
										  awardRecipientName=name;
										  awardLink=listLink;
										  
										  System.out.println("awardYear"+awardYear); 
												
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
														awardDescription, awardField.trim(),awardRecipientName.trim(),
														awardRecipientLocation,awardYear,
														awardUrl, "VA-AWD", "VALUE"));
												
												if(flagTobreak==true)
										    		break;
										  
									   }
									 
									}
								 
									isTableNotReached = false;
								}
								else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
								{
									isTableNotReached = false;
								}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
								}
							}
						}
						
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Make-up Artist"))
				{
					awardTitle="National Film Award for Best Make-up Artist";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
					Element table=doc.select("table").get(1);
					Elements rows=table.select("tr");
					 for (Element row : rows)
					    {
				            int cellIndex = -1;
				            if(row.select("td").hasAttr("rowspan"))
				            {
				                for (Element cell : row.select("td")) 
				                {
				                    cellIndex++;
				                    if (cell.hasAttr("rowspan"))
				                    {
				                        int rowspanCount = Integer.parseInt(cell.attr("rowspan"));
				                        cell.removeAttr("rowspan");

				                        Element copyRow = row;
				                        Element currentNode = row;
				                        for (int i = rowspanCount; i > 1; i--) 
				                        {
				                            Element nextRow = currentNode.nextElementSibling();
				                            Element cellCopy = cell.clone();
				                            Element childTd = nextRow.child(cellIndex);
				                            childTd.after(cellCopy);
				                            currentNode = nextRow;
				                        }
				                    }
				                }
				            }
				        }
					for(int i=2;i<rows.size();i++)
					{
						String finalDate=null;
						String linkName=null;
						Element row=rows.get(i);
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("th");
						Elements colFilmName=row.select("td:eq(2)");
						Elements colLanguage=row.select("td:eq(3)");
						Elements links=colName.select("a[href]");
						
						for(Element link:links)
						{
							linkName=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
						awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
						awardLink=linkName;
						
						
						System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
					}
				}
				
				else if(awardDocTitle.contains("National Film Award for Best Production Design")||
						awardDocTitle.contains("National Film Award for Best Non-Feature Film Editing"))
				{
//					awardTitle="National Film Award for Best Production Design";
//					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					doc.getElementsByTag("small").remove();
				    Elements tables = doc.getElementsByTag("table");
				    Element table = tables.get(1);
				    Elements rows = table.select("tr");
				    for (Element row : rows)
				    {
			            int cellIndex = -1;
			            if(row.select("td").hasAttr("rowspan"))
			            {
			                for (Element cell : row.select("td")) 
			                {
			                    cellIndex++;
			                    if (cell.hasAttr("rowspan"))
			                    {
			                        int rowspanCount = Integer.parseInt(cell.attr("rowspan"));
			                        cell.removeAttr("rowspan");

			                        Element copyRow = row;
			                        Element currentNode = row;
			                        for (int i = rowspanCount; i > 1; i--) 
			                        {
			                            Element nextRow = currentNode.nextElementSibling();
			                            Element cellCopy = cell.clone();
			                            Element childTd = nextRow.child(cellIndex);
			                            childTd.after(cellCopy);
			                            currentNode = nextRow;
			                        }
			                    }
			                }
			            }
			        }
				    for(int j=2;j<rows.size();j++)
			        {
				    	String finalDate=null;
				    	String linkName=null;
				    	
				    	AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
				    	
				    	Element row=rows.get(j);
			            Elements colDate=row.select("td:eq(0)");
			            Elements colName=row.select("th");
			            Elements colFilmName=row.select("td:eq(2)");
			            Elements colLanguage=row.select("td:eq(3)");
			            
			            Elements links=colName.select("a[href]");
						
						for(Element link:links)
						{
							linkName=link.absUrl("href");
						}
			            
			            String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardField=colFilmName.text().replace("•", "").replace(" ", "").trim()+" "+colLanguage.text().replace("•", "").replace(" ", "").trim();
						awardLink=linkName;
						
						if(awardDocTitle.contains("National Film Award for Best Production Design"))
						{
							awardTitle="National Film Award for Best Production Design";
							awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
						}
						
						if(awardDocTitle.contains("National Film Award for Best Non-Feature Film Editing"))
						{
							awardTitle="National Film Award for Best Non-Feature Film Editing";
							awardRecipientName=colName.text().replace("•", "").replace(" ", "").trim();
						}
						
						System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
			            
			        }
					
				}
				
				else if(awardDocTitle.contains("Kalapi Award")||
						awardDocTitle.contains("Kavishwar Dalpatram Award")||
						awardDocTitle.contains("Kumar Suvarna Chandrak")||
						awardDocTitle.contains("Narsinh Mehta Award")||
						awardDocTitle.contains("Premanand Suvarna Chandrak")||
						awardDocTitle.contains("Ramanlal Nilkanth Hasya Paritoshik")||
						awardDocTitle.contains("Sahitya Gaurav Puraskar")||
						awardDocTitle.contains("Sahityaratna Award")||
						awardDocTitle.contains("Shayda Award")||
						awardDocTitle.contains("Vali Gujarati Gazal Award")||
						awardDocTitle.contains("Yuva Gaurav Puraskar"))
				
				{
					doc.getElementsByClass("reference").remove();
					Elements table=doc.getElementsByClass("wikitable");
				    Elements rows=table.select("tr");
				    for(int i=1;i<rows.size();i++)
				    {
				    	String nameList=null;
				    	String finalDate=null;
				    	
				    	AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
				    	
				    	Element row=rows.get(i);
				    	Elements colDate=row.select("td:eq(0)");
				    	Elements colName=row.select("td:eq(1)");
				    	Elements links=colName.select("a[href]");
				    	
				    	for(Element link:links)
				    	{
				    		nameList=link.absUrl("href");
				    	}
				    	
				    	String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 
						
						awardYear=finalDate;
					
						awardLink=nameList;
						
						if(awardDocTitle.contains("Kalapi Award"))
						{
							awardTitle="Kalapi Award";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						
						if(awardDocTitle.contains("Kavishwar Dalpatram Award"))
						{
							awardTitle="Kavishwar Dalpatram Award";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Kumar Suvarna Chandrak"))
						{
							awardTitle="Kumar Suvarna Chandrak";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Narsinh Mehta Award"))
						{
							awardTitle="Narsinh Mehta Award";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Premanand Suvarna Chandrak"))
						{
							awardTitle="Premanand Suvarna Chandrak";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Ramanlal Nilkanth Hasya Paritoshik"))
						{
							awardTitle="Ramanlal Nilkanth Hasya Paritoshik";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Sahitya Gaurav Puraskar"))
						{
							awardTitle="Sahitya Gaurav Puraskar";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Sahityaratna Award"))
						{
							awardTitle="Sahityaratna Award";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Shayda Award"))
						{
							awardTitle="Shayda Award";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Vali Gujarati Gazal Award"))
						{
							awardTitle="Vali Gujarati Gazal Award";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						if(awardDocTitle.contains("Yuva Gaurav Puraskar"))
						{
							awardTitle="Yuva Gaurav Puraskar";
							System.out.println("title is "+awardTitle); 
							awardRecipientName=colName.text().replace("•", "").trim();
						}
						
						System.out.println("awardYear"+awardYear);
							awardBeanList.add(awardBean);
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
				    }
				    
				}
				
				else if(awardDocTitle.contains("FPAI Indian Player of the Year"))
				{
					awardTitle="FPAI Indian Player of the Year";
					doc.getElementsByClass("reference").remove();
					doc.getElementsByClass("sortkey").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String finalDate=null;
						String sp1=null;
						String finalDate1=null;
						String fd=null;
						String listName=null; 
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(i);
						
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("td:eq(1)");
						Elements colClub=row.select("td:eq(2)");
						
						Elements links=colName.select("a[href]");
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
						
						ArrayList<String> dateList = new ArrayList<String>();
					    String[] sp=colDate.text().split("–");
					    if(sp[0].matches("[0-9]{4}"))
						  {
					    		 String str=sp[0].replaceFirst(String.valueOf((char) 160),"").trim();
								 SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								 finalDate=newDateParser.format(oldDateParser.parse(str));  
					     		 fd=finalDate;
								 dateList.add(finalDate);
								 
						  }
					    	for(String spi:sp)
					    	{
					    		if(!spi.equals(sp[0]))
					    		{
					    		  
					    		  sp1=spi;
					    		  if(sp1.matches("[0-9]{2}"))
					   			  {
					           		 String str=sp1.replaceFirst(String.valueOf((char) 160),"").trim();
					   				 SimpleDateFormat oldDateParser= new SimpleDateFormat("yy");
					   				 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					   				 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					   				 finalDate=newDateParser.format(oldDateParser.parse(str)); 
					   				 finalDate1=finalDate;
					   				 dateList.add(finalDate);
					   				 
					   			  }
					    		}
					        }
					    	
					    	awardYear=finalDate1;
					    	awardRecipientName=colName.text();
					    	awardField=colClub.text();
					    	awardLink=listName;
					    	
					    	awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;	
					}
					
				}
				
				else if(awardDocTitle.contains("Ushnas Prize")||
					    awardDocTitle.contains("Takhtasinh Parmar Prize"))
				{
					
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByClass("mw-editsection").remove();
				    Elements table=doc.getElementsByClass("wikitable");
			        Elements rows=table.select("tr");
			        for(int i=1;i<rows.size();i++)
			        {
			        	String finalDate=null;
			        	String sp1=null;
			        	String finalDate1=null;
			        	String listName=null;
			        	
			        	AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
			        	
			        	Element row=rows.get(i);
			        	Elements colDate=row.select("td:eq(0)");
			        	ArrayList<String> dateList = new ArrayList<String>();
			        	String[] sp=colDate.text().split("-");
			        	if(sp[0].matches("[0-9]{4}"))
						 {
			        		 String str=sp[0].replaceFirst(String.valueOf((char) 160),"").trim();
							 SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							 finalDate=newDateParser.format(oldDateParser.parse(str));  
				     		 dateList.add(finalDate);
						 }
			        	for(String spi:sp)
			        	{
			        		if(!spi.equals(sp[0]))
			        		{
			        		  
			        		  sp1=spi;
			        		  if(sp1.matches("[0-9]{2}"))
			       			  {
			               		 String str=sp1.replaceFirst(String.valueOf((char) 160),"").trim();
			       				 SimpleDateFormat oldDateParser= new SimpleDateFormat("yy");
			       				 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			       				 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
			       				 finalDate=newDateParser.format(oldDateParser.parse(str)); 
			       				 finalDate1=finalDate;
			       				 dateList.add(finalDate);
			       			  }
			        		}
			            }
			        	
			        	Elements colName=row.select("td:eq(1)");
			        	Elements colWork=row.select("td:eq(2)");
			        	Elements links=colName.select("a[href]");
			        	
			        	for(Element link:links)
			        	{
			        		listName=link.absUrl("href");
			        	}
			        	
			        	awardYear=finalDate1;
			        	awardRecipientName=colName.text();
			        	awardField=colWork.text();
			        	awardLink=listName;
			        	if(awardDocTitle.contains("Ushnas Prize"))
			        	{
			        		awardTitle="Ushnas Prize";
			        	}
			        	if(awardDocTitle.contains("Takhtasinh Parmar Prize"))
			        	{
			        		awardTitle="Takhtasinh Parmar Prize";
			        	}
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
			        }
				}
				else if(awardDocTitle.contains("Uma-Snehrashmi Prize"))
				{
					awardTitle="Uma-Snehrashmi Prize";
					doc.getElementsByClass("reference").remove();
			        doc.getElementsByClass("mw-editsection").remove();
			        Elements table=doc.getElementsByClass("wikitable");
			        Elements rows=table.select("tr");
			        for(int i=1;i<rows.size();i++)
			        {
			        	String finalDate=null;
			        	String sp1=null;
			        	String finalDate1=null;
			        	String listName=null;
			        	
			        	AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
			        	
			        	Element row=rows.get(i);
			        	Elements colDate=row.select("td:eq(0)");
			        	System.out.println(colDate.text());
			        	ArrayList<String> dateList = new ArrayList<String>();
			        	String[] sp=colDate.text().split("-",2);
			        	if(sp[0].matches("[0-9]{4}"))
						 {
			        		 String str=sp[0].replaceFirst(String.valueOf((char) 160),"").trim();
							 SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							 finalDate=newDateParser.format(oldDateParser.parse(str));  
				     		 dateList.add(finalDate);
						 }
			        	for(String spi:sp)
			        	{
			        		if(!spi.equals(sp[0]))
			        		{
			        		 
			        		  sp1=spi;
			        		  String[] sp2=sp1.split("-");
			        		  for(String sp2i:sp2)
			        		  {
			        			  if(!sp2i.equals(sp2[0]))
			        			  {
			        				  if(sp2i.matches("[0-9]{2}"))
			               			  {
			                       		 String str=sp2i.replaceFirst(String.valueOf((char) 160),"").trim();
			               				 SimpleDateFormat oldDateParser= new SimpleDateFormat("yy");
			               				 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			               				 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
			               				 finalDate=newDateParser.format(oldDateParser.parse(str)); 
			               				 finalDate1=finalDate;
			               			  }
			        				 
			        			  }
			        		  }
			        		  if(sp1.matches("[0-9]{2}"))
			       			  {
			               		 String str=sp1.replaceFirst(String.valueOf((char) 160),"").trim();
			       				 SimpleDateFormat oldDateParser= new SimpleDateFormat("yy");
			       				 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			       				 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
			       				 finalDate=newDateParser.format(oldDateParser.parse(str)); 
			       				 finalDate1=finalDate;
			       				 dateList.add(finalDate);
			       			  }
			        		}
			            }
			        	Elements colName=row.select("td:eq(1)");
			        	Elements colBook=row.select("td:eq(2)");
			            Elements links=colName.select("a[href]");
			        	
			        	for(Element link:links)
			        	{
			        		listName=link.absUrl("href");
			        	}
			        	
			        	awardYear=finalDate1;
						awardRecipientName=colName.text();
						awardField=colBook.text();
						awardLink=listName;
						
						System.out.println("awardYear"+awardYear); 
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
						
			        }
				}
				
				else if(awardDocTitle.contains("Bihari Puraskar"))
				{
					awardTitle="Bihari Puraskar";
					 doc.getElementsByClass("reference").remove();
				        doc.getElementsByTag("sup").remove();
						doc.getElementsByTag("small").remove();
						doc.getElementsByClass("mw-editsection").remove();
				        Elements h2Elements2=doc.getElementsByTag("p");
						for(Element h2Element2 : h2Elements2)
						{
								Element nextElement = h2Element2.nextElementSibling();
								if(nextElement != null) 
								{
									boolean isTableNotReached = true;
									while(isTableNotReached)
									{
										if(nextElement.nodeName().equalsIgnoreCase("ol"))
										{
											String h2Title=h2Element2.text();
											
											Elements li=nextElement.select("li");
											for(int i=0;i<li.size();i++)
											{
												String name=null;
												String finalDate=null;
												String field=null;
												String date1=null;
												String listName=null;
												
												AwardBean awardBean=new AwardBean();
												String awardId="";
												String awardIdentifier="";
												String awardYear="";
												String awardRecipientName="";
												String awardField="";
												String awardRecipientLocation="";
												String awardLink="";
												
												Element liList=li.get(i);
												String[] sp=liList.text().split(",");
												name=sp[0].trim();
												for(String spi:sp)
												{
													if(!spi.equals(sp[0]))
													{
														String[] sp1=spi.trim().split(" ",2);
														date1=sp1[0].trim();
														for(String sp1i:sp1)
														{
															if(!sp1i.equals(sp1[0]))
															{
																field=sp1i.trim().replace("-", "").trim();
															}
														}
														
													}
												}
												String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
												SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
												SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
												newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
												finalDate=newDateParser.format(oldDateParser.parse(str));
												
												Elements links=liList.select("a[href]");
												for(Element link:links)
												{
													listName=link.absUrl("href");
												}
												
												System.out.println(finalDate);
												System.out.println(name);
												System.out.println(field);
												System.out.println(listName);
												
												awardYear=finalDate;
												awardRecipientName=name;
												awardField=field;
												
												 System.out.println("awardYear"+awardYear); 
												 awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
																awardDescription, awardField.trim(),awardRecipientName.trim(),
																awardRecipientLocation,awardYear,
																awardUrl, "VA-AWD", "VALUE"));
														
														if(flagTobreak==true)
												    		break;
											}
											
										
											isTableNotReached = false;
										}
										else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
										{
											isTableNotReached = false;
										}
										else
										{
											if(nextElement.nextElementSibling() != null)
											{
												nextElement = nextElement.nextElementSibling();
											}
											break;
										}
									}
								}
								
						}
				}
				else if(awardDocTitle.contains("Saraswati Samman")||
						awardDocTitle.contains("Odakkuzhal Award"))
				{
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
				    doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					
					for(int i=1;i<rows.size();i++)
					{
							String finalDate=null;
							String field=null;
							String listName=null;
							
						    Element row=rows.get(i);
							
						    AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("td:eq(1)");
							Elements colWork=row.select("td:eq(2)");
							Elements colLanguage=row.select("td:eq(3)");
							Elements links=colWork.select("a[href]");
							for(Element link:links)
							{
								listName=link.absUrl("href");
							}
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							awardYear=finalDate;
							awardRecipientName=colName.text();
							awardField=colWork.text().replaceAll("([\"])", "")+" "+colLanguage.text();
							awardLink=listName;
							
							if(awardDocTitle.contains("Saraswati Samman"))
							{
								awardTitle="Saraswati Samman";
							}
							if(awardDocTitle.contains("Odakkuzhal Award"))
							{
								awardTitle="Odakkuzhal Award";
							}
							 
							 awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
											awardDescription, awardField.trim(),awardRecipientName.trim(),
											awardRecipientLocation,awardYear,
											awardUrl, "VA-AWD", "VALUE"));
									
									if(flagTobreak==true)
							    		break;
						
					}
				}
				
				else if(awardDocTitle.contains("SAARC Literary Award"))
				{
					awardTitle="SAARC Literary Award";
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
					
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String finalDate=null;
						String listName=null;
						
					  AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
						Element row=rows.get(i);
						
						Elements colDate=row.select("td:eq(0)");
						Elements colWriter=row.select("td:eq(1)");
						Elements colNationality=row.select("td:eq(2)");
						Elements links=colWriter.select("a[href]");
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colWriter.text();
						awardLink=listName;
						
						 awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
						
						
						//System.out.println(colNationality.text());
					
						
					}
				}
				
				else if(awardDocTitle.contains("Assam Valley Literary Award"))
				{
					awardTitle="Assam Valley Literary Award";
					doc.getElementsByClass("reference").remove();
			        doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
				    Elements h2Elements2=doc.getElementsByTag("h2");
					for(Element h2Element2 : h2Elements2)
					{
							Element nextElement = h2Element2.nextElementSibling();
							if(nextElement != null) 
							{
								boolean isTableNotReached = true;
								while(isTableNotReached)
								{
									if(nextElement.nodeName().equalsIgnoreCase("div"))
									{
										String h2Title=h2Element2.text();
										
										if(!h2Title.equals("References")&&!h2Title.equals("Navigation menu"))
										{
										  System.out.println(h2Title);
										  Element d1=nextElement.nextElementSibling();
										  Element liList=d1.nextElementSibling();
										  Elements li=liList.select("li");
										  for(int i=0;i<li.size();i++)
										  {
											String finalDate=null;
											String name=null;
											String listName=null;
											
											AwardBean awardBean=new AwardBean();
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
											
											Element liValue=li.get(i);
											Elements links=liValue.select("a[href]");
											
											for(Element link:links)
											{
												listName=link.absUrl("href");
											}
											
										    String[] sp=liValue.text().split(" ",2);
										    for(String spi:sp)
										    {
										    	if(!spi.equals(sp[0]))
										    	{
										    		name=spi;
										    	}
										    }
										    
										    String str=sp[0].replaceFirst(String.valueOf((char) 160),"").trim();
											SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											finalDate=newDateParser.format(oldDateParser.parse(str));
										    
										    awardYear=finalDate;
										    awardRecipientName=name;
										    awardLink=listName;
										    
										    System.out.println("awardYear"+awardYear); 	
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
														awardDescription, awardField.trim(),awardRecipientName.trim(),
														awardRecipientLocation,awardYear,
														awardUrl, "VA-AWD", "VALUE"));
												
												if(flagTobreak==true)
										    		break;
								           
										  }
										
										}
										isTableNotReached = false;
									}
									else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
									{
										isTableNotReached = false;
									}
									else
									{
										if(nextElement.nextElementSibling() != null)
										{
											nextElement = nextElement.nextElementSibling();
										}
										break;
									}
								}
							}
							
					}
				}
				
				
				
				else if(awardDocTitle.contains("J. C. Daniel Award"))
				{
					    awardTitle="J. C. Daniel Award";
					    doc.getElementsByClass("reference").remove();
				        doc.getElementsByTag("sup").remove();
						doc.getElementsByTag("small").remove();
					    doc.getElementsByClass("mw-editsection").remove();
					    doc.getElementsByClass("sortkey").remove();
					   
					    Elements table=doc.getElementsByClass("wikitable");
					    Elements rows=table.select("tr");
					    for(int i=1;i<rows.size();i++)
					    {
					    	String finalDate=null;
					    	String listName=null;
					    	String listName2=null;
					    	
					    	AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
					    	
					    	Element row=rows.get(i);
					        Elements colDate=row.select("td:eq(0)");
					    	Elements colName=row.select("th");
					    	Elements colN=row.select("td:eq(1)");
					    	Elements colWork=row.select("td:eq(2)");
				            Elements links=colName.select("a[href]");
				            Elements links1=colN.select("a[href]");
				            
				        	for(Element link:links)
				        	{
				        		listName=link.absUrl("href");
				        	}
				        	
				        	for(Element link:links1)
				        	{
				        		listName2=link.absUrl("href");
				        	}
				        	
				        	ArrayList<String> nameList=new ArrayList<String>();
							nameList.add(colName.text());
							nameList.add(colN.text());
							
							ArrayList<String> linkList=new ArrayList<String>();
							linkList.add(listName);
							linkList.add(listName2);
							
							String list="";
							for(String listi:linkList)
							{
								list +=listi;
							}
							
							String name="";
							for(String namei:nameList)
							{
								name +=namei;
							}
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							awardYear=finalDate;
							awardRecipientName=name;
							awardField=colWork.text();
							awardLink=list;
					    	
					    	System.out.println("awardYear"+awardYear); 
							
					    	awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
							if(flagTobreak==true)
						    		break;
					    }
				    
				}
				
				else if(awardDocTitle.contains("Moortidevi Award"))
				{
					awardTitle="Moortidevi Award";
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();	
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String listName=null;
						String finalDate=null;
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(i);
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("td:eq(1)");
						Elements colLanguage=row.select("td:eq(2)");
						Elements colWork=row.select("td:eq(3)");
						
						Elements links=colName.select("a[href]");
						
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
			            String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colName.text();
						awardField=colWork.text()+" "+colLanguage.text();
						awardLink=listName;
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
					}
					
				}
				
				else if(awardDocTitle.contains("O. V. Vijayan Sahitya Puraskaram"))
				{
					awardTitle="O. V. Vijayan Sahitya Puraskaram";
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();	
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows = table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String finalDate=null;
						String listName=null;
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(i);
						
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("td:eq(1)");
						Elements colWork=row.select("td:eq(2)");
						Elements colCategory=row.select("td:eq(3)");
						Elements links=colName.select("a[href]");
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=colDate.text();
						awardRecipientName=colName.text();
						awardField=colWork.text()+" "+colCategory.text();
						awardLink=listName;
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, 
								awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
					}
				}
				
				else if(awardDocTitle.contains("Crossword Book Award"))
				{
					awardTitle="Crossword Book Award";
					doc.getElementsByClass("reference").remove();
			        doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
					Elements h3Elements=doc.getElementsByTag("h3");
						for(Element h3Element : h3Elements)
						{
								Element nextElement = h3Element.nextElementSibling();
								if(nextElement != null) 
								{
									boolean isTableNotReached = true;
									while(isTableNotReached)
									{
										if(nextElement.nodeName().equalsIgnoreCase("ul"))
										{
											String h2Title=h3Element.text();
											if(!h2Title.equals("Personal tools") &&
											   !h2Title.equals("Not logged in") &&
											   !h2Title.equals("Talk") &&
											   !h2Title.equals("Contributions") &&
											   !h2Title.equals("Create account")&&
											   !h2Title.equals("Log in")&&
											   !h2Title.equals("Article")&&
											   !h2Title.equals("Views")&&
											   !h2Title.equals("Read")&&
											   !h2Title.equals("Edit")&&
											   !h2Title.equals("View history")&&
											   !h2Title.equals("Namespaces"))
			                               {
												
											Elements li1=nextElement.select("li");
											for(int i=0;i<li1.size();i++)
											{
												String name=null;
												String field=null;
												String finalDate=null;
												String bookField=null;
											    String text=null;
											    
											    AwardBean awardBean=new AwardBean();
												String awardId="";
												String awardIdentifier="";
												String awardYear="";
												String awardRecipientName="";
												String awardField="";
												String awardRecipientLocation="";
												String awardLink="";
												
											String str=h2Title.replaceFirst(String.valueOf((char) 160),"").trim();
											SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											finalDate=newDateParser.format(oldDateParser.parse(str));
												
												Element liList=li1.get(i);
												String[] sp1=liList.text().split(":");
												bookField=sp1[0];
												
												for(String sp1i:sp1)
												{
													if(!sp1i.equals(sp1[0]))
													{
														text=sp1i.trim();
													}
												}
												
												String[] sp=text.split("by",2);
												field=sp[0];
												for(String spi:sp)
												{
													if(!spi.equals(sp[0]))
													{
														name=spi.trim();
													}
														
												}
												awardYear=finalDate;
												awardRecipientName=name;
												awardField=bookField+" "+field;
													
													awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
															awardDescription, awardField.trim(),awardRecipientName.trim(),
															awardRecipientLocation,awardYear,
															awardUrl, "VA-AWD", "VALUE"));
													
													if(flagTobreak==true)
											    		break;
												
											}
											
			                               
			                               }
											isTableNotReached = false;
										
										}
										else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
										{
											isTableNotReached = false;
										}
										else
										{
											if(nextElement.nextElementSibling() != null)
											{
												nextElement = nextElement.nextElementSibling();
											}
											break;
										}
									}
								}
								
						}
					
						 Elements h3Elements1=doc.getElementsByTag("h3");
							for(Element h3Element1 : h3Elements1)
							{
									Element nextElement = h3Element1.nextElementSibling();
									if(nextElement != null) 
									{
										boolean isTableNotReached = true;
										while(isTableNotReached)
										{
											if(nextElement.nodeName().equalsIgnoreCase("p"))
											{
												String h2Title=h3Element1.text();
												Element ul=nextElement.nextElementSibling();
												
												if(!ul.text().contains("2013"))
												{
													
													Elements li=ul.select("li");
													for(int i=0;i<li.size();i++)
													{
														String name=null;
														String field=null;
														String type=null;
														String finalDate=null;
														ArrayList<String> nameList=new ArrayList<String>();
														ArrayList<String> fieldList=new ArrayList<String>();
														
														AwardBean awardBean=new AwardBean();
														String awardId="";
														String awardIdentifier="";
														String awardYear="";
														String awardRecipientName="";
														String awardField="";
														String awardRecipientLocation="";
														String awardLink="";
														
														Element liList=li.get(i);
														if(liList.text().contains("by"))
														{
															
															String[] sp3=liList.text().split("by");
														
															field=sp3[0];
															fieldList.add(sp3[0]);
															
															for(String sp3i:sp3)
															{
																if(!sp3i.equals(sp3[0]))
																{
																	
																	name=sp3i.trim();
																	nameList.add(sp3i.trim());
																}
															}
															
														}
														else if(liList.text().contains(","))
														{
															
															String[] sp4=liList.text().split(",");
															
															field=sp4[0];
															fieldList.add(sp4[0]);
															for(String sp4i:sp4)
															{
																if(!sp4i.equals(sp4[0]))
																{
																
																   name=sp4i.trim();
																   nameList.add(sp4i.trim());
																}
															}
														}
														String namelist="";
														for(String s:nameList)
														{
															namelist +=s;
														}
														String fieldlist="";
														for(String s1:fieldList)
														{
															fieldlist +=s1;
														}
													
													String str=h2Title.replaceFirst(String.valueOf((char) 160),"").trim();
													SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
													SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
													newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
													finalDate=newDateParser.format(oldDateParser.parse(str));
													
													awardYear=finalDate;
													awardRecipientName=namelist;
													awardField=fieldlist;
													
												System.out.println("awardYear"+awardYear); 
												awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
														awardDescription, awardField.trim(),awardRecipientName.trim(),
														awardRecipientLocation,awardYear,
														awardUrl, "VA-AWD", "VALUE"));
														
												if(flagTobreak==true)
												   break;
													
													}
													
												}
												
												isTableNotReached = false;
											}
											else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
											{
												isTableNotReached = false;
											}
											else
											{
												if(nextElement.nextElementSibling() != null)
												{
													nextElement = nextElement.nextElementSibling();
												}
												break;
											}
										}
									}
									
							}
				}
				
				else if(awardDocTitle.contains("Mathrubhumi Literary Award")
					||  awardDocTitle.contains("Muttathu Varkey Award") )
				{
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();	
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String listName=null;
						String finalDate=null;
						
						Element row=rows.get(i);
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("td:eq(1)");
				
						Elements links=colName.select("a[href]");
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colName.text();
						awardLink=listName;
						
						if(awardDocTitle.contains("Mathrubhumi Literary Award"))
						{
							awardTitle="Mathrubhumi Literary Award";
						}
						if(awardDocTitle.contains("Muttathu Varkey Award"))
						{
							awardTitle="Muttathu Varkey Award";
						}
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
								
						if(flagTobreak==true)
						   break;
						
						
					}
				}
				
				else if(awardDocTitle.contains("Vyas Samman"))
				{
					awardTitle="Vyas Samman";
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
			     	Elements h2Elements=doc.getElementsByTag("p");
			     	for(Element h2Element : h2Elements)
					{
							Element nextElement = h2Element.nextElementSibling();
							if(nextElement != null) 
							{
								boolean isTagNotReached = true;
								while(isTagNotReached)
								{
									if(nextElement.nodeName().equalsIgnoreCase("dl"))
									{
										String h2Title=h2Element.text();
										Element dt=h2Element.nextElementSibling();
										Element dl=dt.nextElementSibling();
										Elements li=dl.select("li");
										for(int i=0;i<li.size();i++)
										{
											String date1=null;
											String name=null;
											String finalDate=null;
											
											AwardBean awardBean=new AwardBean();
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
											
											Element liList=li.get(i);
											String[] sp=liList.text().split("-");
											
											date1=sp[0];
											
											for(String spi:sp)
											{
												if(!spi.equals(sp[0]))
												{
												
													name=spi.trim();
												}
											}
											
											
											String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
											SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											finalDate=newDateParser.format(oldDateParser.parse(str));
											
											awardYear=finalDate;
											awardRecipientName=name;
											
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
													awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
													
											if(flagTobreak==true)
											   break;
											
										}
										
										isTagNotReached = false;
									
									}
									else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
									{
										isTagNotReached = false;
									}
									else
									{
										if(nextElement.nextElementSibling() != null)
										{
											nextElement = nextElement.nextElementSibling();
										}
									break;
									}
								}
							}
					}
				}
				else if(awardDocTitle.contains("Vishnupuram Award"))
				{
					awardTitle="Vishnupuram Award";
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
					Elements h2Elements=doc.getElementsByTag("h2");
					for(Element h2Element : h2Elements)
					{
						Element nextElement = h2Element.nextElementSibling();
						if(nextElement != null) 
						{
							boolean isTableNotReached = true;
							while(isTableNotReached)
							{
								if(nextElement.nodeName().equalsIgnoreCase("ul"))
								{
									String h4Title=h2Element.text();
									Element li=h2Element.nextElementSibling();
									Elements liVale=li.select("li");
									for(int i=0;i<liVale.size();i++)
									{
									    String name=null;
									    String date1=null;
									    String finalDate=null;
									    String listName=null;
									    
									    AwardBean awardBean=new AwardBean();
										String awardId="";
										String awardIdentifier="";
										String awardYear="";
										String awardRecipientName="";
										String awardField="";
										String awardRecipientLocation="";
										String awardLink="";
										
										Element liList=liVale.get(i);
										Elements links=liList.select("a[href]");
										for(Element link:links)
										{
											listName=link.absUrl("href");
										}
										String[] sp=liList.text().split("\\[|\\(");
										
										name=sp[0];
										for(String spi:sp)
										{
											if(!spi.equals(sp[0]))
											{
												
												date1=spi.replaceAll("\\)|\\]", "");
											}
										}
										
										String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
										SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
										SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
										finalDate=newDateParser.format(oldDateParser.parse(str));
							
										awardYear=finalDate;
										awardRecipientName=name;
										awardLink=listName;
										
										awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
												
										if(flagTobreak==true)
										   break;
										
									}
								 
									isTableNotReached = false;
								}
								else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
								{
									isTableNotReached = false;
								}
								else
								{
									if(nextElement.nextElementSibling() != null)
									{
										nextElement = nextElement.nextElementSibling();
									}
									break;
								}
							}
						}
						
					}
				}
				
				else if(awardDocTitle.contains("Someswari Brahma Literary Award"))
				{
					awardTitle="Someswari Brahma Literary Award";
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
					
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String finalDate=null;
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(i);
						Elements colDate=row.select("td:eq(1)");
						Elements colBook=row.select("td:eq(2)");
						Elements colAuthor=row.select("td:eq(3)");
						Elements colCategoryOfBook=row.select("td:eq(4)");
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=colDate.text();
						awardRecipientName=colAuthor.text();
						awardField=colBook.text()+" "+colCategoryOfBook.text();
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
								
						if(flagTobreak==true)
						   break;
					}
			
				}
				
				else if(awardDocTitle.contains("Edasseri Award") ||
						awardDocTitle.contains("Indu Sharma Katha Samman") ||
						awardDocTitle.contains("Vayalar Award"))
				{
					doc.getElementsByClass("reference").remove();
			        doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();	
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String finalDate=null;
						String listName=null;
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(i);
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("td:eq(1)");
						Elements colWork=row.select("td:eq(2)");
						
						Elements links=colName.select("a[href]");
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colName.text().trim();
						awardField=colWork.text().trim();
						awardLink=listName;
						
						if(awardDocTitle.contains("Edasseri Award"))
						{
							awardTitle="Edasseri Award";
						}
						if(awardDocTitle.contains("Indu Sharma Katha Samman") )
						{
							awardTitle="Indu Sharma Katha Samman";
						}
						if(awardDocTitle.contains("Vayalar Award"))
						{
							awardTitle="Vayalar Award";
						}
						
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
								
						if(flagTobreak==true)
						   break;
					}
				}
				else if(awardDocTitle.contains("Ezhuthachan Puraskaram")||
						awardDocTitle.contains("Mathrubhumi Literary Award")||
						awardDocTitle.contains("Vallathol Award"))
				{
					
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
				    doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();	
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
						for(int i=1;i<rows.size();i++)
						{
							String finalDate=null;
							String listName=null;
							
							Element row=rows.get(i);
							Elements colDate=row.select("td:eq(0)");
							Elements colName=row.select("td:eq(1)");
							
							
							Elements links=colName.select("a[href]");
							for(Element link:links)
							{
								listName=link.absUrl("href");
							}
							AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName="";
							String awardField="";
							String awardRecipientLocation="";
							String awardLink="";
							
							String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
							SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
							SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							finalDate=newDateParser.format(oldDateParser.parse(str));
							
							awardYear=finalDate;
							awardRecipientName=colName.text();
							awardLink=listName;
							
							if(awardDocTitle.contains("Ezhuthachan Puraskaram"))
							{
								awardTitle="Ezhuthachan Puraskaram";
							}
							
							
							if(awardDocTitle.contains("Mathrubhumi Literary Award"))
							{
								awardTitle="Mathrubhumi Literary Award";
							}
							if(awardDocTitle.contains("Vallathol Award"))
							{
								awardTitle="Vallathol Award";
							}	
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
										awardDescription, awardField.trim(),awardRecipientName.trim(),
										awardRecipientLocation,awardYear,
										awardUrl, "VA-AWD", "VALUE"));
								
								if(flagTobreak==true)
						    		break;
						}
				}
				
				else if(awardDocTitle.contains("Asan Smaraka Kavitha Puraskaram"))
				{
					awardTitle="Asan Smaraka Kavitha Puraskaram";
					doc.getElementsByClass("reference").remove();
			        doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();	
					Elements h2Elements=doc.getElementsByTag("h2");
					for(Element h2Element : h2Elements)
					{
							Element nextElement = h2Element.nextElementSibling();
							if(nextElement != null) 
							{
								boolean isTagNotReached = true;
								while(isTagNotReached)
								{
									if(nextElement.nodeName().equalsIgnoreCase("ul"))
									{
										String h2Title=h2Element.text();
										if(!h2Title.equals("See also") && 
										   !h2Title.equals("External links"))
										{
											
										Elements li=nextElement.select("li");
										for(int i=0;i<li.size();i++)
										{
											String finalDate=null;
											String name=null;
											String date1=null;
											String listName=null;
											
											AwardBean awardBean=new AwardBean();
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
											
											Element liList=li.get(i);
											Elements links=liList.select("a[href]");
											for(Element link:links)
											{
												listName=link.absUrl("href");
											}
											
											
											
											String[] sp=liList.text().split(":");
										
											date1=sp[0];
											for(String spi:sp)
											{
												if(!spi.equals(sp[0]))
												{
												
													name=spi.trim();
												}
											}
											String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
											SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											finalDate=newDateParser.format(oldDateParser.parse(str));
											
											awardYear=finalDate;
											awardRecipientName=name;
											awardLink=listName;
										
											
											System.out.println("awardYear"+awardYear); 
											
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
														awardDescription, awardField.trim(),awardRecipientName.trim(),
														awardRecipientLocation,awardYear,
														awardUrl, "VA-AWD", "VALUE"));
												
												if(flagTobreak==true)
										    		break;
											
											
										}
										
										
										}
										isTagNotReached = false;
									
									}
									else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
									{
										isTagNotReached = false;
									}
									else
									{
										if(nextElement.nextElementSibling() != null)
										{
											nextElement = nextElement.nextElementSibling();
										}
										break;
									}
								}
							}	
					}
				}
				else if(awardDocTitle.contains("List of Yuva Puraskar winners for Assamese")||
						awardDocTitle.contains("List of Yuva Puraskar winners for Bengali")||
						awardDocTitle.contains("List of Yuva Puraskar winners for Gujarati")||
						awardDocTitle.contains("List of Yuva Puraskar winners for Bodo"))
				{
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();	
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String listName=null;
						String finalDate=null;
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(i);
						Elements colDate=row.select("td:eq(0)");
						Elements colAuthor=row.select("td:eq(1)");
						Elements colWork=row.select("td:eq(2)");
						Elements colTypeOfWork=row.select("td:eq(3)");
						Elements links=colAuthor.select("a[href]");
						
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colAuthor.text();
						awardField=colWork.text()+" "+colTypeOfWork.text();
						awardLink=listName;
						
						if(awardDocTitle.contains("List of Yuva Puraskar winners for Assamese"))
						{
							awardTitle="List of Yuva Puraskar winners for Assamese";
						}
						
						if(awardDocTitle.contains("List of Yuva Puraskar winners for Bengali"))
						{
							awardTitle="List of Yuva Puraskar winners for Bengali";
						}
					
						if(awardDocTitle.contains("List of Yuva Puraskar winners for Gujarati"))
						{
							awardTitle="List of Yuva Puraskar winners for Gujarati";
						}
						
						if(awardDocTitle.contains("List of Yuva Puraskar winners for Bodo"))
						{
							awardTitle="List of Yuva Puraskar winners for Bodo";
						}
				
						
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
						
					}
				}
				
				else if(awardDocTitle.contains("Saraswati Samman"))
				{
					awardTitle="Saraswati Samman";
					doc.getElementsByClass("reference").remove();
			        doc.getElementsByTag("sup").remove();
					doc.getElementsByTag("small").remove();
					doc.getElementsByClass("mw-editsection").remove();
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					for(int i=1;i<rows.size();i++)
					{
						String finalDate=null;
						String listName=null;
						
						Element row=rows.get(i);
						
						Elements colDate=row.select("td:eq(0)");
						Elements colName=row.select("td:eq(1)");
						Elements colWork=row.select("td:eq(2)");
						Elements colLanguage=row.select("td:eq(3)");
						Elements links=row.select("a[href]");
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						for(Element link:links)
						{
							listName=link.absUrl("href");
						}
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colName.text();
						awardField=colWork.text().replaceAll("([\"])", "")+" "+colLanguage.text();
						awardLink=listName;
						
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
					
						
						
					}
				}
				
				else if(awardDocTitle.contains("Ranjitram Suvarna Chandrak"))
				{
					awardTitle="Ranjitram Suvarna Chandrak";
					doc.getElementsByClass("reference").remove();
				    doc.getElementsByClass("mw-editsection").remove();
				    Elements h2Elements=doc.getElementsByTag("h2");
						for(Element h2Element : h2Elements)
						{
							Element nextElement = h2Element.nextElementSibling();
							if(nextElement != null) 
							{
								boolean isTableNotReached = true;
								while(isTableNotReached)
								{
									if(nextElement.nodeName().equalsIgnoreCase("ul"))
									{
										String h2Title=h2Element.text();
										String name=null;
										String finalDate=null;
										String listName=null;
										
										Elements li=nextElement.select("li");
										for(int i=0;i<li.size();i++)
										{
											AwardBean awardBean=new AwardBean();
											String awardId="";
											String awardIdentifier="";
											String awardYear="";
											String awardRecipientName="";
											String awardField="";
											String awardRecipientLocation="";
											String awardLink="";
											
											Element liList=li.get(i);
											Elements links=liList.select("a[href]");
											for(Element link:links)
											{
												listName=link.absUrl("href");
											}
											
											String[] sp=liList.text().split(" ",2);
											
											String str=sp[0].replaceFirst(String.valueOf((char) 160),"").trim();
											SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
											SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
											newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
											finalDate=newDateParser.format(oldDateParser.parse(str));
											
											for(String spi:sp)
											{
												if(!spi.equals(sp[0]))
												{
												   name=spi.trim();
												}
											}
											
											System.out.println(finalDate);
											System.out.println(name);
											System.out.println(listName);
											
											awardYear=finalDate;
											awardRecipientName=name;
											awardLink=listName;
											
											System.out.println("awardYear"+awardYear); 
								    
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
														awardDescription, awardField.trim(),awardRecipientName.trim(),
														awardRecipientLocation,awardYear,
														awardUrl, "VA-AWD", "VALUE"));
												
												if(flagTobreak==true)
										    		break;
											
													
										}
										
										
									
										isTableNotReached = false;
									}
									else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
									{
										isTableNotReached = false;
									}
									else
									{
										if(nextElement.nextElementSibling() != null)
										{
											nextElement = nextElement.nextElementSibling();
										}
										break;
									}
								}
							}
							
						}
				}
				
				else if(awardDocTitle.contains("Pampa Award")
					||  awardDocTitle.contains("Narmad Suvarna Chandrak"))
				{
					doc.getElementsByClass("reference").remove();
					Elements table=doc.getElementsByClass("wikitable");
				    Elements rows=table.select("tr");
				   
				    for(int i=1;i<rows.size();i++)
				    {
				    	String nameList=null;
				    	String finalDate=null;
				    	
				    	AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
				    	
				    	Element row=rows.get(i);
				    	Elements colDate=row.select("td:eq(0)");
				    	Elements colName=row.select("td:eq(1)");
				    	Elements colWork=row.select("td:eq(2)");
				    	
				    	Elements links=colName.select("a[href]");
				    	
				    	for(Element link:links)
				    	{
				    		nameList=link.absUrl("href");
				    	}
				    	
				    	String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str)); 
						
						awardYear=finalDate;
						awardRecipientName=colName.text().trim();
						awardField=colWork.text().trim();
						awardLink=nameList;
						
						if(awardDocTitle.contains("Narmad Suvarna Chandrak"))
						{
							awardTitle="Narmad Suvarna Chandrak";
							System.out.println("title is "+awardTitle); 
						}
						
						if(awardDocTitle.contains("Pampa Award"))
						{
							awardTitle="Pampa Award";
							System.out.println("title is "+awardTitle); 
						}
						
						System.out.println("awardYear"+awardYear); 
							awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
									awardDescription, awardField.trim(),awardRecipientName.trim(),
									awardRecipientLocation,awardYear,
									awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;
				    }
				}
				
				else if(awardDocTitle.contains("Sangeetha Kalanidhi")||
						awardDocTitle.contains("Sangeetha Kalasikhamani")||
						awardDocTitle.contains("Swaralaya Kairali Yesudas Award")||
						awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Male")||
						awardDocTitle.contains("Bollywood Movie Award – Best Playback Singer Female")||
						awardDocTitle.contains("Isai Perarignar")||
						awardDocTitle.contains("Lata Mangeshkar Award")||
						awardDocTitle.contains("Kamal Kumari National Award")||
						awardDocTitle.contains("Jakanachari Award")||
						awardDocTitle.contains("Nishagandhi Puraskaram")||
						awardDocTitle.contains("Vijay Music Awards")||
						awardDocTitle.equalsIgnoreCase("Mirchi Music Awards South - Wikipedia")||
						awardDocTitle.equalsIgnoreCase("Mirchi Music Awards Bangla - Wikipedia")||
						awardDocTitle.equalsIgnoreCase("Mirchi Music Awards - Wikipedia")||
						awardDocTitle.contains("Shilp Guru"))
				{
					
					MysqlAwardExtractorParser m = new MysqlAwardExtractorParser();
				    m.sqlFunction(awardDocTitle,doc,awardBeanList,awardDescription,awardUrl);
				}
				else if(awardDocTitle.contains("Nataka Kalasarathy"))
				{
					awardTitle="Nataka Kalasarathy";
					 doc.getElementsByClass("reference").remove();
						Elements h2Elements = doc.getElementsByTag("h2");
						for(Element h3Element : h2Elements)
						 {
								Element nextElement = h3Element.nextElementSibling();
								if(nextElement != null)
								{
									boolean isTableNotReached = true;
									while(isTableNotReached) 
									{
										if(nextElement.nodeName().equalsIgnoreCase("ul"))
										{
											String tableTitle = h3Element.text();
											Elements li=nextElement.select("li");
											for(int i=0;i<li.size();i++)
											{
												 AwardBean awardBean=new AwardBean();
													String awardId="";
													String awardIdentifier="";
													String awardYear="";
													String awardRecipientName="";
													String awardField="";
													String awardRecipientLocation="";
													String awardLink="";
												
												
												String finalDate="";
												String name="";
												String date1="";
												Element liList=li.get(i);
											if(!liList.text().equals("Kathadi Ramamurthy"))
											{
												String[] st=liList.text().split(" ",2);
												date1=st[0];
												String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
												SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
												SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
												newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
												finalDate=newDateParser.format(oldDateParser.parse(str));
											
												for(String sti:st)
												{
													if(!sti.equals(st[0]))
													{
														name=sti;
													}
														
												}
											}
											if(liList.text().equals("Kathadi Ramamurthy"))
											{
												name=liList.text();
											}
											awardYear=finalDate;
											awardRecipientName=name;
											awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
													awardDescription, awardField.trim(),awardRecipientName.trim(),
												    awardRecipientLocation,awardYear,
													awardUrl, "VA-AWD", "VALUE"));
												
												if(flagTobreak==true)
										    		break;	
											
											}
										}
										else if(nextElement.nodeName().equalsIgnoreCase("h2")) 
										{
											isTableNotReached = false;
										}
										else
										{
											if(nextElement.nextElementSibling() != null)
											{
												nextElement = nextElement.nextElementSibling();
											}
											
										}
										break;
									}
										
									
								}
						 }
				}
				
				
				//############
				else if (awardDocTitle.contains("Vir Chakra"))
				{	
					awardTitle="Vir Chakra";
					System.out.println("title is "+awardTitle); 
					doc.getElementsByClass("reference").remove();
					Elements table=doc.getElementsByClass("wikitable");
					Elements rows=table.select("tr");
					 for(int i=1;i<rows.size();i++)
					 {
						
						 Element row=rows.get(i);
						 Elements col_name=row.select("td:eq(1)");
						 Elements link_name=col_name.select("a[href]");
						 String link_list_name=null;
						 for(Element links : link_name)
						 {
							 link_list_name=links.absUrl("href");
						 }
						 Elements col_awarded=row.select("td:eq(4)");
						 Elements link_awarded=col_awarded.select("a[href]");
						 String list_link_awarded=null;
						 for(Element links:link_awarded)
						 {
							 list_link_awarded=links.absUrl("href");
						 }
						
						 AwardBean awardBean=new AwardBean();
							String awardId="";
							String awardIdentifier="";
							String awardYear="";
							String awardRecipientName=col_name.text();
							String awardField=col_awarded.text();
							String awardRecipientLocation="";
							String awardLink=link_list_name;
						 
						 Elements col_date=row.select("td:eq(3)");
						 String date1=null;
						 date1=col_date.text();
						 
						 String str=date1.replaceFirst(String.valueOf((char) 160),"").trim();
						 if(date1.matches("[0-9]{4}"))
						 {
							
							    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
								SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
								newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
								System.out.println("olddateparser"+oldDateParser.parse(str)); 
								awardYear=newDateParser.format(oldDateParser.parse(str)); 
								
						 }
						 else if(date1.matches("[0-9]{2} [a-z,A-Z]{3} [0-9]{4}"))
						 {
							 
							 SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMM yyyy");   
							 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							 System.out.println("olddateparser"+oldDateParser.parse(str)); 
							 awardYear=newDateParser.format(oldDateParser.parse(str)); 
						 }
						 else if(date1.matches("[0-9]{2} [a-z,A-Z]{4} [0-9]{4}"))
						 {
							 
							 SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMMM yyyy");      
							 SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
							 newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
							 System.out.println("olddateparser"+oldDateParser.parse(str)); 
							 awardYear=newDateParser.format(oldDateParser.parse(str));
						 }
						 else if(date1.matches("[0-9]{2} [a-z,A-Z]{3} [0-9]{4}"))
						 {
							SimpleDateFormat oldDateParser= new SimpleDateFormat("dd-MMM-yyyy");      
						    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						    newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						    System.out.println("olddateparser"+oldDateParser.parse(str)); 
						    awardYear=newDateParser.format(oldDateParser.parse(str));
						 }
						 else if(date1.matches("[0-9]{2}-[a-z,A-Z]{3}-[0-9]{4}"))
						 {
						   SimpleDateFormat oldDateParser= new SimpleDateFormat("dd-MMM-yyyy");      
						   SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						   newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						   System.out.println("olddateparser"+oldDateParser.parse(str)); 
						   awardYear=newDateParser.format(oldDateParser.parse(str));
						 }
						 else
						 {
							SimpleDateFormat oldDateParser= new SimpleDateFormat("dd MMMM yyyy");      
						    SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						    newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						    System.out.println("olddateparser"+oldDateParser.parse(str)); 
						    awardYear=newDateParser.format(oldDateParser.parse(str));
						 }
						 System.out.println("awardYear"+awardYear); 
			
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
								awardDescription, awardField.trim(),awardRecipientName.trim(),
							    awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
							
							if(flagTobreak==true)
					    		break;		
					
				}
					
			}	
				
				
				else
				{	
					for(int i=1;i<tableData.children().size();i++)
						{
							//System.out.println("table data child is---"+tableData.child(i).text());  
						System.out.println("title is "+awardTitle); 
								Element trData=tableData.child(i);
								
								for(int j=4;j<trData.children().size();j++)
								{
									//System.out.println("year: "+trData.child(j).text());
									Element tdData=trData.child(j);
									for(int k=0;k<tdData.children().size();k+=4)
									{
										AwardBean awardBean=new AwardBean();
										String awardId="";
										String awardIdentifier="";
										String awardYear="";
										String awardRecipientName="";
										String awardField="";
										String awardRecipientLocation="";
										String awardLink="";
										
										for(Element name:tdData.child(k+1).getElementsByClass("fn"))
										{
											awardRecipientName=name.text();
											System.out.println("awardRecipientName"+awardRecipientName); 
										}
										
										String date=tdData.child(k).text(); 
										SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
										SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
										newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
										
										System.out.println("olddateparser"+oldDateParser.parse(date)); 
										
										awardYear=newDateParser.format(oldDateParser.parse(date)); 
										System.out.println("awardYear"+awardYear); 
										
										awardField=tdData.child(k+2).text();
										awardRecipientLocation=tdData.child(k+3).text();
										
										awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
												awardDescription, awardField.trim(),awardRecipientName.trim(),
												awardRecipientLocation,awardYear,
												awardUrl, "VA-AWD", "VALUE"));
									
									if(k==tdData.children().size()-1)
										break;
									}
								}
							}
						
					}
		}
	
			else if(awardStateTitle.contains("Sangeet Natak AkademiYuva Puraskar")||
					awardStateTitle.contains("Sangeet Natak AkademiTagore Akademi Fellow")||
					awardStateTitle.contains("Sangeet Natak AkademiAkademi Fellow")||
					awardStateTitle.contains("Sangeet Natak Akademi")||
					awardStateTitle.contains("Sangeet Natak AkademiTagore Akademi Awardee"))
			{	
				doc.getElementsByTag("strong").remove();
				Elements table=doc.getElementsByClass("awrd_tbl");
				Elements rows=table.select("tr");
				for(int i=2;i<rows.size();i++)
				{
					Element row=rows.get(i);
					Elements col0=row.select("td:eq(0)");
					Elements col1=row.select("td:eq(1)");
					Elements col2=row.select("td:eq(2)");
					Elements links=col0.select("a[href]");
					String List_link=null;
					for(Element link:links)
					{
						List_link=link.absUrl("href");
					}
					
					AwardBean awardBean=new AwardBean();
					String awardId="";
					String awardIdentifier="";
					String awardYear="";
					String awardRecipientName=col0.text().trim();
					String awardField=col2.text().trim();
					String awardRecipientLocation="";
					String awardLink=List_link;
					
					String str1=col1.text().replaceFirst(String.valueOf((char) 160),"").trim(); 
				    String date = str1;
				    SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
					
					if(awardStateTitle.contains("Sangeet Natak AkademiYuva Puraskar"))
					{
						awardTitle="Sangeet Natak AkademiYuva Puraskar";
						System.out.println("title is "+awardTitle);
					}
					
					if(awardStateTitle.contains("Sangeet Natak AkademiTagore Akademi Fellow"))
					{
						awardTitle="Sangeet Natak AkademiTagore Akademi Fellow";
						System.out.println("title is "+awardTitle);
					}
					
					if(awardStateTitle.contains("Sangeet Natak AkademiAkademi Fellow"))
					{
						awardTitle="Sangeet Natak AkademiAkademi Fellow";
					}
					
					if(awardStateTitle.contains("Sangeet Natak Akademi"))
					{
						awardTitle="Sangeet Natak AkademiAkademi Awardee";
					}
					
					if(awardStateTitle.contains("Sangeet Natak AkademiTagore Akademi Awardee"))
					{
						awardTitle="Sangeet Natak AkademiTagore Akademi Awardee";
					}
					
					awardYear=newDateParser.format(oldDateParser.parse(date)); 
					System.out.println("awardYear"+awardYear); 
	
					awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, awardTitle,
							awardDescription, awardField.trim(),awardRecipientName.trim(),
							awardRecipientLocation,awardYear,
							awardUrl, "VA-AWD", "VALUE"));
					
					if(flagTobreak==true)
			    		break;
					
				}
				
			}
			
			else if(award_Title.contains("..:: SAHITYA : Akademi Awards ::.."))
			{
				award_Title=award_Title.replaceAll("..::", "").replaceAll(":", "").trim();
				System.out.println(award_Title);
				Elements tables=doc.select("table");
				for(int i=1;i<tables.size();i++)
				{
					Element table=tables.get(i);
					Element heading=table.previousElementSibling();
					
					Elements rows=table.select("tr");
					for(int j=1;j<rows.size();j++)
					{
					
						String finalDate=null;
						String typeofBook=null;
						
						AwardBean awardBean=new AwardBean();
						String awardId="";
						String awardIdentifier="";
						String awardYear="";
						String awardRecipientName="";
						String awardField="";
						String awardRecipientLocation="";
						String awardLink="";
						
						Element row=rows.get(j);
						Elements colDate=row.select("td:eq(0)");
						Elements colBookName=row.select("td:eq(1)");
						Elements colAuthor=row.select("td:eq(2)");
						
						String str=colDate.text().replaceFirst(String.valueOf((char) 160),"").trim();
						SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
						SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
						newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						finalDate=newDateParser.format(oldDateParser.parse(str));
						
						awardYear=finalDate;
						awardRecipientName=colAuthor.text();
						awardField=colBookName.text()+" "+"-"+heading.text();
						
						
						System.out.println("awardYear"+awardYear); 
						
						awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, award_Title,
								awardDescription, awardField,awardRecipientName,
								awardRecipientLocation,awardYear,
								awardUrl, "VA-AWD", "VALUE"));
						
						if(flagTobreak==true)
				    		break;
					}
				}
			}
			
			
			else if(award_Title.contains("Jeevan Raksha Padak Awards – 2014 announced"))
			{
				award_Title="";
				Elements p=doc.select("p[class=MsoNormal]");		
				for(int i=3;i<p.size();i++)
				{
					Element pi=p.get(i);
					Elements pu=pi.select("u");
					
					AwardBean awardBean=new AwardBean();
					String awardId="";
					String awardIdentifier="";
					String awardYear="";
					String awardRecipientName="";
					String awardField="";
					String awardRecipientLocation="";
					String awardLink="";
					
					for(int j=0;j<pu.size();j++)
					{
						Element v1=pu.get(j);
						if(!v1.text().matches("\u00a0"))
						{
							award_Title=v1.text().trim();
							
						}
					}
					if(!pi.text().equals("***")&&!pi.text().equals("KSD/AH/BK")&& !pi.text().matches("\u00a0"))
					{
					if(!pi.text().equals(pu.text()))
					{
					String [] sp=pi.text().replaceAll("[0-9].{2}", "").split(",");
					
					awardRecipientName=sp[0].trim().replace("     ", "").replace("     ", "").replace(" ", "").replace(" ", "");
					for(String str:sp)
					{
						if(!str.equals(sp[0]))
						{
						
						awardRecipientLocation=str.trim().replace("                     ", "").replace("                  ", "").replace("               ", "");
						}
					} 
					
					SimpleDateFormat oldDateParser= new SimpleDateFormat("yyyy");
					SimpleDateFormat newDateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					newDateParser.setTimeZone(TimeZone.getTimeZone("IST"));
						
					System.out.println("olddateparser"+oldDateParser.parse("2014")); 
					awardYear=newDateParser.format(oldDateParser.parse("2014"));
					
					awardBeanList.add(awardBeanValue(awardId, awardLink, awardIdentifier, award_Title,
							awardDescription, awardField,awardRecipientName,
							awardRecipientLocation.trim(),awardYear.trim(),
							awardUrl, "VA-AWD", "VALUE"));
					
					if(flagTobreak==true)
			    		break;
					}
					}
				}
				
			}
				LOG.info("Generated AwardBeanList is==>"+awardBeanList);
				return awardBeanList;
		}
		catch(Exception e)
		{
			e.printStackTrace(); 
		}
		return awardBeanList;
}
public AwardBean awardBeanValue(String id,String link,String Identifier,String title,String description,String field,String name,String location,String year,String url,String VA_AWD,String VALUE)

{
	AwardBean awardBean=new AwardBean();
	
	awardId=year+name.replaceAll(" ", "");
	awardIdentifier="VALUE_"+year+"_"+name.replaceAll(" ", "");
	
	awardBean.setId(awardId);
	awardBean.setLink(link);
	awardBean.setIdentifier(awardIdentifier);
	awardBean.setTitle(title);
	awardBean.setDescription(description);
	awardBean.setField(field);
	awardBean.setAwardRecipientName(name);
	awardBean.setLocation(location);
	awardBean.setDate(year);
	awardBean.setUrl(url);
	awardBean.setIdCode("VA-AWD");
	awardBean.setResourceCode("VALUE"); 
	return awardBean;
	
}


}