package cdac.nvli.extractor.extract;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import cdac.nvli.extractor.extract.ActsExtractor;
import cdac.nvli.extractor.extract.AwardExtractor;
import cdac.nvli.extractor.extract.PolicyExtractor;
import cdac.nvli.extractor.extract.PublicationExtractor;
import cdac.nvli.extractor.extract.SchemeExtractor;
import cdac.nvli.extractor.parse.ActsExtractorParser;
import cdac.nvli.extractor.extract.ExaminationExtractor;
import cdac.nvli.extractor.extract.ConferenceExtractor;

public class VasExtractor {
	
		private static String URLS_SEED_FILE_PATH; 
	
	public static void main(String[] args) {
		
		URLS_SEED_FILE_PATH=args[0];
		
    	ActsExtractor actsExtractor = new ActsExtractor();
    	actsExtractor.actsMain(args);

    	//AwardExtractor awardExtractor=new AwardExtractor();
		//awardExtractor.awardMain(args);

    	//ConferenceExtractor conferenceExtractor= new ConferenceExtractor();
     	//conferenceExtractor.ConferenceMain(args);
    	
    	//ExaminationExtractor examinationExtractor= new ExaminationExtractor();
    	//examinationExtractor.ExaminationMain(args);
    	
    	//PolicyExtractor policyExtractor= new PolicyExtractor();
    	//policyExtractor.PolicyMain(args);
    	
    	//PublicationExtractor publicationExtractor=new PublicationExtractor();
    	//publicationExtractor.PublicationMain(args);
    	
    	//SchemeExtractor schemeExtractor=new SchemeExtractor();
    	//schemeExtractor.SchemeMain(args);

	}

}
