package cdac.nvli.extractor.extract;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cdac.nvli.extractor.backend.ExtractorSolrServer;
import cdac.nvli.extractor.bean.PublicationBean;
import cdac.nvli.extractor.index.VasExtractorIndexer;
import cdac.nvli.extractor.parse.PublicationExtractorParser;

public class PublicationExtractor {
	
	static PublicationExtractorParser publicationExtractorParser;
	static VasExtractorIndexer publicationExtractorIndexer;
	static ArrayList<PublicationBean> publicationBeanList;
	static HttpSolrServer solrServer;
	static ExtractorSolrServer extractorSolrServer;
	private static final Logger LOG = LoggerFactory
         .getLogger(PublicationExtractor.class);
	
	private static String URLS_SEED_FILE_PATH; 
	public static String SOLR_FILE_PATH;
	
	//public static void main(String args[])
	public static void PublicationMain(String args[])
	{
		publicationExtractorParser = new PublicationExtractorParser();
		publicationExtractorIndexer = new VasExtractorIndexer();
		extractorSolrServer =new ExtractorSolrServer();
    try {
			
			LOG.info("Extractor started---->");
			
			
			//get solr server
			//solrServer=extractorSolrServer.getSolrServer();
			
			///Addded NEW 
			SOLR_FILE_PATH=args[1];
			solrServer=extractorSolrServer.getSolrServer();
			
			
			LOG.info("Solr conected---->");
			//URLS_SEED_FILE_PATH="src/resources/publicationUrls";
			URLS_SEED_FILE_PATH=args[0];
			BufferedReader br=new BufferedReader(new FileReader(URLS_SEED_FILE_PATH));
			
			String url="";
			while((url=br.readLine())!=null)		
			{
				BufferedWriter writer = null;
				try {
				//parse the url
				publicationBeanList=publicationExtractorParser.urlParser(url);
				System.out.println("parsing done---->");
				
				
				
					writer = new BufferedWriter(new FileWriter(
					          new File("tempub.txt"), true));
						for(PublicationBean cob:publicationBeanList)
						{
							writer.write(cob.toString());
						    writer.newLine();
						    writer.flush();  
						}
					
					
				LOG.info("Solr indexing started---->");
				//index parsed data
				for(PublicationBean publicationBean:publicationBeanList)
				{
					try
					{
					  publicationExtractorIndexer.indexVASDocs(solrServer, publicationBean);
					}
					catch(Exception ex)
					{
						continue;
					}
				}
				LOG.info("Solr indexing finished---->");
				} catch (Exception ex) {
					  // report
					LOG.error("Excecption occurred---->"+ex.getMessage());
					continue;
					} finally {
					   try {writer.close();} catch (Exception ex) {/*ignore*/}
					}
			}
		} 
		catch (Exception e1) {
			LOG.error("Excecption occurred---->"+e1.getMessage());
			}
		
	}
}
