package cdac.nvli.extractor.extract;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

import cdac.nvli.extractor.backend.ExtractorSolrServer;
import cdac.nvli.extractor.bean.AwardBean;
import cdac.nvli.extractor.bean.ConferenceBean;
import cdac.nvli.extractor.index.AwardExtractorIndexer;
import cdac.nvli.extractor.parse.AwardExtractorParser;
////

import cdac.nvli.extractor.parse.MysqlAwardExtractorParser;


public class AwardExtractorForLocalTesting {

	static AwardExtractorParser awardExtractorParser;
	static AwardExtractorIndexer awardExtractorIndexer;
	static ArrayList<AwardBean> awardBeanList;
	static HttpSolrServer solrServer;
	static ExtractorSolrServer extractorSolrServer;
	public static void main(String args[])
	{
		//String url="https://en.wikipedia.org/wiki/List_of_Padma_Bhushan_award_recipients_(1980–89)";		
		
		//String url="http://timesofindia.indiatimes.com/city/pune/carol-gracias-walks-the-ramp/articleshow/58923517.cms";
		//String url="http://www.manoramaonline.com/news/just-in/2017/05/31/kodanadu-estate-the-week-story.html";
		String url="https://en.wikipedia.org/wiki/Kamal_Kumari_National_Award";
		
		/*
		String articleEnds="html";
		if(url.contains(articleEnds)) 
		{
			System.out.println("pattern found"); 
		}*/
		
		
		try {
			
			awardExtractorParser= new AwardExtractorParser();
			awardExtractorIndexer= new AwardExtractorIndexer();
			extractorSolrServer =new ExtractorSolrServer();
			
			
				System.out.println("extractor started---->");
			
				//get solr server
				solrServer=extractorSolrServer.getSolrServer();
				System.out.println("solr conected---->");
				//parse the url
			
				
				awardBeanList=awardExtractorParser.parseIt(url);
				System.out.println("parsing done---->");
				BufferedWriter writer = null;
				
				try {
					writer = new BufferedWriter(new FileWriter(
					          new File("temp.txt"), true));
						for(AwardBean cob:awardBeanList)
						{
							writer.write(cob.toString());
						    writer.newLine();
						    writer.flush();
						 //   System.out.println("List is *****"+cob.toString()); 
						}
					} catch (IOException ex) {
					  // report
					} finally {
					   try {writer.close();} catch (Exception ex) {/*ignore*/}
					}
					
				
				/*for(AwardBean awardBean:awardBeanList)
				{
					//index parsed data
					awardExtractorIndexer.indexVASDocs(solrServer, awardBean);
				}*/
				System.out.println("finished---->");
			
		} 
		catch (Exception e1) {
			
			e1.printStackTrace();
		}
	}
	
		
}
