package cdac.nvli.extractor.extract;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import org.apache.solr.client.solrj.impl.HttpSolrServer;

import cdac.nvli.extractor.backend.ConferenceExtractorSolrServer;
import cdac.nvli.extractor.backend.ExtractorSolrServer;
import cdac.nvli.extractor.bean.AwardBean;
import cdac.nvli.extractor.bean.ConferenceBean;
import cdac.nvli.extractor.index.ConferenceExtractorIndexer;
import cdac.nvli.extractor.index.VasExtractorIndexer;
import cdac.nvli.extractor.parse.ConferenceExtractorParser;

public class ConferenceExtractor {
	static ConferenceExtractorParser conferenceExtractorParser;
	static VasExtractorIndexer conferenceExtractorIndexer;
	static ArrayList<ConferenceBean> conferenceBeanList;
	static HttpSolrServer solrServer;
	static ExtractorSolrServer extractorSolrServer;
	
	private static String URLS_SEED_FILE_PATH; 
	public static String SOLR_FILE_PATH;
	
	//public static void main(String args[])
	public static void ConferenceMain(String args[])
	{
		conferenceExtractorParser= new ConferenceExtractorParser();
		conferenceExtractorIndexer= new VasExtractorIndexer();
		extractorSolrServer =new ExtractorSolrServer();
		try {
				System.out.println("extrator started---->");
				//get solr server
				SOLR_FILE_PATH=args[1];
				solrServer=extractorSolrServer.getSolrServer();
				System.out.println("solr conected---->");
				
				//URLS_SEED_FILE_PATH="src/resources/upcomingConfUrls";
				URLS_SEED_FILE_PATH=args[0];
				BufferedReader br=new BufferedReader(new FileReader(URLS_SEED_FILE_PATH));
				
			//	BufferedReader br=new BufferedReader(new FileReader("src/resources/upcomingConfUrls"));
				String url="";
				while((url=br.readLine())!=null)		
				{
					try {
						//parse the url
						conferenceBeanList=conferenceExtractorParser.parseIt(url);
						BufferedWriter writer = null;
						Collections.sort(conferenceBeanList, new Comparator<ConferenceBean>() {
					        @Override
					        public int compare(ConferenceBean c1, ConferenceBean c2)
					        {

					            return  c1.getDate().compareTo(c2.getDate());
					        }
					    });
						
						try {
								writer = new BufferedWriter(new FileWriter(
								          new File("temp-conf.txt"), true));
								for(ConferenceBean cob:conferenceBeanList)
								{
									System.out.println("List is *****"+cob.toString()); 
										writer.write(cob.toString());
									    writer.newLine();
									    writer.flush();
									 
									}
								} catch (IOException ex) {
								  // report
								} finally {
								   try {writer.close();} catch (Exception ex) {/*ignore*/}
								}
						
						
						System.out.println("parsing done---->@@");
					
						for(ConferenceBean conferenceBean:conferenceBeanList)
						{
							//index parsed data
							try
							{
								conferenceExtractorIndexer.indexVASDocs(solrServer, conferenceBean);
							}
							catch(Exception ex)
							{
								continue;
							}
						}
						System.out.println("indexing done---->");
					} catch (Exception e) {
						
						e.printStackTrace();
					}
				}
				System.out.println("finished---->");
		}
		catch (Exception e1) {
			
			e1.printStackTrace();
		}
	}
	
}