package cdac.nvli.extractor.extract;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cdac.nvli.extractor.backend.ExtractorSolrServer;
import cdac.nvli.extractor.bean.AwardBean;
import cdac.nvli.extractor.index.VasExtractorIndexer;
import cdac.nvli.extractor.parse.AwardExtractorParser;


/**
 * This class is the entry point for extracting VAS related 
 * data for NVLI
 * @author pragya
 *
 */

public class AwardExtractor {

	static AwardExtractorParser awardExtractorParser;
	static VasExtractorIndexer awardExtractorIndexer;
	static ArrayList<AwardBean> awardBeanList;
	static HttpSolrServer solrServer;
	static ExtractorSolrServer extractorSolrServer;
	private static final Logger LOG = LoggerFactory
		      .getLogger(AwardExtractor.class);
	
	private static String URLS_SEED_FILE_PATH;
	public static String SOLR_FILE_PATH;
	
	/**
	 * @param url seed list file location
	 */
	//public static void main(String args[])
	public static void awardMain(String args[])
	{
		awardExtractorParser= new AwardExtractorParser();
		awardExtractorIndexer= new VasExtractorIndexer();
		extractorSolrServer =new ExtractorSolrServer();
		try {
			
			LOG.info("Extractor started---->");
			
			
			//get solr server	
			//System.out.println(SOLR_FILE_PATH);
			SOLR_FILE_PATH=args[1];
			solrServer=extractorSolrServer.getSolrServer();
			
			LOG.info("Solr conected---->");
			//URLS_SEED_FILE_PATH="src/resources/awardUrls";
			URLS_SEED_FILE_PATH=args[0];
			BufferedReader br=new BufferedReader(new FileReader(URLS_SEED_FILE_PATH));
			
			String url="";
			while((url=br.readLine())!=null)		
			{
				
				//parse the url
				awardBeanList=awardExtractorParser.parseIt(url);
				System.out.println("parsing done---->");
				BufferedWriter writer = null;
				
				try {
					writer = new BufferedWriter(new FileWriter(
					          new File("temp.txt"), true));
						for(AwardBean cob:awardBeanList)
						{
							writer.write(cob.toString());
						    writer.newLine();
						    writer.flush();
						 
						}
					} catch (IOException ex) {
					  // report
					} finally {
					   try {writer.close();} catch (Exception ex) {/*ignore*/}
					}
					
				LOG.info("Solr indexing started---->");
				//index parsed data
				for(AwardBean awardBean:awardBeanList)
				{
					try
					{
						awardExtractorIndexer.indexVASDocs(solrServer, awardBean);
					}
					catch(Exception ex)
					{
						continue;
					}
				}
				LOG.info("Solr indexing finished---->");
			}
		} 
		catch (Exception e1) 
		{
			LOG.error("Excecption occurred---->"+e1.getMessage());
		}
	}
}
