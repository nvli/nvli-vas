package cdac.nvli.extractor.extract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import org.apache.solr.client.solrj.impl.HttpSolrServer;

import cdac.nvli.extractor.backend.ExtractorSolrServer;
import cdac.nvli.extractor.bean.ConferenceBean;
import cdac.nvli.extractor.index.VasExtractorIndexer;
import cdac.nvli.extractor.parse.ConferenceExtractorParser;

public class ConferenceExtractorForLocalTesting {
	static ConferenceExtractorParser conferenceExtractorParser;
	static VasExtractorIndexer conferenceExtractorIndexer;
	static ArrayList<ConferenceBean> conferenceBeanList;
	static HttpSolrServer solrServer;
	static ExtractorSolrServer extractorSolrServer;
	
	public static void main(String args[])
	{
			//String url="http://ncsm.gov.in/?page_id=2621"; --not done
			//String url="http://www.muhs.ac.in/dept_links.aspx?doctype=CS&doctypetitle=Conferences%20/%20Seminars";// --indexing done for this url
			//String url="http://www.nagarjunauniversity.ac.in/seminars.php"; --not done 
		//String url="https://nalsar.ac.in/events"; --not done
		
		//String url="http://www.nipfp.org.in/events/"; //--indexing done for this url
		//String url="http://www.ieee.org/conferences_events/conferences/search/index.html?RANGE_FROM_DATE=2017-01-01&RANGE_TO_DATE=2030-12-31&KEYWORDS=&COUNTRY=ALL&STATE=ALL&CITY=ALL&REGION=ALL&RECORD_NUM=ALL&SPONSOR=ALL&EXHIBIT=ALL&TUTORIALS=ALL&RowsPerPage=10&PageLinkNum=10&ActivePage=67&SORTORDER=asc&SORTFIELD=start_date&ROWSTART=660&CONF_SRCH_RDO=conf_date"; 
		String url="https://www.allconferencealert.com/event_detail.php?ev_id=113216";
			conferenceExtractorParser= new ConferenceExtractorParser();
			conferenceExtractorIndexer= new VasExtractorIndexer();
			extractorSolrServer =new ExtractorSolrServer();
		
			System.out.println("extrator started---->");
			try {
				//get solr server
				solrServer=extractorSolrServer.getSolrServer();
				System.out.println("solr conected---->");
				//parse the url
				conferenceBeanList=conferenceExtractorParser.parseIt(url);
				
				
				Collections.sort(conferenceBeanList, new Comparator<ConferenceBean>() {
			        @Override
			        public int compare(ConferenceBean c1, ConferenceBean c2)
			        {

			            return  c1.getDate().compareTo(c2.getDate());
			        }
			    });
				
				for(ConferenceBean cob:conferenceBeanList)
				{
					System.out.println("List is *****"+cob.toString()); 
				}
				
				//System.out.println("parsing done---->@@");
				
				/*for(ConferenceBean conferenceBean:conferenceBeanList)
				{
					//index parsed data
					System.out.println("indexing satrted---->@@");
					conferenceExtractorIndexer.indexVASDocs(solrServer, conferenceBean);
				}*/
				System.out.println("finished---->");
			} catch (Exception e) {
				
				e.printStackTrace();
			
		}	
	}
}
