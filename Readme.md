NVLI -- Value Added Service Extractor -- Build 1.0

Steps: 

1. Create a clean build of the project  (preferably in Eclipse IDE, as build.xml is yet to be developed)
2. Create a collection (nvli_vas) at your local solr and copy the schema.xml from resources folder to the collection/conf
3. Start the solr
4. Set the proper locations of solr server in resources/solrconfig.properties 
5. The url list is available in resources folder (awardUrls & upcomingConfUrls)
6. Run the AwardExtractor & ConferenceExtractor from cdac.nvli.extractor.extract packages for extraction of conference and awards pages from the url lists
